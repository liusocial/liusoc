<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Schema;

class Announcement extends Model
{
    use SoftDeletes;

    public $table = 'announcements';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'description',
        'image',
        'start_date',
        'user_id',
        'school_id',
        'end_date',
        'active',
        'deleted_at',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'announcement_user', 'announcement_id', 'user_id');
    }


    public static function chkAnnouncementExpire($id)
    {
        $current_date = date('Y-m-d', strtotime(Carbon::now()));
        $announcement = self::find($id);

        if ($announcement->start_date < $announcement->end_date && $current_date <= $announcement->end_date) {
            return 'notexpired';
        } else {
            return 'expired';
        }
    }

    static function createOrUpdate($data, $keys) {
        $record = self::where($keys)->first();
        if (is_null($record)) {
            return self::create($data);
        } else {
            return self::where($keys)->update($data);
        }
    }
    public static function get($default = '')
    {
        $result = '';
        if (Schema::hasTable('announcements')) {
            $value = DB::table('announcements')->where('value', 1)->pluck('id');
            foreach ($value as $val) {
                $result = $val;
            }

            return $value ? $result : $default;
        }

        return $default;
    }
}
