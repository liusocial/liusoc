<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Schema;
use Auth;

class Block extends Model
{
    use SoftDeletes;

    public $table = 'blocks';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'timeline_id',
        'timeline_report_id',
        'description',
        'start_date',
        'end_date',
        'deleted_at',
    ];


//    public function users()
//    {
//        return $this->belongsToMany('App\User', 'announcement_user', 'announcement_id', 'user_id');
//    }


    public function deleteBlockSettings($id)
    {
        $result = DB::table('blocks')->where('id', $id)->delete();

        return $result;
    }


    public static function chkBlockExpire($id)
    {
        $current_date = date('Y-m-d', strtotime(Carbon::now()));
        $block = self::find($id);

        if ($block->start_date < $block->end_date && $current_date <= $block->end_date) {
            return 'notexpired';
        } else {
            return 'expired';
        }
    }
//
    public static function chkBlockUser($username)
    {
        $timeline = Timeline::where('username',$username)->first();
        $block_user =Block::all();
//        $current_date = date('Y-m-d', strtotime(Carbon::now()));
//        $block = self::find($id);

        foreach ($block_user as $block){
        if ($block->timeline_id == $timeline->id) {
            return 'existed';
        } else {
            return 'notexisted';
        }
        }
    }

    static function createOrUpdate($data, $keys) {
        $record = self::where($keys)->first();
        if (is_null($record)) {
            return self::create($data);
        } else {
            return self::where($keys)->update($data);
        }
    }
    public static function get($default = '')
    {
        $result = '';
        if (Schema::hasTable('announcements')) {
            $value = DB::table('announcements')->where('value', 1)->pluck('id');
            foreach ($value as $val) {
                $result = $val;
            }

            return $value ? $result : $default;
        }

        return $default;
    }
}
