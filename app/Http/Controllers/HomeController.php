<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function getDownload()
    {

        $file = public_path() . "/download/adn/admin_guide.pdf";
        $file = public_path() . "/download/den/dean_guide.pdf";
        $file = public_path() . "/download/user_guide.pdf";

        $headers = [
            'Content-Type' => 'application/pdf',
        ];


    }
}