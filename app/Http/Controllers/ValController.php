<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Category;
use App\Comment;
use App\Group;
use App\Notification;
use App\Page;
use App\Post;
use App\School;
use App\Block;
use App\Role;
use App\Setting;
use App\StaticPage;
use App\Timeline;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Exception;
use File;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Teepluss\Theme\Facades\Theme;
use Validator;

class ValController extends Controller
{
    public function __construct()
    {
        $this->middleware('disabledemo', ['only' => [
            'addCustomPage',
            'updateCustomPage',
            'updateGeneralSettings',
            'updateUserSettings',
            'updatePageSettings',
            'updateGroupSettings',
            'updateAnnouncement',
            'addAnnouncements',
            'removeAnnouncement',
            'activeAnnouncement',
            'updateUser',
            'updatePassword',
            'deleteUser',
            'updatePage',
            'deletePage',
            'updateGroup',
            'deleteGroup',
            'markSafeReports',
            'deletePostReports',
            'updateManageAds',
            'markPageSafeReports',
            'deletePageReports',
            'deleteGroupReports',
            'deleteUserReports',
            'saveEnv',
        ],
        ]);

    }




    public function show_req()
    {//$users2 = Auth::user()->where('email_verified','=','0')->get();
        $users = User::where('verified','=','0')->orderBy('created_at', 'desc')->paginate(Setting::get('items_page', 10));

        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('validator');

        return $theme->scope('validator/show-req', compact('users'))->render();
    }





    public function approveUser($user_id)
    {$user = User::where('id', '=', $user_id)->first();

        if ($user) {
            $user->verified = 1;
            $chk_ver=$user->update();
            if ($chk_ver) {

                Flash::success(trans('messages.user_approved_success'));

                return redirect()->back();

            }
        }
    }
    public function rejectUser($user_id)
    { $user = User::find($user_id);
        $chk_user = $user->getUserSettings($user_id);
        if ($chk_user) {

            $chk_delete = $user->deleteUserSettings($user_id);
            if ($chk_delete) {
                if ($user->delete()) {
                    Flash::success(trans('messages.user_reject'));

                    return redirect()->back();
                }
            }
        }


    }



    public function themes()
    {
        $theme = Theme::uses(Setting::get('current_theme', 'default'))->layout('validator');

        $themes = File::directories(base_path('public/themes'));

        $themesInfo = [];

        //  Setting::get('current_theme')


        foreach ($themes as $key => $value) {
            $themeInfo = json_decode(file_get_contents($value.'/theme.json'));
            $themeInfo->thumbnail = str_replace(base_path('public'), '', $value).'/'.$themeInfo->thumbnail;
            $themeInfo->directory = str_replace(base_path('public/themes/'), '', $value);
            $themesInfo[] = $themeInfo;
        }

        return $theme->scope('admin/themes', compact('themesInfo'))->render();
    }
}
