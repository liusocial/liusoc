<?php

use Cmgmyr\Messenger\Models\Message;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::get('/contact', 'PageController@contact');
Route::post('/contact', 'PageController@saveContact');

Route::group(['prefix' => 'api', 'middleware' => ['auth', 'cors'], 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require config('infyom.laravel_generator.path.api_routes');
    });
});

Route::post('pusher/auth', function (Illuminate\Http\Request $request, Pusher $pusher) {
    return $pusher->presence_auth(
        $request->input('channel_name'),
        $request->input('socket_id'),
        uniqid(),
        ['username' => $request->input('username')]
    );
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('timelines', 'TimelineController');

Route::group(['middleware' => ['web']], function () {
    Route::auth();
});


// Redirect to facebook to authenticate
Route::get('facebook', 'Auth\AuthController@facebookRedirect');
// Get back to redirect url
Route::get('account/facebook', 'Auth\AuthController@facebook');

// Redirect to google to authenticate
Route::get('google', 'Auth\AuthController@googleRedirect');
// Get back to redirect url
Route::get('account/google', 'Auth\AuthController@google');

// Redirect to twitter to authenticate
Route::get('twitter', 'Auth\AuthController@twitterRedirect');
// Get back to redirect url
Route::get('account/twitter', 'Auth\AuthController@twitter');

// Redirect to linkedin to authenticate
Route::get('linkedin', 'Auth\AuthController@linkedinRedirect');
// Get back to redirect url
Route::get('account/linkedin', 'Auth\AuthController@linkedin');


// Login
Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@login');

// Register
Route::get('/register', 'Auth\AuthController@register');

Route::post('/register', 'Auth\AuthController@registerUser');

Route::get('email/verify', 'Auth\AuthController@verifyEmail');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'TimelineController@showFeed');
    Route::get('/browse', 'TimelineController@showGlobalFeed');
    Route::get('/deactivate-home', 'TimelineController@deactivateHome');
});


Route::get('/home', 'HomeController@index');

Route::post('/member/update-role', 'TimelineController@assignMemberRole');
Route::post('/member/updatepage-role', 'TimelineController@assignPageMemberRole');
Route::get('/post/{post_id}', 'TimelineController@singlePost');

Route::get('/poost/{post_id}', 'TimelineController@postshareframe');
/*
|--------------------------------------------------------------------------
| Admin routes
|--------------------------------------------------------------------------
*/


Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'role:admin']], function () {
    Route::get('/', 'AdminController@dashboard');
    Route::get('/general-settings', 'AdminController@generalSettings');
    Route::post('/general-settings', 'AdminController@updateGeneralSettings');

    Route::get('/user-settings', 'AdminController@userSettings');
    Route::post('/user-settings', 'AdminController@updateUserSettings');

    Route::get('/page-settings', 'AdminController@pageSettings');
    Route::post('/page-settings', 'AdminController@updatePageSettings');

    Route::get('/group-settings', 'AdminController@groupSettings');
    Route::post('/group-settings', 'AdminController@updateGroupSettings');

    Route::get('/custom-pages', 'AdminController@listCustomPages');
    Route::get('/custom-pages/create', 'AdminController@createCustomPage');
    Route::post('/custom-pages', 'AdminController@addCustomPage');
    Route::get('/custom-pages/{id}/edit', 'AdminController@editCustomPage');
    Route::post('/custom-pages/{id}/update', 'AdminController@updateCustomPage');

    Route::get('/announcements', 'AdminController@getAnnouncements');
    Route::get('/announcements/create', 'AdminController@createAnnouncement');
    Route::get('/announcements/create_business', 'AdminController@createBusinessAnnouncement');
    Route::get('/announcements/create_art', 'AdminController@createArtAnnouncement');
    Route::get('/announcements/create_it', 'AdminController@createITAnnouncement');
    Route::get('/announcements/create_medicine', 'AdminController@createMedicineAnnouncement');
    Route::get('/announcements/create_eng', 'AdminController@createEngineeringAnnouncement');
    Route::get('/announcements/create_english', 'AdminController@createEnglishAnnouncement');
    Route::get('/announcements/create_std', 'AdminController@createStudentAnnouncement');
    Route::get('/announcements/{id}/edit', 'AdminController@editAnnouncement');
    Route::post('/announcements/{id}/update', 'AdminController@updateAnnouncement');
    Route::post('/announcements', 'AdminController@addAnnouncements');
    Route::get('/activate/{announcement_id}', 'AdminController@activeAnnouncement');
    Route::get('/inactivate/{announcement_id}', 'AdminController@inactiveAnnouncement');

    Route::get('/themes', 'AdminController@themes');
    Route::get('/change-theme/{name}', 'AdminController@changeTheme');

    Route::get('/users', 'AdminController@showUsers');
    Route::get('/users/show', 'AdminController@showDeactivateUsers');
    Route::get('/users/add/create', 'AdminController@createUser');
    Route::get('/users/add/create_admin', 'AdminController@createAdmin');
    Route::get('/users/{username}/edit', 'AdminController@editUser');
    Route::post('/users/{username}/edit', 'AdminController@updateUser');
    Route::post('/users', 'AdminController@addUsers');
    Route::post('/users/add-admin', 'AdminController@addAdmin');
    Route::get('/users/{user_id}/delete', 'AdminController@deleteUser');
    Route::get('/activate-user/{user_id}', 'AdminController@activateUser');
    Route::get('/deactivate-user/{user_id}', 'AdminController@deactivateUser');

    Route::get('/requests', 'AdminController@show_req');
    Route::get('/requests/{user_id}/approve', 'AdminController@approveUser');
    Route::get('/requests/{user_id}/reject', 'AdminController@rejectUser');


    Route::get('/users/{username}/delete', 'AdminController@deleteUser');
    Route::post('/users/{username}/newpassword', 'AdminController@updatePassword');

    Route::get('/pages', 'AdminController@showPages');
    Route::get('/pages/add/create', 'AdminController@createPage');
    Route::post('/pages', 'AdminController@addPage');
    Route::get('/pages/{username}/edit', 'AdminController@editPage');
    Route::post('/pages/{username}/edit', 'AdminController@updatePage');
    Route::get('/pages/{page_id}/delete', 'AdminController@deletePage');


    Route::get('/groups', 'AdminController@showGroups');
    Route::get('/groups/{username}/edit', 'AdminController@editGroup');
    Route::post('/groups/{username}/edit', 'AdminController@updateGroup');
    Route::get('/groups/{group_id}/delete', 'AdminController@deleteGroup');


    //Route::get('/blocks', 'AdminController@getAnnouncements');
    Route::get('/blocks/{id}/create', 'AdminController@createBlock');
    Route::get('/blocks/{id}/edit', 'AdminController@editBlock');
    Route::post('/blocks/{id}/update', 'AdminController@updateBlock');
    Route::post('/blocks', 'AdminController@addBlocks');
    Route::get('/blocks/{block_id}/{report_id}/delete', 'AdminController@removeBlock');


    Route::get('/manage-reports', 'AdminController@manageReports');
    Route::post('/manage-reports', 'AdminController@updateManageReports');
    Route::get('/mark-safe/{report_id}', 'AdminController@markSafeReports');
    Route::get('/delete-post/{report_id}/{post_id}', 'AdminController@deletePostReports');

    Route::get('/manage-ads', 'AdminController@manageAds');
    Route::get('/update-database', 'AdminController@getUpdateDatabase');
    Route::post('/update-database', 'AdminController@postUpdateDatabase');
    Route::get('/get-env', 'AdminController@getEnv');
    Route::post('/save-env', 'AdminController@saveEnv');
    Route::post('/manage-ads', 'AdminController@updateManageAds');
    Route::get('/settings', 'AdminController@settings');
    Route::get('/markpage-safe/{report_id}', 'AdminController@markPageSafeReports');
    Route::get('/deletepage-post/{report_id}/{timeline_id}', 'AdminController@deletePageReports');
    Route::get('/deleteuser-post/{report_id}/{timeline_id}', 'AdminController@deleteUserReports');
    Route::get('/deletegroup-post/{report_id}/{timeline_id}', 'AdminController@deleteGroupReports');
});



Route::group(['prefix' => '/devloper', 'middleware' => ['auth', 'role:devloper']], function () {
    Route::get('/', 'DevController@dashboard');
    Route::get('/general-settings', 'DevController@generalSettings');
    Route::post('/general-settings', 'DevController@updateGeneralSettings');

    Route::get('/user-settings', 'DevController@userSettings');
    Route::post('/user-settings', 'DevController@updateUserSettings');

    Route::get('/page-settings', 'DevController@pageSettings');
    Route::post('/page-settings', 'DevController@updatePageSettings');

    Route::get('/group-settings', 'DevController@groupSettings');
    Route::post('/group-settings', 'DevController@updateGroupSettings');

    Route::get('/custom-pages', 'DevController@listCustomPages');
    Route::get('/custom-pages/create', 'DevController@createCustomPage');
    Route::post('/custom-pages', 'DevController@addCustomPage');
    Route::get('/custom-pages/{id}/edit', 'DevController@editCustomPage');
    Route::post('/custom-pages/{id}/update', 'DevController@updateCustomPage');

    Route::get('/announcements', 'DevController@getAnnouncements');
    Route::get('/announcements/create', 'DevController@createAnnouncement');
    Route::get('/announcements/create_business', 'DevController@createBusinessAnnouncement');
    Route::get('/announcements/create_art', 'DevController@createArtAnnouncement');
    Route::get('/announcements/create_it', 'DevController@createITAnnouncement');
    Route::get('/announcements/create_medicine', 'DevController@createMedicineAnnouncement');
    Route::get('/announcements/create_eng', 'DevController@createEngineeringAnnouncement');
    Route::get('/announcements/create_english', 'DevController@createEnglishAnnouncement');
    Route::get('/announcements/create_std', 'DevController@createStudentAnnouncement');
    Route::get('/announcements/{id}/edit', 'DevController@editAnnouncement');
    Route::post('/announcements/{id}/update', 'DevController@updateAnnouncement');
    Route::post('/announcements', 'DevController@addAnnouncements');
    Route::get('/activate/{announcement_id}', 'DevController@activeAnnouncement');
    Route::get('/inactivate/{announcement_id}', 'DevController@inactiveAnnouncement');

    Route::get('/themes', 'DevController@themes');
    Route::get('/change-theme/{name}', 'DevController@changeTheme');


    Route::get('/users', 'DevController@showUsers');
    Route::get('/users/show', 'DevController@showDeactivateUsers');
    Route::get('/users', 'DevController@showUsers');
    Route::get('/users/add/create', 'DevController@createUser');
    Route::get('/users/add/create_admin', 'DevController@createAdmin');
    Route::get('/users/{username}/edit', 'DevController@editUser');
    Route::post('/users/{username}/edit', 'DevController@updateUser');
    Route::post('/users', 'DevController@addUsers');
    Route::post('/users/add-admin', 'DevController@addAdmin');
    Route::get('/users/{user_id}/delete', 'DevController@deleteUser');
    Route::get('/activate-user/{user_id}', 'DevController@activateUser');
    Route::get('/deactivate-user/{user_id}', 'DevController@deactivateUser');


    Route::get('/requests', 'DevController@show_req');
    Route::get('/requests/{user_id}/approve', 'DevController@approveUser');
    Route::get('/requests/{user_id}/reject', 'DevController@rejectUser');


    Route::get('/users/{username}/delete', 'DevController@deleteUser');
    Route::post('/users/{username}/newpassword', 'DevController@updatePassword');

    Route::get('/pages', 'DevController@showPages');
    Route::get('/pages/add/create', 'DevController@createPage');
    Route::post('/pages', 'DevController@addPage');
    Route::get('/pages/{username}/edit', 'DevController@editPage');
    Route::post('/pages/{username}/edit', 'DevController@updatePage');
    Route::get('/pages/{page_id}/delete', 'DevController@deletePage');


    Route::get('/groups', 'DevController@showGroups');
    Route::get('/groups/{username}/edit', 'DevController@editGroup');
    Route::post('/groups/{username}/edit', 'DevController@updateGroup');
    Route::get('/groups/{group_id}/delete', 'DevController@deleteGroup');


    //Route::get('/blocks', 'AdminController@getAnnouncements');
    Route::get('/blocks/{id}/create', 'DevController@createBlock');
    Route::get('/blocks/{id}/edit', 'DevController@editBlock');
    Route::post('/blocks/{id}/update', 'DevController@updateBlock');
    Route::post('/blocks', 'DevController@addBlocks');
    Route::get('/blocks/{block_id}/{report_id}/delete', 'DevController@removeBlock');


    Route::get('/manage-reports', 'DevController@manageReports');
    Route::post('/manage-reports', 'DevController@updateManageReports');
    Route::get('/mark-safe/{report_id}', 'DevController@markSafeReports');
    Route::get('/delete-post/{report_id}/{post_id}', 'DevController@deletePostReports');

    Route::get('/manage-ads', 'DevController@manageAds');
    Route::get('/update-database', 'DevController@getUpdateDatabase');
    Route::post('/update-database', 'DevController@postUpdateDatabase');
    Route::get('/get-env', 'DevController@getEnv');
    Route::post('/save-env', 'DevController@saveEnv');
    Route::post('/manage-ads', 'DevController@updateManageAds');
    Route::get('/settings', 'DevController@settings');
    Route::get('/markpage-safe/{report_id}', 'DevController@markPageSafeReports');
    Route::get('/deletepage-post/{report_id}/{timeline_id}', 'DevController@deletePageReports');
    Route::get('/deleteuser-post/{report_id}/{timeline_id}', 'DevController@deleteUserReports');
    Route::get('/deletegroup-post/{report_id}/{timeline_id}', 'DevController@deleteGroupReports');
});


Route::group(['prefix' => '/validator', 'middleware' => ['auth', 'role:validator']], function () {
    Route::get('/requests', 'ValController@show_req');
    Route::get('/requests/{user_id}/approve', 'ValController@approveUser');
    Route::get('/requests/{user_id}/reject', 'ValController@rejectUser');

    Route::get('/themes', 'ValController@themes');
    Route::get('/change-theme/{name}', 'ValController@changeTheme');

});

Route::group(['prefix' => '/dean', 'middleware' => ['auth', 'role:dean']], function () {
    Route::get('/', 'DeanController@dashboard');
    Route::get('/general-settings', 'DeanController@generalSettings');
    Route::post('/general-settings', 'DeanController@updateGeneralSettings');

    Route::get('/user-settings', 'DeanController@userSettings');
    Route::post('/user-settings', 'DeanController@updateUserSettings');

    Route::get('/page-settings', 'DeanController@pageSettings');
    Route::post('/page-settings', 'DeanController@updatePageSettings');

    Route::get('/group-settings', 'DeanController@groupSettings');
    Route::post('/group-settings', 'DeanController@updateGroupSettings');

    Route::get('/custom-pages', 'DeanController@listCustomPages');
    Route::get('/custom-pages/create', 'DeanController@createCustomPage');
    Route::post('/custom-pages', 'DeanController@addCustomPage');
    Route::get('/custom-pages/{id}/edit', 'DeanController@editCustomPage');
    Route::post('/custom-pages/{id}/update', 'DeanController@updateCustomPage');

    Route::get('/announcements', 'DeanController@getAnnouncements');
    Route::get('/announcements/create', 'DeanController@createAnnouncement');
    Route::get('/announcements/{id}/edit', 'DeanController@editAnnouncement');
    Route::post('/announcements/{id}/update', 'DeanController@updateAnnouncement');
    Route::post('/announcements', 'DeanController@addAnnouncements');
    Route::get('/activate/{announcement_id}', 'DeanController@activeAnnouncement');
    Route::get('/inactivate/{announcement_id}', 'DeanController@inactiveAnnouncement');

    Route::get('/themes', 'DeanController@themes');
    Route::get('/change-theme/{name}', 'DeanController@changeTheme');

    Route::get('/users', 'DeanController@showUsers');
    Route::get('/users/add/create', 'DeanController@createUser');
    Route::get('/users/{username}/edit', 'DeanController@editUser');
    Route::post('/users/{username}/edit', 'DeanController@updateUser');
    Route::post('/users', 'DeanController@addUsers');
    Route::get('/users/{user_id}/delete', 'DeanController@deleteUser');
    Route::get('/activate-user/{user_id}', 'DeanController@activateUser');
    Route::get('/deactivate-user/{user_id}', 'DeanController@deactivateUser');

    Route::get('/users/{username}/delete', 'DeanController@deleteUser');
    Route::post('/users/{username}/newpassword', 'DeanController@updatePassword');
    Route::post('/users', 'DeanController@addUsers');
    Route::get('/pages', 'DeanController@showPages');

//    Route::get('/pages', 'DeanController@addPage');
//    Route::post('/pages/add', 'DeanController@createPage');

    //Route::post('/pages', 'DeanController@addPage');
    Route::get('/pages/add/create', 'DeanController@createPage');
    Route::post('/pages', 'DeanController@addPage');
//    Route::get('/pages/create', 'DeanController@createPage');
    Route::get('/pages/{username}/edit', 'DeanController@editPage');
    Route::post('/pages/{username}/edit', 'DeanController@updatePage');
    Route::get('/pages/{page_id}/delete', 'DeanController@deletePage');


    Route::get('/groups', 'DeanController@showGroups');
    Route::get('/groups/{username}/edit', 'DeanController@editGroup');
    Route::post('/groups/{username}/edit', 'DeanController@updateGroup');
    Route::get('/groups/{group_id}/delete', 'DeanController@deleteGroup');


    Route::get('/manage-reports', 'DeanController@manageReports');
    Route::post('/manage-reports', 'DeanController@updateManageReports');
    Route::get('/mark-safe/{report_id}', 'DeanController@markSafeReports');
    Route::get('/delete-post/{report_id}/{post_id}', 'DeanController@deletePostReports');

    Route::get('/manage-ads', 'DeanController@manageAds');
    Route::get('/update-database', 'DeanController@getUpdateDatabase');
    Route::post('/update-database', 'DeanController@postUpdateDatabase');
    Route::get('/get-env', 'DeanController@getEnv');
    Route::post('/save-env', 'DeanController@saveEnv');
    Route::post('/manage-ads', 'DeanController@updateManageAds');
    Route::get('/settings', 'DeanController@settings');
    Route::get('/markpage-safe/{report_id}', 'DeanController@markPageSafeReports');
    Route::get('/deletepage-post/{report_id}/{timeline_id}', 'DeanController@deletePageReports');
    Route::get('/deleteuser-post/{report_id}/{timeline_id}', 'DeanController@deleteUserReports');
    Route::get('/deletegroup-post/{report_id}/{timeline_id}', 'DeanController@deleteGroupReports');
});

/*
|--------------------------------------------------------------------------
| Messages routes
|--------------------------------------------------------------------------
*/

    Route::get('messages/{username?}', 'MessageController@index');
    Route::get('messages/{thread_id}/delete', 'MessageController@deleteChat');


/*
|--------------------------------------------------------------------------
| User routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => '/{username}', 'middleware' => 'auth'], function ($username) {
    Route::get('/', 'TimelineController@showTimeline');

    Route::get('/messages', 'UserController@messages');

    Route::get('/followers', 'UserController@followers');

    Route::get('/following', 'UserController@following');

    Route::get('/posts', 'TimelineController@posts');
    Route::get('/deactivate', 'TimelineController@deactivate');

    Route::get('/liked-pages', 'UserController@likedPages');
    Route::get('/joined-groups', 'UserController@joinedGroups');

    Route::get('/create-page', 'TimelineController@addPage');
    Route::post('/create-page', 'TimelineController@createPage');

    Route::get('/create-group', 'TimelineController@addGroup');
    Route::post('/create-group', 'TimelineController@createGroupPage');
    Route::get('/members/{group_id}', 'TimelineController@getGroupMember');

    Route::get('/pages-groups', 'TimelineController@pagesGroups');
    Route::get('/pages-groups_in', 'TimelineController@pagesGroupsIn');

    Route::get('/groupadmin/{group_id}', 'TimelineController@getAdminMember');
    Route::get('/groupposts/{group_id}', 'TimelineController@getGroupPosts');
    Route::get('/page-posts', 'TimelineController@getPagePosts');
    Route::get('/add-members', 'UserController@membersList');
    Route::get('/page-likes', 'TimelineController@getPageLikes');
    Route::get('/pagemembers', 'TimelineController@getPageMember');
    Route::get('/pageadmin', 'TimelineController@getPageAdmins');
    Route::get('/pagedean', 'TimelineController@getPageDeans');
    Route::get('/add-pagemembers', 'UserController@pageMembersList');

    Route::get('/notification/{id}', 'NotificationController@redirectNotification');
});

Route::group(['prefix' => '/{username}', 'middleware' => ['auth', 'editown']], function ($username) {
    Route::get('/follow-requests', 'UserController@followRequests');
});

Route::group(['prefix' => '/{username}/settings', 'middleware' => ['auth', 'editown']], function ($username) {
    Route::get('/general', 'UserController@userGeneralSettings');
    Route::post('/general', 'UserController@saveUserGeneralSettings');

    Route::get('/privacy', 'UserController@userPrivacySettings');

    Route::post('/privacy', 'UserController@SaveUserPrivacySettings');

    Route::get('/password', 'UserController@userPasswordSettings');
    Route::post('/password', 'UserController@saveNewPassword');

    Route::get('/affliates', 'UserController@affliates');

    Route::get('/deactivate', 'UserController@deactivateUser');
    Route::get('/deleteme', 'UserController@deleteMe');

    Route::get('/notifications', 'UserController@emailNotifications');
    Route::post('/notifications', 'UserController@updateEmailNotifications');
});

/*
|--------------------------------------------------------------------------
| User dashboard routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => '/{username}/page-settings', 'middleware' => ['auth', 'editpage']], function ($username) {
    Route::get('/general', 'TimelineController@generalPageSettings');
    Route::post('/general', 'TimelineController@updateGeneralPageSettings');
    Route::get('/privacy', 'TimelineController@privacyPageSettings');
    Route::post('/privacy', 'TimelineController@updatePrivacyPageSettings');
    Route::get('/roles', 'TimelineController@rolesPageSettings');
    Route::get('/likes', 'TimelineController@likesPageSettings');
});

Route::group(['prefix' => '/{username}/group-settings', 'middleware' => ['auth', 'editgroup']], function ($username) {
    Route::get('/general', 'TimelineController@userGroupSettings');
    Route::post('/general', 'TimelineController@updateUserGroupSettings');
    Route::get('/closegroup', 'TimelineController@userGroupSettings');
    Route::get('/join-requests/{group_id}', 'TimelineController@getJoinRequests');
});

/*
|--------------------------------------------------------------------------
| Ajax Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for ajax.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'ajax', 'middleware' => ['auth']], function () {
    Route::post('create-post', 'TimelineController@createPost');
    Route::post('get-youtube-video', 'TimelineController@getYoutubeVideo');
    Route::post('like-post', 'TimelineController@likePost');
    Route::post('follow-post', 'TimelineController@follow');
    Route::post('notify-user', 'TimelineController@getNotifications');
    Route::post('post-comment', 'TimelineController@postComment');
    Route::post('page-like', 'TimelineController@pageLike');
    Route::post('change-avatar', 'TimelineController@changeAvatar');
    Route::post('change-cover', 'TimelineController@changeCover');
    Route::post('comment-like', 'TimelineController@likeComment');
    Route::post('comment-delete', 'TimelineController@deleteComment');
    Route::post('post-delete', 'TimelineController@deletePost');
    Route::post('page-delete', 'TimelineController@deletePage');
    Route::post('share-post', 'TimelineController@sharePost');
    Route::post('page-liked', 'TimelineController@pageLiked');
    Route::post('get-soundcloud-results', 'TimelineController@getSoundCloudResults');
    Route::post('join-group', 'TimelineController@joiningGroup');
    Route::post('join-close-group', 'TimelineController@joiningClosedGroup');
    Route::post('join-accept', 'TimelineController@acceptJoinRequest');
    Route::post('join-reject', 'TimelineController@rejectJoinRequest');
    Route::post('follow-accept', 'UserController@acceptFollowRequest');
    Route::post('follow-reject', 'UserController@rejectFollowRequest');
    Route::get('get-more-posts', 'TimelineController@getMorePosts');
    Route::get('get-more-feed', 'TimelineController@showFeed');
    Route::get('get-global-feed', 'TimelineController@showGlobalFeed');
    Route::post('add-memberGroup', 'UserController@addingMembersGroup');
    Route::post('get-users', 'UserController@getUsersJoin');
    Route::get('get-users-mentions', 'UserController@getUsersMentions');
    Route::post('groupmember-remove', 'TimelineController@removeGroupMember');
    Route::post('group-join', 'TimelineController@timelineGroups');
    Route::post('report-post', 'TimelineController@reportPost');
    Route::post('follow-user-confirm', 'TimelineController@userFollowRequest');
    Route::post('post-message/{id}', 'MessageController@update');
    Route::post('chat-delete', 'MessageController@removeChat');
    Route::post('create-message', 'MessageController@store');
    Route::post('page-report', 'TimelineController@pageReport');
    Route::post('get-notifications', 'UserController@getNotifications');
    Route::post('get-unread-notifications', 'UserController@getUnreadNotifications');
    Route::post('get-messages', 'MessageController@getMessages');
    Route::post('get-message/{id}', 'MessageController@getMessage');
    Route::post('get-conversation/{id}', 'MessageController@show');
    Route::post('get-private-conversation/{userId}', 'MessageController@getPrivateConversation');
    Route::post('get-unread-message', 'UserController@getUnreadMessage');
    Route::post('get-unread-messages', 'MessageController@getUnreadMessages');
    Route::post('pagemember-remove', 'TimelineController@removePageMember');
    Route::post('get-users-modal', 'UserController@getUsersModal');
    Route::post('edit-post', 'TimelineController@editPost');
    Route::get('load-emoji', 'TimelineController@loadEmoji');
    Route::post('update-post', 'TimelineController@updatePost');
    Route::post('/mark-all-notifications', 'NotificationController@markAllRead');
    Route::post('add-page-members', 'UserController@addingMembersPage');
    Route::post('get-members-join', 'UserController@getMembersJoin');
    Route::post('announce-delete', 'AdminController@removeAnnouncement');
});

Route::get('/download', 'HomeController@getDownload');


/*
|--------------------------------------------------------------------------
| Image routes
|--------------------------------------------------------------------------
*/

Route::get('user/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/avatars/'.$filename)->response();
});

Route::get('user/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/covers/'.$filename)->response();
});

Route::get('user/gallery/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/users/gallery/'.$filename)->response();
});

Route::get('page/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/pages/avatars/'.$filename)->response();
});

Route::get('page/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/pages/covers/'.$filename)->response();
});

Route::get('group/avatar/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/groups/avatars/'.$filename)->response();
});

Route::get('group/cover/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/groups/covers/'.$filename)->response();
});

Route::get('setting/{filename}', function ($filename) {
    return Image::make(storage_path().'/uploads/settings/'.$filename)->response();
});

Route::get('/page/{pagename}', 'PageController@page');
