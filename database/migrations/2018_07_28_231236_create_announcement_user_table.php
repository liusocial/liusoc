<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnnouncementUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('announcement_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('announcement_id')->unsigned()->index('announcement_user_announcement_id_foreign');
			$table->integer('user_id')->unsigned()->index('announcement_user_user_id_foreign');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('announcement_user');
	}

}
