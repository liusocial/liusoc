<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnnouncementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('announcements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('description', 65535);
			$table->string('image');
			$table->date('start_date');
			$table->date('end_date');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('user_id')->unsigned()->index('u-fk');
			$table->integer('school_id')->index('scf');
			$table->integer('value')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('announcements');
	}

}
