<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id')->unsigned()->nullable()->index('notifications_post_id_foreign');
			$table->integer('timeline_id')->unsigned()->nullable()->index('notifications_timeline_id_foreign');
			$table->integer('user_id')->unsigned()->index('notifications_user_id_foreign');
			$table->integer('notified_by')->unsigned()->index('notifications_notified_by_foreign');
			$table->boolean('seen')->default(0);
			$table->text('description', 65535);
			$table->string('type', 250);
			$table->string('link', 250)->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('notifications');
	}

}
