<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_likes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('page_id')->unsigned()->index('page_likes_page_id_foreign');
			$table->integer('user_id')->unsigned()->index('page_likes_user_id_foreign');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_likes');
	}

}
