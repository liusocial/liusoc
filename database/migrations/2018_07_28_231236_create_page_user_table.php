<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('page_id')->unsigned()->index('page_user_page_id_foreign');
			$table->integer('user_id')->unsigned()->index('page_user_user_id_foreign');
			$table->integer('role_id')->unsigned()->index('page_user_role_id_foreign');
			$table->boolean('active');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_user');
	}

}
