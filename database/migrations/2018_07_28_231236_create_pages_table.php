<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('timeline_id')->unsigned()->index('pages_timeline_id_foreign');
			$table->text('address', 65535);
			$table->boolean('active')->default(1);
			$table->integer('category_id')->unsigned()->index('pages_category_id_foreign');
			$table->integer('school_id')->index('school_id');
			$table->string('message_privacy', 15);
			$table->string('member_privacy', 15);
			$table->string('phone', 15);
			$table->string('timeline_post_privacy', 15);
			$table->string('website');
			$table->boolean('verified')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
