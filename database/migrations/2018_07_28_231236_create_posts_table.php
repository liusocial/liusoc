<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('description', 65535);
			$table->integer('timeline_id')->unsigned()->index('posts_timeline_id_foreign');
			$table->integer('user_id')->unsigned()->index('posts_user_id_foreign');
			$table->boolean('active')->default(1);
			$table->string('soundcloud_title', 250);
			$table->string('soundcloud_id');
			$table->string('youtube_title');
			$table->string('youtube_video_id', 250);
			$table->string('location', 250);
			$table->string('type', 100);
			$table->timestamps();
			$table->softDeletes();
			$table->string('link');
            $table->integer('share_post')->nullable();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
