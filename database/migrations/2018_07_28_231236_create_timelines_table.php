<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimelinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('timelines', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username', 250)->unique();
			$table->string('name', 250);
			$table->text('about', 65535);
			$table->integer('avatar_id')->unsigned()->nullable()->index('timelines_avatar_id_foreign');
			$table->integer('cover_id')->unsigned()->nullable()->index('timelines_cover_id_foreign');
			$table->string('cover_position');
			$table->enum('type', array('user','page','group'));
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('timelines');
	}

}
