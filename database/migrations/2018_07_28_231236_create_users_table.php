<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('timeline_id')->unsigned()->index('users_timeline_id_foreign');
			$table->string('email', 250);
			$table->string('verification_code', 250);
			$table->boolean('verified')->default(0);
			$table->boolean('email_verified');
			$table->string('remember_token', 250);
			$table->string('password', 250);
			$table->float('balance')->default(0.00);
			$table->date('birthday');
			$table->string('city', 100);
			$table->string('country', 100);
			$table->enum('gender', array('male','female','other'));
			$table->boolean('active')->default(1);
			$table->timestamp('last_logged')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->integer('school_id')->index('dp_fk');
			$table->integer('st_id')->nullable();
			$table->string('language', 15)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->unique(['id','timeline_id','school_id'], 'id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
