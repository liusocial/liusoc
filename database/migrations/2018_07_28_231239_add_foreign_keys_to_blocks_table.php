<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBlocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('blocks', function(Blueprint $table)
		{
			$table->foreign('timeline_id', 'timefk')->references('id')->on('timelines')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('timeline_report_id', 'timrefk')->references('id')->on('timeline_reports')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('blocks', function(Blueprint $table)
		{
			$table->dropForeign('timefk');
			$table->dropForeign('timrefk');
		});
	}

}
