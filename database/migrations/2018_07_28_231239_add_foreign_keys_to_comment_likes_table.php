<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCommentLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('comment_likes', function(Blueprint $table)
		{
			$table->foreign('comment_id')->references('id')->on('comments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('comment_likes', function(Blueprint $table)
		{
			$table->dropForeign('comment_likes_comment_id_foreign');
			$table->dropForeign('comment_likes_user_id_foreign');
		});
	}

}
