<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPageUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('page_user', function(Blueprint $table)
		{
			$table->foreign('page_id')->references('id')->on('pages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('role_id')->references('id')->on('roles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('page_user', function(Blueprint $table)
		{
			$table->dropForeign('page_user_page_id_foreign');
			$table->dropForeign('page_user_role_id_foreign');
			$table->dropForeign('page_user_user_id_foreign');
		});
	}

}
