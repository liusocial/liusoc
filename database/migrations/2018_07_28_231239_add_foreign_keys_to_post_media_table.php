<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPostMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('post_media', function(Blueprint $table)
		{
			$table->foreign('media_id')->references('id')->on('media')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('post_id')->references('id')->on('posts')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('post_media', function(Blueprint $table)
		{
			$table->dropForeign('post_media_media_id_foreign');
			$table->dropForeign('post_media_post_id_foreign');
		});
	}

}
