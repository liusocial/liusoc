<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTimelineReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('timeline_reports', function(Blueprint $table)
		{
			$table->foreign('reporter_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('timeline_id')->references('id')->on('timelines')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('timeline_reports', function(Blueprint $table)
		{
			$table->dropForeign('timeline_reports_reporter_id_foreign');
			$table->dropForeign('timeline_reports_timeline_id_foreign');
		});
	}

}
