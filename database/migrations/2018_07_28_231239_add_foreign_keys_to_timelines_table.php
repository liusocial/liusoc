<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTimelinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('timelines', function(Blueprint $table)
		{
			$table->foreign('avatar_id')->references('id')->on('media')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('cover_id')->references('id')->on('media')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('timelines', function(Blueprint $table)
		{
			$table->dropForeign('timelines_avatar_id_foreign');
			$table->dropForeign('timelines_cover_id_foreign');
		});
	}

}
