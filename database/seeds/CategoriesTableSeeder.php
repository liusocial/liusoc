<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [

        'Administrative'                   => 'Administrative',
        'Academic'                         => 'Academic',
        'Entertainment'                    => 'Entertainment',
        'Event'                            => 'Event',
        'Activities'                       => 'Activities',
        'Awareness'                        => 'Awareness',
        'Club'                             => 'Club',

];

        $i = 1;
        foreach ($categories as $key) {
            $category = Category::firstOrNew(['name' => $key, 'description' => 'description about '.$key, 'active' => '1', 'parent_id' => $i]);
            $category->save();
            $i++;
        }
    }
}
