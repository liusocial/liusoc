<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Config::get('app.env') == 'demo' || Config::get('app.env') == 'local') {

            $this->call(CategoriesTableSeeder::class);
            $this->call(RolesTableSeeder::class);
            $this->call(InstallerSeeder::class);
            $this->call(StaticpageTableSeeder::class);
            // $this->call(InstallerSeeder::class);
        } else {

            $this->call(CategoriesTableSeeder::class);
            $this->call(RolesTableSeeder::class);
            $this->call(InstallerSeeder::class);
            $this->call(StaticpageTableSeeder::class);
        }
    }
}
