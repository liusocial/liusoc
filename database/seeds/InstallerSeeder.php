<?php

use App\Setting;
use App\Timeline;
use App\User;
use Illuminate\Database\Seeder;

class InstallerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('schools')->insert([
            [
            'id' => '1',
            'name' => 'General',
            ],
        [
        'id' => '2',
            'name' => 'Art'
        ],

        ['id' => '3',
                'name' => 'Engineering']
        ,
            [
                'id' => '4',
                'name' => 'IT'],
            ['id' => '5',
                'name' => 'Pharmacy'],
            ['id' => '6',
                'name' => 'English'],

            ['id' => '7',
                'name' => 'business'],
            ['id' => '8',
                'name' => 'Student Canter']]
        );



        //Create admin account
        $account = Timeline::firstOrNew(['username' => 'admin1']);
        $account->username = 'admin1';
        $account->name = 'admin1';
        $account->about = 'Some text about me';
        $account->type = 'user';
        $account->save();


        $account = Timeline::firstOrNew(['username' => 'mnabood']);
        $account->username = 'mnabood';
        $account->name = 'Mohammed abood';
        $account->about = 'Some text about me';
        $account->type = 'user';
        $account->save();

        $account = Timeline::firstOrNew(['username' => 'medeliwah']);
        $account->username = 'medeliwah';
        $account->name = 'Mohammed Eliwah';
        $account->about = 'Some text about me';
        $account->type = 'user';
        $account->save();

        $account = Timeline::firstOrNew(['username' => 'jouzifa']);
        $account->username = 'jouzifa';
        $account->name = 'Mohammed Juzaifah';
        $account->about = 'Some text about me';
        $account->type = 'user';
        $account->save();


        $user = User::create([
            'timeline_id'       => 1,
            'email'             => 'admin@admin.com',
            'school_id'             => '1',
            'verified'             => '1',
            'verification_code' => str_random(18),
            'remember_token'    => str_random(10),
            'password'          => Hash::make('123456'),
            'city'              => 'sanaa',
            'country'           => 'yemen',
            'gender'            => 'male',
            'email_verified'    => 1,
        ]);

        $user = User::create([
            'timeline_id'       => 2,
            'email'             => 'abood@abood.com',
            'verification_code' => str_random(18),
            'school_id'             => '1',
            'remember_token'    => str_random(10),
            'password'          => Hash::make('123456'),
            'city'              => 'sanaa',
            'country'           => 'yemen',
            'verified'             => '1',
            'gender'            => 'male',
            'email_verified'    => 1,
        ]);

        $user = User::create([
            'timeline_id'       => 3,
            'email'             => 'eliwah@eliwah.com',
            'verification_code' => str_random(18),
            'school_id'             => '1',
            'remember_token'    => str_random(10),
            'password'          => Hash::make('123456'),
            'city'              => 'sanaa',
            'country'           => 'yemen',
            'verified'             => '1',
            'gender'            => 'male',
            'email_verified'    => 1,
        ]);

        $user = User::create([
            'timeline_id'       => 4,
            'email'             => 'jouzifa@juzifa.com',
            'verification_code' => str_random(18),
            'school_id'             => '1',
            'remember_token'    => str_random(10),
            'password'          => Hash::make('123456'),
            'city'              => 'sanaa',
            'country'           => 'yemen',
            'verified'             => '1',
            'gender'            => 'male',
            'email_verified'    => 1,
        ]);


        DB::table('role_user')->insert([

            ['user_id' => '1',
                'role_id' => '1'],
            ['user_id' => '2',
                'role_id' => '5'],
            ['user_id' => '3',
                'role_id' => '5'],
            ['user_id' => '4',
                'role_id' => '5'],
        ]);


        DB::table('user_settings')->insert([[
            'user_id'               => '1',
            'confirm_follow'        => 'no',
            'follow_privacy'        => 'everyone',
            'comment_privacy'       => 'everyone',
            'timeline_post_privacy' => 'everyone',
            'post_privacy'          => 'everyone', ],

        ['user_id'                  => '2',
            'confirm_follow'        => 'no',
            'follow_privacy'        => 'everyone',
            'comment_privacy'       => 'everyone',
            'timeline_post_privacy' => 'everyone',
            'post_privacy'          => 'everyone', ],

        ['user_id'                   => '3',
            'confirm_follow'        => 'no',
            'follow_privacy'        => 'everyone',
            'comment_privacy'       => 'everyone',
            'timeline_post_privacy' => 'everyone',
            'post_privacy'          => 'everyone', ],

        ['user_id'                  => '4',
            'confirm_follow'        => 'no',
            'follow_privacy'        => 'everyone',
            'comment_privacy'       => 'everyone',
            'timeline_post_privacy' => 'everyone',
            'post_privacy'          => 'everyone', ]]

    );




        //Create default website settings

        $settings = ['comment_privacy'                => 'everyone',
                        'confirm_follow'              => 'no',
                        'follow_privacy'              => 'everyone',
                        'user_timeline_post_privacy'  => 'everyone',
                        'post_privacy'                => 'everyone',
                        'page_message_privacy'        => 'everyone',
                        'page_timeline_post_privacy'  => 'everyone',
                        'page_member_privacy'         => 'only_admins',
                        'member_privacy'              => 'only_admins',
                        'group_timeline_post_privacy' => 'members',
                        'group_member_privacy'        => 'only_admins',
                        'site_name'                   => 'LIUsocial',
                        'site_title'                  => 'LIUsocial',
                        'site_url'                    => 'social.liu-ye.com',
                        'twitter_link'                => 'http://twitter.com/',
                        'facebook_link'               => 'http://facebook.com/',
                        'youtube_link'                => 'http://youtube.com/',
                        'support_email'               => 'admin@liusocial.com',
                        'mail_verification'           => 'off',
                        'captcha'                     => 'off',
                        'censored_words'              => '',
                        'birthday'                    => 0,
                        'city'                        => 0,
                        'about'                       => 0,
                        'contact_text'                => 'Contact page description can be here',
                        'address_on_mail'             => 'liusocial,<br> sanaa,<br> yemen',
                        'items_page'                  => '10',
                        'min_items_page'              => '5',
                        ];

        foreach ($settings as $key => $value) {
            $settings = Setting::firstOrNew(['key' => $key, 'value' => $value]);
            $settings->save();
        }
    }
}
