<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Create admin role
        $role = Role::firstOrNew(['name' => 'admin']);
        $role->display_name = 'Admin';
        $role->description = 'Access to everything';
        $role->save();

        //Create student role
        $role = Role::firstOrNew(['name' => 'student']);
        $role->display_name = 'Student';
        $role->description = 'Access limited to user';
        $role->save();

        //Create dean role
        $role = Role::firstOrNew(['name' => 'dean']);
        $role->display_name = 'Dean';
        $role->description = 'Access limited to dean controls';
        $role->save();

        //Create instructor role
        $role = Role::firstOrNew(['name' => 'instructor']);
        $role->display_name = 'Instructor';
        $role->description = 'Access limited to instructor';
        $role->save();

        //Create moderator role
        $role = Role::firstOrNew(['name' => 'devloper']);
        $role->display_name = 'Devloper';
        $role->description = 'Access limited to moderator';
        $role->save();

        //Create validator role
        $role = Role::firstOrNew(['name' => 'validator']);
        $role->display_name = 'validator';
        $role->description = 'Access limited to validator';
        $role->save();
    }
}
