
$(function () {

    $('.login-form').ajaxForm({
        url: SP_source() + 'login',

        beforeSend: function() {
            login_form = $('.login-form');
            login_button = login_form.find('.btn-submit');
            login_button.attr('disabled', true);
            $('.login-progress').removeClass('hidden');
            $('.login-errors').html('');
        },

        success: function(responseText) {

            login_button.attr('disabled', false);
            $('.login-progress').addClass('hidden');
            if (responseText.status == 200) {
                window.location = SP_source();
            } else {
                $('.login-errors').html(responseText.message);

            }

        }
    });



    $('.signup-form').ajaxForm({
        url: SP_source() + 'register',

        beforeSend: function() {
            signup_form = $('.signup-form');
            signup_button = signup_form.find('.StaticLoggedOutHomePage-buttonSignup');
            signup_button.attr('disabled', true);
            $('.login-progress').removeClass('hidden');
            // signup_button.attr('disabled', true).append(' <i class="fa fa-spinner fa-pulse "></i>');
        },

        success: function(responseText) {
            // console.log(jQuery.parseJSON(responseText));
            signup_button.attr('disabled', false).find('.fa-spinner').addClass('hidden');
            $('.login-progress').addClass('hidden');
            if (responseText.status == 200){
                $('.signup-errors').html('');
                // alert(responseText.message);
                $('.signup-form')[0].reset();

                $('.signup-suc').append('<li CLASS="fa fa-check">'+ responseText.message + '</li>');

            }  else {
                //    var y = document.getElementsByClassName('form1')[0];
                // var x = document.getElementsByClassName('sign_suc')[0];

                //  var s=responseText.err_result;
                // $('.signup-errors').html('');

                $('.signup-errors').html('');
                $.each(responseText.err_result, function(key, value) {
                    $('.signup-errors').append('<li>'+ value[0] + '</li>');
                });
                // To reset form fields

                // y.style.display = "none";
                //
                // x.style.display = "block";
                // $.each(responseText.message, function(key, value) {
                //     $('.sign_suc').append('<li>'+ value[0] + '</li>');
                // });
                //  $('.re-suc').append(s[0]);
            }

        }
    });


});
