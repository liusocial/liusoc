<form action="{{ url('') }}" method="post" class="create-post-form">
  {{ csrf_field() }}

    <div class="panel panel-default panel-create"> <!-- panel-create -->


        <div class="panel-heading"style="border-top-right-radius: 50px;

border-top-left-radius: 50px;">


                                @if(Auth::user()->language =='ar')
                                    <div class="heading-text" dir="rtl">
                                        @else
                                            <div class="heading-text" dir="ltr">
                                                @endif

                {{ trans('messages.whats-going-on') }}
            </div>
        </div>
                            @if(Auth::user()->language =='ar')
                                <div class="panel-body" dir="rtl">
                                    @else
                                        <div class="panel-body" dir="ltr">
                                            @endif

            <textarea name="description" class="form-control createpost-form comment" cols="30" rows="3" id="createPost" cols="30" rows="2" placeholder="{{ trans('messages.post-placeholder') }}"></textarea>
               

                <div class="user-tags-added" style="display:none">
                    &nbsp; -- {{ trans('common.with') }}
                    <div class="user-tag-names">
                        
                    </div>
                </div>
                <div class="user-tags-addon post-addon" style="display: none">
                    <span class="post-addon-icon"><i class="fa fa-user-plus"></i></span>
                    <div class="form-group">
                        <input type="text" id="userTags" class="form-control user-tags youtube-text" placeholder="{{ trans('messages.who_are_you_with') }}" autocomplete="off" value="" >
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="users-results-wrapper"></div>
                <div class="youtube-iframe"></div>
        
                <div class="video-addon post-addon" style="display: none">
                    <span class="post-addon-icon"><i class="fa fa-film"></i></span>
                    <div class="form-group">
                        <input type="text" name="youtubeText" id="youtubeText" class="form-control youtube-text" placeholder="{{ trans('messages.what_are_you_watching') }}"  value="" >
                        <div class="clearfix"></div>
                    </div>
                </div>
            <div class="file-addon post-addon" style="display: none">
                <span class="post-addon-icon"><i class="fa fa-file"></i></span>
                <div class="form-group">
                    <input type="text" name="link" id="link" class="form-control youtube-text" placeholder="{{ trans('messages.upload_file') }}"  value="" >
                    <div class="clearfix"></div>
                </div>
            </div>
              <div class="music-addon post-addon" style="display: none">
                  <span class="post-addon-icon"><i class="fa fa-music" aria-hidden="true"></i></span>
                 <div class="form-group">
                    <input type="text" name="soundCloudText" autocomplete="off" id ="soundCloudText" class="form-control youtube-text" placeholder="{{ trans('messages.what_are_you_listening_to') }}"  value="" >
                    <div class="clearfix"></div>
                 </div>
              </div>
              <div class="soundcloud-results-wrapper">

              </div>
            <div class="location-addon post-addon" style="display: none">
                  <span class="post-addon-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                 <div class="form-group">
                    <input type="text" name="location" id="pac-input" class="form-control" placeholder="{{ trans('messages.where_are_you') }}"  autocomplete="off" value="" onKeyPress="return initMap(event)"><div class="clearfix"></div>
                 </div>                 
            </div>
              <div class="emoticons-wrapper  post-addon" style="display:none">
                  
              </div>
              <div class="images-selected post-images-selected" style="display:none">
                  <span>3</span> {{ trans('common.photo_s_selected') }}
              </div>
              <!-- Hidden elements  -->
              <input type="hidden" name="timeline_id" value="{{ $timeline->id }}">
              <input type="hidden" name="youtube_title" value="">
              <input type="hidden" name="youtube_video_id" value="">
              <input type="hidden" name="locatio" value="">
            <input type="hidden" name="link_id" value="">
              <input type="hidden" name="soundcloud_id" value="">
              <input type="hidden" name="user_tags" value="">
              <input type="hidden" name="soundcloud_title" value="">
              <input type="file"   class="post-images-upload hidden" multiple="multiple"  accept="image/jpeg,image/png,image/gif" name="post_images_upload[]" >
              <div id="post-image-holder"></div>
        </div><!-- panel-body -->

        <div class="panel-footer">

            <ul class="list-inline left-list">
                <li><a href="#" id="addUserTags"><i class="fa fa-user-plus"></i></a></li>
                <li><a href="#" id="imageUpload"><i class="fa fa-camera-retro"></i></a></li>
                <li><a href="#" id="musicUpload"><i class="fa fa-music"></i></a></li>
                <li><a href="#" id="fileUpload"><i class="fa fa-file"></i></a></li>
                <li><a href="#" id="videoUpload"><i class="fa fa-film"></i></a></li>
                <li><a href="#" id="locationUpload"><i class="fa fa-map-marker"></i></a></li>
                <li><a href="#" id="emoticons"><i class="fa fa-smile-o"></i></a></li>
            </ul>
            {{--@foreach($block_users as $block)--}}
                {{--<li><span class="label label-default">{{  $block->id}}</span></li>--}}
            {{--@endforeach--}}
            {{--@if($block_users != null)--}}
                {{--@foreach($block_user as $block)--}}
                    {{--@if($block_user->chkBlockUser(Auth::user()->username) == "existed")--}}
            @if($block_user !=null)
                @if($block_user->chkBlockExpire($block_user->id) == "notexpired")
                    <ul class="list-inline right-list">
                        <li><span class="label label-default">{{ trans('common.you_are_blocked') }}</span></li>
                        {{--<span class="post-addon-icon"><i aria-hidden="true"></i>{{ trans('common.you_are_blocked') }}</span>--}}
                    </ul>
                    @else
                        <ul class="list-inline right-list">
                            <li><button type="submit" class="btn btn-submit btn-success">{{ trans('common.post_notexpired') }}</button></li>
                        </ul>
                @endif
            @else
                {{--@elseif($block_user->chkBlockUser(Auth::user()->username) != "existed")--}}
                    <ul class="list-inline right-list">
                        <li><button type="submit" class="btn btn-submit btn-success">{{ trans('common.post') }}</button></li>
                    </ul>
                @endif
                {{--@endforeach--}}


            {{--@if($block_user != null)--}}
                {{--<ul class="list-inline right-list">--}}
                    {{--<li><button type="submit" class="btn btn-submit btn-success">{{ trans('common.post') }}</button></li>--}}
                {{--</ul>--}}
            {{--@endif--}}
            <div class="clearfix"></div>
        </div>
    </div>
</form>


@if(Setting::get('postcontent_ad') != NULL)
    <div id="link_other" class="page-image">
        {!! htmlspecialchars_decode(Setting::get('postcontent_ad')) !!}
    </div>
@endif


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_vuWi_hzMDDeenNYwaNAj0PHzzS2GAx8&libraries=places&callback=initMap"
        async defer></script>

<script>
function initMap(event) 
{    
    var key;  
    var map = new google.maps.Map(document.getElementById('pac-input'), {
    });

    var input = /** @type {!HTMLInputElement} */(
        document.getElementById('pac-input'));        

    if(window.event)
    {
        key = window.event.keyCode; 

    }
    else 
    {
        if(event)
            key = event.which;      
    }       

    if(key == 13){       
    //do nothing 
    return false;       
    //otherwise 
    } else { 
        var autocomplete = new google.maps.places.Autocomplete(input);  
        autocomplete.bindTo('bounds', map);

    //continue as normal (allow the key press for keys other than "enter") 
    return true; 
    } 
}
</script>




