<div class="list-group list-group-navigation liusocial-group">
    <a href="{{ url('/devloper') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ (Request::segment(1) == 'devloper' && Request::segment(2)==null) ? 'active' : '' }}">
            <i class="fa fa-dashboard"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.dashboard') }}
            <div class="text-muted">
                {{ trans('common.application_statistics') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/requests') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'requests' ? 'active' : '' }}">
            <i class="fa fa fa fa-check-circle"><span class="fa fa-remove"></span></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.account_req') }}
            <div class="text-muted">
                {{ trans('common.account_req_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/general-settings') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'general-settings' ? 'active' : '' }}">
            <i class="fa fa-shield"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.website_settings') }}
            <div class="text-muted">
                {{ trans('common.general_website_settings') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/user-settings') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'user-settings' ? 'active' : '' }}">
            <i class="fa fa-user-secret"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.user_settings') }}
            <div class="text-muted">
                {{ trans('common.user_settings_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/custom-pages') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'custom-pages' ? 'active' : '' }}">
            <i class="fa fa-files-o"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.custom_pages') }}
            <div class="text-muted">
                {{ trans('common.custom_pages_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
<!--
    <a href="{{ url('/devloper/themes') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'themes' ? 'active' : '' }}">
            <i class="fa fa-picture-o"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.themes') }}
        <div class="text-muted">
{{ trans('common.themes_text') }}
        </div>
    </div>
    <span class="clearfix"></span>
</a>
-->
    <a href="{{ url('/devloper/page-settings') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'page-settings' ? 'active' : '' }}">
            <i class="fa fa-comments"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.page_settings') }}
            <div class="text-muted">
                {{ trans('common.page_settings_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/group-settings') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'group-settings' ? 'active' : '' }}">
            <i class="fa fa-group"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.group_settings') }}
            <div class="text-muted">
                {{ trans('common.group_settings_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/announcements') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'announcements' ? 'active' : '' }}">
            <i class="fa fa-bullhorn"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.announcements') }}
            <div class="text-muted">
                {{ trans('common.announcements_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>

    <a href="{{ url('/devloper/users') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'users' ? 'active' : '' }}">
            <i class="fa fa-user-plus"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.manage_users') }}
            <div class="text-muted">
                {{ trans('common.manage_users_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>

    <a href="{{ url('/devloper/users/show') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'users' ? 'active' : '' }}">
            <i class="fa fa-user-md"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.manage_deactivate_users') }}
            <div class="text-muted">
                {{ trans('common.manage_deactivate_users_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>

    <a href="{{ url('/devloper/pages') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'pages' ? 'active' : '' }}">
            <i class="fa fa-file-text"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.manage_pages') }}
            <div class="text-muted">
                {{ trans('common.manage_pages_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/groups') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'groups' ? 'active' : '' }}">
            <i class="fa fa-group"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.manage_groups') }}
            <div class="text-muted">
                {{ trans('common.manage_groups_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>

    <a href="{{ url('/devloper/manage-reports') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'manage-reports' ? 'active' : '' }}">
            <i class="fa fa-bug"></i>
        </div>

        <div class="list-text">
            @if(Auth::user()->getReportsCount() > 0)
                <span class="badge pull-right">{{ Auth::user()->getReportsCount() }}</span>
            @endif
            {{ trans('common.manage_reports') }}
            <div class="text-muted">
                {{ trans('common.manage_reports_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>

    {{--<a href="{{ url('/devloper/manage-ads') }}" class="list-group-item">--}}

    {{--<div class="list-icon liusocial-icon {{ Request::segment(2) == 'manage-ads' ? 'active' : '' }}">--}}
    {{--<i class="fa fa-send"></i>--}}
    {{--</div>--}}
    {{--<div class="list-text">--}}
    {{--<span class="badge pull-right"></span>--}}
    {{--{{ trans('common.manage_ads') }}--}}
    {{--<div class="text-muted">--}}
    {{--{{ trans('common.manage_ads_text') }}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<span class="clearfix"></span>--}}
    {{--</a>--}}
    <a href="{{ url('/devloper/get-env') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'get-env' ? 'active' : '' }}">
            <i class="fa fa-cogs"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.environment_settings') }}
            <div class="text-muted">
                {{ trans('common.edit_on_risk') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
    <a href="{{ url('/devloper/update-database') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(2) == 'update-database' ? 'active' : '' }}">
            <i class="fa fa-database"></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            Database Update
            <div class="text-muted">
                database update
            </div>
        </div>
        <span class="clearfix"></span>
    </a>
</div>



