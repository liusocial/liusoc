<!-- Modal starts here-->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog modal-likes" role="document">
        <div class="modal-content">
        	<i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>
</div>

	<div class="footer-description" style="

">
		@if (Auth::guest())		<div class="liusocial-terms text-center">

		@elseif(Auth::user()->hasRole('admin'))
			<div class="liusocial-terms text-center">
				@elseif(Auth::user()->hasRole('dean'))
					<div class="liusocial-terms text-center" style="background: #02647B">

						@else
							<div class="liusocial-terms text-center">

							@endif

			@if(Auth::check())
				{{--<a href="{{ url('contact') }}">{{ trans('common.contact') }}</a> ---}}
				<a href="{{ url('/'.Auth::user()->username.'/settings/general') }}">{{ trans('common.settings') }}</a> -
				<a href="{{ url('messages') }}">{{ trans('common.messages') }}</a> -
									@if(Auth::user()->hasRole('admin'))
										<a href="{{ url('/download/adn/admin_guide.pdf') }}">{{ trans('common.Download_guide_admin') }}</a>
									@elseif(Auth::user()->hasRole('dean'))
										<a href="{{ url('dean_guide.pdf') }}">{{ trans('common.Download_guide_dean') }}</a>
									@elseif(Auth::user()->hasRole('devloper'))
										<a href="{{ url('/download/adn/admin_guide.pdf') }}">{{ trans('common.Download_guide_admin') }}</a>
									@else
										<a href="{{ url('/download/user_guide.pdf') }}">{{ trans('common.Download_guide_all') }}</a>
									@endif


							<a class="" href="{{ url('/') }}">
								<img class="liusocial-logo" src="{!! url('setting/'.Setting::get('logo')) !!}" alt="{{ Setting::get('site_name') }}" title="{{ Setting::get('site_name') }}">
							</a>
			@else
				<a href="{{ url('login') }}">{{ trans('auth.login') }}</a> -
				<a href="{{ url('login') }}">{{ trans('auth.register') }}</a><a class="" href="{{ url('/') }}">
								<img class="liusocial-logo" src="{!! url('setting/'.Setting::get('logo')) !!}" alt="{{ Setting::get('site_name') }}" title="{{ Setting::get('site_name') }}">
							</a>
			@endif
			@foreach(App\StaticPage::get() as $staticpage)
				- <a href="{{ url('page/'.$staticpage->slug) }}">{{ $staticpage->title }}</a>
		    @endforeach
		    {{--<a href="{{url('/contact')}}"> - {{ trans('common.contact') }}</a>--}}
		</div>
					@if (Auth::guest())<div class="liusocial-terms text-center">
					@elseif(Auth::user()->hasRole('admin'))
						<div class="liusocial-terms text-center">
							@elseif(Auth::user()->hasRole('dean'))
								<div class="liusocial-terms text-center" style=" ">

									@else
										<div class="liusocial-terms text-center">


									@endif
											@if(Auth::check())
									<div style="    display: contents;
    color: #585555cc;
"> Version:1.1 &nbsp; Built By:
									</div>      <a href="{{url('/medeliwah') }}"  style= "color: #5379A2 !important;font-weight: 501;">{{ trans('common.elewah') }}</a> -&nbsp;
												<a href="{{ url('/jouzifa') }}" style= "color: #5379A2 !important;font-weight: 501;">{{ trans('common.juzaifah') }}</a> -&nbsp;
												<a href="{{url('/mnabood')}}" style= "color: #5379A2 !important;font-weight: 501;" >{{ trans('common.abood') }}</a> &nbsp;
												<div style="    display: contents;
    color: #585555cc;
">	&copy; &nbsp;{{ date('Y') }}&nbsp; {{ trans('common.liu')}}	</div>

											@else
											<div style="    display: contents;
    color: #585555cc;
">Version:1.1 &nbsp; Built By:</div> {{ trans('common.developers') }}  <div style="    display: contents;
    color: #585555cc;
">	&copy; &nbsp;{{ date('Y') }}&nbsp; {{ trans('common.liu')}}	</div>
											@endif

		</div>
	</div>
</div>

							</div>

{!! Theme::asset()->container('footer')->usePath()->add('app', 'js/app.js') !!}