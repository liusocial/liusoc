<div class="list-group list-group-navigation liusocial-group">

    <a href="{{ url('/validator/requests') }}" class="list-group-item">

        <div class="list-icon liusocial-icon {{ Request::segment(1) == 'requests' ? 'active' : '' }}">
            <i class="fa fa fa fa-check-circle"><span class="fa fa-remove"></span></i>
        </div>
        <div class="list-text">
            <span class="badge pull-right"></span>
            {{ trans('common.account_req') }}
            <div class="text-muted">
                {{ trans('common.account_req_text') }}
            </div>
        </div>
        <span class="clearfix"></span>
    </a>

</div>



