
<div class="panel panel-default">
	<div class="panel-heading no-bg panel-settings">
	@include('flash::message')
		<h3 class="panel-title">
			{{ trans('admin.create_announcement') }}
		</h3>
	</div>
	<div class="panel-body">		
	@if($mode=="create")
		<form method="POST" class="liusocial-form" action="{{ url('admin/announcements') }}">
																@else
																		<form method="POST" class="socialite-form" action="{{ url('admin/announcements/'.$announcement->id.'/update') }}">

	@endif		    
	
	{{ csrf_field() }}
		<div class="form-horizontal announcements">
			<div class="form-group required ">
				{{--{{ Form::label('title', trans('admin.user id'), ['class' => 'col-sm-2 control-label']) }}--}}
				<div class="col-sm-10">
					<div class="col-sm-10">
						@if($mode == "create")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@elseif($mode=="create_business")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@elseif($mode=="create_art")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@elseif($mode=="create_it")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@elseif($mode=="create_medicine")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@elseif($mode=="create_eng")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@elseif($mode=="create_english")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@elseif($mode=="create_std")
							{{ Form::hidden('user_id', Auth::user()->id) }}

						@else
							{{ Form::hidden('user_id', $announcement->user_id) }}

						@endif
					</div>
				</div>
			</div>

			<div class="form-group required ">
				{{--{{ Form::label('School_id', trans('admin.school id'), ['class' => 'col-sm-2 control-label']) }}--}}
				<div class="col-sm-10">
					@if($mode == "create")
						{{ Form::hidden('school_id', Auth::user()->school_id) }}

					@elseif($mode=="create_business")
						{{ Form::hidden('school_id', '7') }}
					@elseif($mode=="create_art")
						{{ Form::hidden('school_id', '2') }}
						{{--{{ Form::text('school_id', '2', ['class' => 'form-control', 'readonly' => 'true']) }}--}}
					@elseif($mode=="create_it")
						{{ Form::hidden('school_id', '4') }}
					@elseif($mode=="create_medicine")
						{{ Form::hidden('school_id', '5') }}
					@elseif($mode=="create_eng")
						{{ Form::hidden('school_id', '3') }}
					@elseif($mode=="create_english")
						{{ Form::hidden('school_id', '6') }}
					@elseif($mode=="create_std")
						{{ Form::hidden('school_id', '8') }}

				</div>
				@else
					{{ Form::hidden('school_id', $announcement->school_id) }}
					{{--{{ Form::text('school_id', $announcement->school_id, ['class' => 'form-control', 'readonly' => 'true']) }}--}}
				@endif
				@if ($errors->has('title'))
					<span class="help-block">
					<strong>{{ $errors->first('title') }}</strong>
					</span>
				@endif
			</div>
		</div>

			<div class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
			    {{ Form::label('title', trans('admin.title'), ['class' => 'col-sm-2 control-label']) }}
			    <div class="col-sm-10">
			      @if($mode == "create")
			      	{{ Form::text('title',null,['class' => 'form-control']) }}

					@elseif($mode=="create_business")
						{{ Form::text('title',null,['class' => 'form-control']) }}
					@elseif($mode=="create_art")
						{{ Form::text('title',null,['class' => 'form-control']) }}
					@elseif($mode=="create_it")
						{{ Form::text('title',null,['class' => 'form-control']) }}
					@elseif($mode=="create_medicine")
						{{ Form::text('title',null,['class' => 'form-control']) }}
					@elseif($mode=="create_eng")
						{{ Form::text('title',null,['class' => 'form-control']) }}
					@elseif($mode=="create_english")
						{{ Form::text('title',null,['class' => 'form-control']) }}
					@elseif($mode=="create_std")
						{{ Form::text('title',null,['class' => 'form-control']) }}
			      @else
			      	{{ Form::text('title', $announcement->title, ['class' => 'form-control']) }}
			      @endif
			      
			      @if ($errors->has('title'))
			      <span class="help-block">
			      	<strong>{{ $errors->first('title') }}</strong>
			      </span>
			      @endif
			    </div>
			</div>
			<br>
			<br>
			<div class="form-group required {{ $errors->has('description') ? ' has-error' : '' }}">
			    {{ Form::label('description', trans('common.description'), ['class' => 'col-sm-2 control-label']) }}
			    <div class="col-sm-10">
			     	@if($mode =="create")
			     		{{ Form::textarea('description', null ,['class' => 'form-control']) }}

					@elseif($mode=="create_business")
						{{ Form::textarea('description', null ,['class' => 'form-control']) }}
					@elseif($mode=="create_art")
						{{ Form::textarea('description', null ,['class' => 'form-control']) }}
					@elseif($mode=="create_it")
						{{ Form::textarea('description', null ,['class' => 'form-control']) }}
					@elseif($mode=="create_medicine")
						{{ Form::textarea('description', null ,['class' => 'form-control']) }}
					@elseif($mode=="create_eng")
						{{ Form::textarea('description', null ,['class' => 'form-control']) }}
					@elseif($mode=="create_english")
						{{ Form::textarea('description', null ,['class' => 'form-control']) }}
					@elseif($mode=="create_std")
						{{ Form::textarea('description', null ,['class' => 'form-control']) }}
			     	@else
			     	{{ Form::textarea('description', $announcement->description, ['class' => 'form-control']) }}
			     	@endif

			     	@if ($errors->has('description'))
					<span class="help-block">
						<strong>{{ $errors->first('description') }}</strong>
					</span>
					@endif		     	
			    </div>
			</div>
			
			<div class="form-group required {{ $errors->has('start_date') || $errors->has('end_date') ? ' has-error' : '' }}">
				<div class="row">
					<div class="col-md-6">
					 	{{ Form::label('start_date', trans('admin.start_date'), ['class' => 'col-sm-4 control-label']) }}

					 	<div class="input-group date datepicker col-sm-8">
                            <span class="input-group-addon addon-left calendar-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                            @if($mode=="create")
                            	<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">

							@elseif($mode=="create_business")
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							@elseif($mode=="create_art")
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							@elseif($mode=="create_it")
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							@elseif($mode=="create_medicine")
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							@elseif($mode=="create_eng")
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							@elseif($mode=="create_english")
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							@elseif($mode=="create_std")
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
                            @else
                            	<input type="text" class="form-control" name="start_date" id="datepicker1" value="{{ $announcement->start_date }}">
                            @endif                            
                            <span class="input-group-addon addon-right angle-addon">
                                <span class="fa fa-angle-down"></span>
                            </span>
                        </div>
                        @if ($errors->has('start_date'))
                        <span class="help-block">
                        	<strong>{{ $errors->first('start_date') }}</strong>
                        </span>
                        @endif
					</div>
					<div class="col-md-6">
					 	{{ Form::label('end_date', trans('admin.end_date'), ['class' => 'col-sm-4 control-label']) }}
					 	<div class="input-group date datepicker col-sm-8">
                            <span class="input-group-addon addon-left calendar-addon">
                                <span class="fa fa-calendar"></span>
                            </span>                           
                            @if($mode=="create")
                            	<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							@elseif($mode=="create_business")
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							@elseif($mode=="create_art")
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							@elseif($mode=="create_it")
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							@elseif($mode=="create_medicine")
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							@elseif($mode=="create_eng")
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							@elseif($mode=="create_english")
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							@elseif($mode=="create_std")
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
                            @else
                            	<input type="text" class="form-control" name="end_date" id="datepicker2" value="{{ $announcement->end_date }}">
                            @endif 
                             <span class="input-group-addon addon-right angle-addon">
                                <span class="fa fa-angle-down"></span>
                            </span>
                        </div>
                        @if ($errors->has('end_date'))
                        <span class="help-block">
                        	<strong>{{ $errors->first('end_date') }}</strong>
                        </span>
                        @endif
                    </div>
				</div>
			</div>
			
			<div class="form-group">
			    <div class="text-center">
			      @if($mode=="create")
			      	<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
					@elseif($mode=="create_business")
						<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
					@elseif($mode=="create_art")
						<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
					@elseif($mode=="create_it")
						<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
					@elseif($mode=="create_medicine")
						<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
					@elseif($mode=="create_eng")
						<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
					@elseif($mode=="create_english")
						<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
					@elseif($mode=="create_std")
						<button type="submit" class="btn btn-success">{{ trans('common.create') }}</button>
			      @else
			      	<button type="submit" class="btn btn-success">{{ trans('common.save_changes') }}</button>
			      @endif
			    </div>
			</div>
		</div><!-- /announcements -->
		</form>
	</div>
</div>












