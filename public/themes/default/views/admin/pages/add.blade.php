<!-- <div class="main-content"> -->
@if(Auth::user()->hasRole('admin'))




            <div class="panel panel-default">
                <div class="panel-heading no-bg panel-settings">

                    <h3 class="panel-title">{{ trans('common.create_page') }}</h3>
                </div>
                @if($mode =="create")
                    <div class="panel-body">
                        <div class="liusocial-form">
                            @if(isset($message))
                                <div class="alert alert-success">
                                    {{ $message }}
                                </div>
                            @endif

                            <form class="margin-right" method="POST" action="{{ url('admin/pages') }}">
                                {{ csrf_field() }}

                                <fieldset class="form-group required {{ $errors->has('category') ? ' has-error' : '' }}">
                                    {{ Form::label('category', trans('common.category'), ['class' => 'control-label']) }}

                                    {{ Form::select('category', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control')) }}
                                    @if ($errors->has('category'))
                                        <span class="help-block">
										<strong>{{ $errors->first('category') }}</strong>
									</span>
                                    @endif

                                </fieldset>

                                <fieldset class="form-group">
                                    {{ Form::label('school', trans('common.school'), ['class' => 'control-label']) }}

                                    {{ Form::select('school_id', array('' => trans('common.select_school'))+ $school_options, '', array('class' => 'form-control')) }}
                                    @if ($errors->has('school'))
                                        <span class="help-block">
										<strong>{{ $errors->first('school') }}</strong>
									</span>
                                    @endif

                                    {{--{{ Form::hidden('school_id', Auth::user()->school_id) }}--}}
                                    {{--{{ Form::label('school', trans('common.school'), ['class' => 'control-label']) }}--}}
                                    {{--{{ Form::select('school', array('7' => trans('common.business'),'2' => trans('common.art'),'3' => trans('common.engineering'),'4' => trans('common.it'),'5' => trans('common.medicine'),'6' => trans('common.english')) , null, ['class' => 'form-control']) }}--}}
                                    {{--{{ Form::text('school_id',Auth::user()->school_id, ['class' => 'form-control', 'readonly' => 'true']) }}--}}
                                </fieldset>

                                <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                    {{ Form::label('name', trans('auth.name'), ['class' => 'control-label']) }}
                                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')]) }}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
                                    @endif
                                </fieldset>
                                <fieldset class="form-group required {{ $errors->has('username') ? ' has-error' : '' }}">
                                    {{ Form::label('username', trans('common.username'), ['class' => 'control-label']) }}
                                    {{ Form::text('username', old('username'), ['class' => 'form-control','maxlength' => '26', 'placeholder' => trans('common.username')]) }}
                                    @if ($errors->has('username'))
                                        <span class="help-block">
										<strong>{{ $errors->first('username') }}</strong>
									</span>
                                    @endif

                                </fieldset>
                                <fieldset class="form-group">
                                    {{ Form::label('about', trans('common.about'), ['class' => 'control-label']) }}
                                    {{ Form::textarea('about', old('about'), ['class' => 'form-control', 'placeholder' => trans('messages.create_page_placeholder'), 'rows' => '4', 'cols' => '20']) }}
                                </fieldset>

                                <div class="pull-right">
                                    {{ Form::submit(trans('common.create_page'), ['class' => 'btn btn-success']) }}
                                </div>
                                <div class="clearfix"></div>

                            </form>
                        </div>
                    </div><!-- /panel-body -->
            </div>


        {{--<div class="col-md-4">--}}
            {{--<div class="panel panel-default">--}}
                {{--<div class="panel-heading no-bg panel-settings">--}}
                    {{--<h3 class="panel-title">{{ trans('common.about').' '.trans('common.pages') }}</h3>--}}
                {{--</div>--}}
                {{--<div class="panel-body right-panel">--}}
                    {{--<div class="privacy-question">--}}
                        {{--<ul class="list-group right-list-group">--}}
                            {{--<li href="#" class="list-group-item">--}}
                                {{--<div class="holder">--}}
                                    {{--<div class="about-page">--}}
                                        {{--{{ Form::label('about_page_heading1', trans('messages.about_page_heading1'), ['class' => 'right-side-label']) }}--}}
                                    {{--</div>--}}
                                    {{--<div class="page-description">--}}
                                        {{--{{ trans('messages.about_page_content1') }}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                            {{--<li href="#" class="list-group-item">--}}
                                {{--<div class="holder">--}}
                                    {{--<div class="about-page">--}}
                                        {{--{{ Form::label('about_page_heading2', trans('messages.about_page_heading2'), ['class' => 'right-side-label']) }}--}}
                                    {{--</div>--}}
                                    {{--<div class="page-description">--}}
                                        {{--{{ trans('messages.about_page_content2') }}--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul><!-- /list-group -->--}}
                    {{--</div>--}}
                {{--</div><!-- /panel-body -->--}}
            {{--</div>--}}

            {{--@if(Setting::get('createpage_ad') != NULL)--}}
                {{--<div id="link_other" class="page-image">--}}
                    {{--{!! htmlspecialchars_decode(Setting::get('createpage_ad')) !!}--}}
                {{--</div>--}}
            {{--@endif--}}
        {{--</div><!-- /col-md-4 -->--}}


<!-- </div> -->
@endif

@endif
