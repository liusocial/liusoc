<div class="panel panel-default">
    <div class="panel-heading no-bg panel-settings">
        <h3 class="panel-title">
            {{ trans('admin.users_acc_req') }}
        </h3>
    </div>
    <div class="panel-body timeline">
        @include('flash::message')
        @if(count($users) > 0)
            <div class="table-responsive manage-table">
                <table class="table existing-products-table liusocial">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>{{ trans('auth.st_id') }}</th>
                        <th>{{ trans('auth.name') }}</th>
                        <th>{{ trans('auth.major') }}</th>

                        <th>{{ trans('common.email') }}</th>
                        <th>{{ trans('admin.user_req_date') }}</th>

                        <th>{{ trans('admin.approve_req') }}</th>
                        <th>{{ trans('admin.reject_req') }}</th>


                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>&nbsp;</td>
                            <td>{{ $user->st_id }}</td>
                            <td style="padding-bottom: 10px;"><a href="#"><img src="{{ $user->avatar }}" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="{{ url($user->timeline->username) }}" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> {{ $user->timeline->username }}</a></td>                            <td>{{ $user->school->name }}</td>

                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>

                                <a href="{{ url('admin/requests/'.$user->id.'/approve')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class=" btn btn  btn-info btn-lg" style=""><i class="glyphicon glyphicon-ok" aria-hidden="true">Accept</i></span></a>
                            </td>
                            <td>
                                   <a href="{{ url('admin/requests/'.$user->id.'/reject')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="btn btn btn-danger btn-lg"><i class="glyphicon glyphicon-remove" aria-hidden="true">Reject</i></span></a>



                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagination-holder">
                {{ $users->render() }}
            </div>
        @else
            <div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
        @endif
    </div>
</div>
