<style>
    input[type=radio] + .fordebit,
    input[type=radio] + .forcredit{
        display:none;
    }
    input[type=radio]:checked + .fordebit{
        display:block;
    }
    input[type=radio]:checked + .forcredit{
        display:block;
    }

    input[type=radio] + .details{
        display: none;
    }

    input[type=radio]:checked + .fordebit {
        display: block;
    }

    input[type=radio]:checked + .forcredit {
        display: block;
    }
    input[type=radio] {
        float:left;
    }
</style>
<div class="panel panel-default">
    <div class="panel-body">
        @include('flash::message')
        <div class="panel-heading no-bg panel-settings">
            <h3 class="panel-title">
                {{ trans('admin.add_user') }}
            </h3>
        </div>
        @if($mode =="create")
        <form method="POST" action="{{ url('admin/users') }}" class="socialite-form">
            {{ csrf_field() }}

            <fieldset class="form-group required {{ $errors->has('username') ? ' has-error' : '' }}">
                {{ Form::label('username', trans('common.username'), ['class' => 'control-label']) }}

                <input type="text" class="form-control content-form" placeholder="{{ trans('common.username') }}" name="username" required>
                {{--<small class="text-muted">{{ trans('admin.user_username_text') }}</small>--}}
                @if ($errors->has('username'))
                    <span class="help-block">
					<strong>{{ $errors->first('username') }}</strong>
				</span>
                @endif
            </fieldset>

            <fieldset class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                {{ Form::label('email', trans('auth.email_address'), ['class' => 'control-label']) }}
                <input type="email" class="form-control" name="email" placeholder="{{ trans('common.email') }}" required>
                {{--<small class="text-muted">{{ trans('admin.user_email_text') }}</small>--}}
                @if ($errors->has('email'))
                    <span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
                @endif
            </fieldset>

            <fieldset class="form-group required {{ $errors->has('password') ? ' has-error' : '' }}">
                {{ Form::label('password', trans('common.password'), ['class' => 'control-label']) }}
                <input type="password" class="form-control" name="password" placeholder="{{ trans('common.new_password') }}"required>
                {{--<small class="text-muted">{{ trans('common.new_password_text') }}</small>--}}
                @if ($errors->has('password'))
                    <span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
                @endif
            </fieldset>
            <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                {{ Form::label('name', trans('auth.name'), ['class' => 'control-label']) }}
                <input type="text" class="form-control require-if-active" name="name"  placeholder="Name" required>
                {{--<small class="text-muted">{{ trans('admin.user_name_text') }}</small>--}}
                @if ($errors->has('name'))
                    <span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
                @endif
            </fieldset>

            <fieldset class="form-group">
                {{ Form::label('gender', trans('common.gender'), ['class' => 'control-label']) }}
                {{ Form::select('gender', array('male' => trans('common.male'),'female' => trans('common.female'),'other' => trans('common.other')) , null, ['class' => 'form-control']) }}
                {{--<small class="text-muted">{{ trans('admin.user_gender_text') }}</small>--}}
            </fieldset>

            <fieldset class="form-group">
                {{ Form::label('school', trans('common.school'), ['class' => 'control-label']) }}

                {{ Form::select('school_id', array('7' => trans('common.business'),'2' => trans('common.art'),'3' => trans('common.engineering'),'4' => trans('common.it'),'5' => trans('common.medicine'),'6' => trans('common.english')) , null, ['class' => 'form-control']) }}
                {{--<i class="fa fa-user">@$school->name</i>--}}{{--<h4>({{$school->name}})</h4>--}}
                {{--{{ Form::text('school_id',Auth::user()->school_id, ['class' => 'form-control', 'readonly' => 'true']) }}--}}
                {{--{{ Form::text('title',null,['class' => 'form-control']) }}--}}
                {{--{{ Form::number('user_id',null,['class' => 'form-control']) }}--}}
                {{--@else--}}
                {{--{{ Form::text('title', $announcement->user_id, ['class' => 'form-control']) }}--}}
                {{--@endif--}}
                {{--</div>--}}

            </fieldset>


            <fieldset class="form-group">
                {{ Form::label('role', trans('common.type'), ['class' => 'control-label']) }}
                <div>
                    <div>
                        <div class="col-md-4">
                        <label class="radio-header"><i class="fa fa-user"></i> {{ trans('common.instructor') }}</label>
                        <input type="radio" name="type" id="choice-inst" value="4" required>

                        {{--<div class="forcredit details">--}}

                            {{--<label>CVV no:</label>--}}
                            {{--<input type="text" name="cvvno" required=""><span style="color: red;">*</span>--}}
                            {{--<br>--}}
                            {{--<label>Expiration MM/YYYY</label>--}}
                            {{--<input type="month" name="expire" required=""><span style="color: red;">*</span>--}}
                            {{--<br>--}}
                        {{--</div>--}}
                        </div>
                    </div>
                    <div>
                        {{--<div class="col-md-4">--}}
                            {{--<label class="radio-header"><i class="fa fa-user"></i> {{ trans('common.validator') }}</label>--}}
                            {{--<input type="radio" name="type" id="choice-inst" value="6" required>--}}

                            {{--<div class="forcredit details">--}}

                            {{--<label>CVV no:</label>--}}
                            {{--<input type="text" name="cvvno" required=""><span style="color: red;">*</span>--}}
                            {{--<br>--}}
                            {{--<label>Expiration MM/YYYY</label>--}}
                            {{--<input type="month" name="expire" required=""><span style="color: red;">*</span>--}}
                            {{--<br>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                        <div>
                            <div class="col-md-3">
                                <label class="radio-header"><i class="fa fa-user"></i> {{ trans('common.dean') }}</label>
                                <input type="radio" name="type" id="choice-dean" value="3">

                                <div class="forcredit details">

                                    {{--<br>--}}
                                    {{--<fieldset id="optional1" class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">--}}
                                        {{--{{ Form::label('gender', trans('common.gender'), ['class' => 'control-label']) }}--}}
                                        {{--{{ Form::select('gender', array('male' => trans('common.male'),'female' => trans('common.female'),'other' => trans('common.other')) , null, ['class' => 'form-control']) }}--}}
                                        {{--<small class="text-muted">{{ trans('admin.user_name_text') }}</small>--}}
                                        {{--@if ($errors->has('name'))--}}
                                            {{--<span class="help-block">--}}
					{{--<strong>{{ $errors->first('name') }}</strong>--}}
				{{--</span>--}}
                                        {{--@endif--}}
                                    {{--</fieldset>--}}
                                {{--<br>--}}
                                </div>
                            </div>
                        </div>


                <div>
                    <div class="col-md-3">
                        <label class="radio-header"><i class="fa fa-user"></i> {{ trans('common.validator') }}</label>
                        <input type="radio" name="type" id="choice-dean" value="6">

                        <div class="forcredit details">

                            {{--<br>--}}
                            {{--<fieldset id="optional1" class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">--}}
                            {{--{{ Form::label('gender', trans('common.gender'), ['class' => 'control-label']) }}--}}
                            {{--{{ Form::select('gender', array('male' => trans('common.male'),'female' => trans('common.female'),'other' => trans('common.other')) , null, ['class' => 'form-control']) }}--}}
                            {{--<small class="text-muted">{{ trans('admin.user_name_text') }}</small>--}}
                            {{--@if ($errors->has('name'))--}}
                            {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('name') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--</fieldset>--}}
                            {{--<br>--}}
                        </div>
                    </div>
                </div>


                        <div>
                        <label class="margin-left-113 radio-header"><i class="fa fa-lock"></i> {{ trans('common.student') }}</label>
                        <input type="radio" name="type"  id="choice-student" value="2">

                        <div class="fordebit details">
                            <br>
                            <fieldset id="optional1" class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                                {{ Form::label('id', trans('common.liu_id'), ['class' => 'control-label']) }}
                                <input type="number" class="form-control" name="std_id"  placeholder="ID" data-require-pair="#choice-student">
                                {{--<small class="text-muted">{{ trans('admin.user_name_text') }}</small>--}}
                                @if ($errors->has('id'))
                                    <span class="help-block">
					<strong>{{ $errors->first('id') }}</strong>
				</span>
                                @endif
                            </fieldset>

                            {{--<label>CVV no:</label>--}}
                            {{--<input type="text" name="cvvno" required=""><span style="color: red;">*</span>--}}
                            {{--<br>--}}
                            {{--<label>Expiration MM/YYYY</label>--}}
                            {{--<input type="month" name="expire" required=""><span style="color: red;">*</span>--}}
                            {{--<br>--}}
                        </div>
                    </div>



            </fieldset>

            @if(Setting::get('birthday') == "on")
                <fieldset class="form-group">
                    {{ Form::label('birthday', trans('common.birthday'), ['class' => 'control-label']) }}
                    <input class="datepicker form-control hasDatepicker" size="16" id="datepick2" name="birthday" type="text" value="" data-date-format="yyyy-mm-dd">
                </fieldset>
            @endif
            {{--<fieldset class="form-group">--}}
                {{--{{ Form::label('School_id', trans('admin.school'), ['class' => 'col-sm-2 control-label']) }}--}}


                {{--<i class="fa fa-user">@$school->name</i>--}}{{----}}{{--<h4>({{$school->name}})</h4>--}}
                {{--{{ Form::hidden('school_id', Auth::user()->school_id) }}--}}
                    {{--{{ Form::text('school_id',Auth::user()->school_id, ['class' => 'form-control', 'readonly' => 'true']) }}--}}
                    {{--{{ Form::text('title',null,['class' => 'form-control']) }}--}}
                    {{--{{ Form::number('user_id',null,['class' => 'form-control']) }}--}}
                    {{--@else--}}
                    {{--{{ Form::text('title', $announcement->user_id, ['class' => 'form-control']) }}--}}
                    {{--@endif--}}
                    {{--</div>--}}

            {{--</fieldset>--}}



            {{--<fieldset class="form-group">--}}
                {{--{{ Form::label('about', trans('common.about'), ['class' => 'control-label']) }}--}}
                {{--<textarea class="form-control about-form" name="about" rows="3" value="" placeholder="{{ trans('common.about') }}"></textarea>--}}
                {{--<small class="text-muted">{{ trans('admin.user_about_text') }}</small>--}}
            {{--</fieldset>--}}
            @if(Setting::get('city') == "on")
            <fieldset class="form-group">
                {{ Form::label('city', trans('common.current_city'), ['class' => 'control-label']) }}
                <input type="text" class="form-control" name="city" value="" placeholder="{{ trans('common.current_city') }}">
                <small class="text-muted">{{ trans('admin.user_city_text') }}</small>
            </fieldset>
            @endif

            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm">{{ trans('common.save_changes') }}</button>
            </div>
        </form>

    </div>
</div>
@endif
<script>
//    function hideA(x) {
//        if (x.checked) {
//            document.getElementById("A").style.visibility = "hidden";
//            document.getElementById("B").style.visibility = "visible";
//        }
//    }
//
//    function hideB(x) {
//        if (x.checked) {
//            document.getElementById("B").style.visibility = "hidden";
//            document.getElementById("A").style.visibility = "visible";
//        }
//    }

//    $('input[name="type"]').click(function(e) {
//        if(e.target.value === '4') {
//            $('#optional1').hide();
//            $('#optional2').show();
//        } if(e.target.value === '2') {
//            $('#optional2').hide();
//            $('#optional1').show();
//        }
//    })
//
//    $('#optional').hide();

</script>