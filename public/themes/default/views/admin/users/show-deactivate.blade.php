<div class="panel panel-default">
    <div class="panel-heading no-bg panel-settings">
        <h3 class="panel-title">
            {{ trans('common.manage_users') }}


        </h3>
    </div>

    @if(count($users) > 0)
        <div class="table-responsive manage-table">
            <table class="table existing-products-table liusocial">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>{{ trans('admin.id') }}</th>
                    <th>{{ trans('auth.name') }}</th>
                    <th>{{ trans('auth.name') }}</th>
                    <th>{{ trans('auth.user_type') }}</th>
                    <th>{{ trans('common.email') }}</th>
                    <th>{{ trans('common.status') }}</th>
                    <th>{{ trans('admin.options') }}</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $user)
                    <tr>
                        <td>&nbsp;</td>
                        <td>{{ $user->id }}</td>
                        <td style="padding-bottom: 10px;"><a href="#"><img src="{{ $user->avatar }}" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="{{ url($user->timeline->username) }}" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> {{ $user->timeline->username }}</a></td>                        @foreach($user->roles as $role)
                            <td>{{ $user->timeline->name }}</td>
                            <td>{{ $role->name }}</td>
                        @endforeach
                        <td>{{ $user->email }}</td>
                        <td>
                            @if($user->active == "0")
                                <a href="{{ url('admin/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >NonActive</a>
                            @endif
                        </td>
                        <td>
                            <ul class="list-inline">
                                <li><a href="{{ url('admin/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
                                {{----}}
                                {{--<li><a href="{{ url('dean/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
                            </ul>

                        </td>
                        <td>&nbsp;</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="pagination-holder">
            {{ $users->render() }}
        </div>
    @else
        <div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
    @endif
</div>
</div>
