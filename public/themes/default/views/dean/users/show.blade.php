<div class="panel panel-default">
	<div class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			{{ trans('common.manage_users') }}
			@if(Auth::user()->language =='ar')
				<span class="pull-left">
				<a href="{{ url('dean/users/add/create') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					@else
				<span class="pull-right">
				<a href="{{ url('dean/users/add/create') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
							@endif

		</h3>
	</div>
	<br>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#students">{{ trans('common.students') }}</a></li>
		<li><a data-toggle="tab" href="#inst">{{ trans('common.instructors') }}</a></li>
		{{--<li><a data-toggle="tab" href="#std">Student Center</a></li>--}}
	</ul>
	@include('flash::message')
	<div class="tab-content clearfix">

		<div id="students" class="tab-pane fade in active">
			@if(count($users) > 0)
			<div class="table-responsive manage-table">
				<table class="table existing-products-table liusocial">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th>{{ trans('auth.st_id') }}</th>
							<th>{{ trans('auth.username') }}</th>
							<th>{{ trans('auth.name') }}</th>
							<th>{{ trans('common.user_type') }}</th>
							<th>{{ trans('common.email') }}</th>
							<th>{{ trans('common.status') }}</th>
							<th>{{ trans('admin.options') }}</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>

						@foreach($users as $user)
							@foreach($user->roles as $role)
								@if($role->name =='student')
						<tr>
							<td>&nbsp;</td>	
							<td>{{ $user->st_id }}</td>
							<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>
							@foreach($user->roles as $role)
								<td>{{ $user->timeline->name }}</td>
								<td>{{ $role->name }}</td>
							@endforeach
							<td>{{ $user->email }}</td>
							<td>
								@if($user->active == "1")
									<a href="{{ url('dean/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
								@else
									<a href="{{ url('dean/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
								@endif
							</td>
							<td>
								<ul class="list-inline">
									<li><a href="{{ url('dean/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
									{{--<li><a href="{{ url('dean/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
								</ul>

							</td>
							<td>&nbsp;</td> 
						</tr>
						@endif
								@endforeach
						@endforeach
						</tbody>
					</table>
				</div>
				<div class="pagination-holder">
					{{ $users->render() }}
				</div>	
			@else
				<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
			@endif
		</div>{{--end students--}}



	<div id="inst" class="tab-pane fade">
		@if(count($users) > 0)
			<div class="table-responsive manage-table">
				<table class="table existing-products-table liusocial">
					<thead>
					<tr>
						<th>&nbsp;</th>
						<th>{{ trans('admin.id') }}</th>
						<th>{{ trans('auth.username') }}</th>
						<th>{{ trans('auth.name') }}</th>
						<th>{{ trans('common.user_type') }}</th>
						<th>{{ trans('common.email') }}</th>
						<th>{{ trans('common.status') }}</th>
						<th>{{ trans('admin.options') }}</th>
						<th>&nbsp;</th>
					</tr>
					</thead>
					<tbody>

					@foreach($users as $user)
						@foreach($user->roles as $role)
							@if($role->name =='instructor')
						<tr>
							<td>&nbsp;</td>
							<td>{{ $user->id }}</td>
							<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

								<td>{{ $user->timeline->name }}</td>
								<td>{{ $role->name }}</td>

							<td>{{ $user->email }}</td>
							<td>
								@if($user->active == "1")
									<a href="{{ url('dean/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
								@else
									<a href="{{ url('dean/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
								@endif
							</td>
							<td>
								<ul class="list-inline">
									<li><a href="{{ url('dean/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
									{{--<li><a href="{{ url('dean/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
								</ul>

							</td>
							<td>&nbsp;</td>
						</tr>
						@endif
							@endforeach
					@endforeach
					</tbody>
				</table>
			</div>
			<div class="pagination-holder">
				{{ $users->render() }}
			</div>
		@else
			<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
		@endif
	</div>
	</div>
	</div>
