{{--<style>--}}
	{{--.content-posts {--}}
		{{--margin-left: 240px;--}}
		{{--margin-top: 65px;--}}
	{{--}--}}
	{{--.container-posts {--}}
		{{--padding: 0 130px;--}}
	{{--}--}}
	{{--.primary-content .container-posts {--}}
		{{--padding: 0 40px;--}}
	{{--}--}}
	{{--.primary-content { width:70%;--}}
		{{--float: left; }--}}
	{{--@media screen and (max-width:1199px)  {   header { padding: 15px 30px 10px 220px;  }--}}

		{{--.sidebar-nav {  width:190px;  }--}}

		{{--.content-posts {  margin-left:190px;}--}}

		{{--.container-posts { padding:0px 30px;  }--}}
	{{--}--}}
	{{--.content-posts  {  margin:0px;--}}
		{{--margin-top:120px;}--}}

	{{--.container-posts, .primary-content .container-posts { padding:0px 15px;  }--}}
{{--</style>--}}



<div class="panel panel-default">
@include('flash::message')



		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#devloper">devloper Announcements</a></li>
			<li><a data-toggle="tab" href="#business">{{ trans('common.bus_users') }}</a></li>
			<li><a data-toggle="tab" href="#art">{{ trans('common.art_users') }}</a></li>
			<li><a data-toggle="tab" href="#it">{{ trans('common.it_users') }}</a></li>
			<li><a data-toggle="tab" href="#med">{{ trans('common.mid_users') }}</a></li>
			<li><a data-toggle="tab" href="#eng">{{ trans('common.eng_users') }}</a></li>
			<li><a data-toggle="tab" href="#english">{{ trans('common.english_users') }}</a></li>
			<li><a data-toggle="tab" href="#std">{{ trans('common.student_center') }}</a></li>
		</ul>
		<div class="tab-content">
			<div id="devloper" class="tab-pane fade in active">

				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements != NULL)
							@foreach($current_anouncements as $current_anouncement)
								<div class="clearfix"></div>
							<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
</div>


				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
						@if($total_days != 0)
						{{--{{ $total_days }}--}}
							{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>

								<div class="clearfix"></div>
								<div class="announcement-description pull-left">
									{{  $current_anouncement->description }}
									<div class="time-created">
                                        <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
										{!! '<br> Created on '.$announces_date !!}
									</div>
								</div>
								<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
							</div>
							@endforeach
						@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif
					</div>
				</div>



	<div class="panel-heading no-bg panel-settings">

			<br>
			{{--<div class="content-posts profile-content">--}}
				{{--<div class="banner-profile">--}}
				{{--</div>--}}
				{{--<!-- Tab Panel -->--}}
				{{--<ul class="nav nav-tabs" role="tablist">--}}
					{{--<li class="active"><a href="#devloper" role="tab" id="adminTab" data-toggle="tab" aria-controls="devloper" aria-expanded="true">devloper Announcements</a></li>--}}
					{{--<li><a href="#business" role="tab" id="businessTab" data-toggle="tab" aria-controls="business" aria-expanded="true">Bussiness College Announcements</a></li>--}}
					{{--<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Art College Announcements</a></li>--}}
					{{--<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">IT College Announcements</a></li>--}}
					{{--<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Midical College Announcements</a></li>--}}
					{{--<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Engnniring College Announcements</a></li>--}}
					{{--<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Student Center Announcements</a></li>--}}
				{{--</ul>--}}
				{{--<div class="tab-content">--}}
					{{--<!-- Tab devloper -->--}}
				{{--<div class="tab-pane fade active in" role="tabpanel" id="devloper" aria-labelledby="adminTab">--}}
					{{--<div id="posts-container" class="container-fluid container-posts">--}}

			<h3 class="panel-title">
							{{ trans('devloper.announcements') }}
				@if(Auth::user()->language =='ar')
				<span class="pull-left">
				<a href="{{ url('devloper/announcements/create') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					@else
					<span class="pull-right">
				<a href="{{ url('devloper/announcements/create') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					@endif
		</h3>
	</div>
	<div class="panel-body">
		<div class="announcement-container table-responsive">	
			<table class="table announcements-table">
				<thead>
			    	<th>{{ trans('devloper.title') }}</th>
			        <th>{{ trans('common.description') }}</th>	 
			        <th>{{ trans('devloper.start_date') }}</th>
			        <th>{{ trans('devloper.end_date') }}</th>
			        <th>{{ trans('common.status') }}</th>
			        <th>{{ trans('devloper.action') }}</th>
		    	</thead>
			    <tbody>
			     @foreach($announcements as $announcement)
			    	<tr>
			        	<td>{{ $announcement->title }}</td>
			            <td> 
			            	<span class="description">
			            		{{ $announcement->description }} 
			            		{{-- <div class="time">			 --}}
			            			{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
			            			{{-- {{ $announce_date }}
			            		</div> --}}
			            		<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div> 
			            	</span>
						</td> 
						<td>
							{{ $announcement->start_date }}
						</td>
						<td>
							{{ $announcement->end_date }}
						</td>
						{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
						<td>
							@if($announcement->value == "1")
							<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
							@else
							<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
							@endif
						</td>
						<td>
							<ul class="list-inline">	
								<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
								<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
							</ul>
						</td>
			        </tr>			        
			        @endforeach
			    </tbody>
			</table>
			<div class="pagination-holder">
				{{ $announcements->render() }}
			</div>	
		</div>
	</div>

</div><!-- end Tab devloper -->

			<div id="business" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements_bu != NULL)
							@foreach($current_anouncements_bu as $current_anouncement)
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
</div>


				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
					@if($total_days != 0)
						{{--{{ $total_days }}--}}
						{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										{{  $current_anouncement->description }}
										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											{!! '<br> Created on '.$announces_date !!}
										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
								</div>
							@endforeach
						@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
				<!-- Tab business -->
				{{--<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">--}}
					{{--<div class="container-fluid container-posts">--}}

						<h3 class="panel-title">
							{{ trans('devloper.business_announcements') }}

							<span class="pull-right">
				<a href="{{ url('devloper/announcements/create_business') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
						</h3>
				</div>
					<div class="panel-body">
						<div class="announcement-container table-responsive">
							<table class="table announcements-table">
								<thead>
								<th>{{ trans('devloper.title') }}</th>
								<th>{{ trans('common.description') }}</th>
								<th>{{ trans('devloper.start_date') }}</th>
								<th>{{ trans('devloper.end_date') }}</th>
								<th>{{ trans('common.status') }}</th>
								<th>{{ trans('devloper.action') }}</th>
								</thead>
								<tbody>
								@foreach($bu_announcements as $announcement)
									<tr>
										<td>{{ $announcement->title }}</td>
										<td>
			            	<span class="description">
			            		{{ $announcement->description }}
								{{-- <div class="time">			 --}}
								{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
								{{-- {{ $announce_date }}
                            </div> --}}
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div>
			            	</span>
										</td>
										<td>
											{{ $announcement->start_date }}
										</td>
										<td>
											{{ $announcement->end_date }}
										</td>
										{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
										<td>
											@if($announcement->value == "1")
												<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
											@else
												<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
											@endif
										</td>
										<td>
											<ul class="list-inline">
												<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
												<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
											</ul>
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
							<div class="pagination-holder">
								{{ $announcements->render() }}
							</div>
						</div>
					</div>

			</div><!-- end Tab bussiness -->

			<div id="art" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements_art != NULL)
							@foreach($current_anouncements_art as $current_anouncement)
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
</div>


				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
					@if($total_days != 0)
						{{--{{ $total_days }}--}}
						{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										{{  $current_anouncement->description }}
										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											{!! '<br> Created on '.$announces_date !!}
										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
								</div>
							@endforeach
						@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					{{--<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">--}}
					{{--<div class="container-fluid container-posts">--}}

					<h3 class="panel-title">
						{{ trans('devloper.art_announcements') }}

						<span class="pull-right">
				<a href="{{ url('devloper/announcements/create_art') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th>{{ trans('devloper.title') }}</th>
							<th>{{ trans('common.description') }}</th>
							<th>{{ trans('devloper.start_date') }}</th>
							<th>{{ trans('devloper.end_date') }}</th>
							<th>{{ trans('common.status') }}</th>
							<th>{{ trans('devloper.action') }}</th>
							</thead>
							<tbody>
							@foreach($art_announcements as $announcement)
								<tr>
									<td>{{ $announcement->title }}</td>
									<td>
			            	<span class="description">
			            		{{ $announcement->description }}
								{{-- <div class="time">			 --}}
								{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
								{{-- {{ $announce_date }}
                            </div> --}}
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										{{ $announcement->start_date }}
									</td>
									<td>
										{{ $announcement->end_date }}
									</td>
									{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
									<td>
										@if($announcement->value == "1")
											<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
										@else
											<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
										@endif
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="pagination-holder">
							{{ $announcements->render() }}
						</div>
					</div>
				</div>

			</div><!-- end Tab art -->

			<div id="it" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements_it != NULL)
							@foreach($current_anouncements_it as $current_anouncement)
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
</div>


				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
					@if($total_days != 0)
						{{--{{ $total_days }}--}}
						{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										{{  $current_anouncement->description }}
										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											{!! '<br> Created on '.$announces_date !!}
										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
								</div>
							@endforeach
						@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					{{--<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">--}}
					{{--<div class="container-fluid container-posts">--}}

					<h3 class="panel-title">
						{{ trans('devloper.it_announcements') }}

						<span class="pull-right">
				<a href="{{ url('devloper/announcements/create_it') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th>{{ trans('devloper.title') }}</th>
							<th>{{ trans('common.description') }}</th>
							<th>{{ trans('devloper.start_date') }}</th>
							<th>{{ trans('devloper.end_date') }}</th>
							<th>{{ trans('common.status') }}</th>
							<th>{{ trans('devloper.action') }}</th>
							</thead>
							<tbody>
							@foreach($it_announcements as $announcement)
								<tr>
									<td>{{ $announcement->title }}</td>
									<td>
			            	<span class="description">
			            		{{ $announcement->description }}
								{{-- <div class="time">			 --}}
								{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
								{{-- {{ $announce_date }}
                            </div> --}}
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										{{ $announcement->start_date }}
									</td>
									<td>
										{{ $announcement->end_date }}
									</td>
									{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
									<td>
										@if($announcement->value == "1")
											<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
										@else
											<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
										@endif
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="pagination-holder">
							{{ $announcements->render() }}
						</div>
					</div>
				</div>

			</div><!-- end Tab it -->

			<div id="med" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements_med != NULL)
							@foreach($current_anouncements_med as $current_anouncement)
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
</div>


				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
					@if($total_days != 0)
						{{--{{ $total_days }}--}}
						{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										{{  $current_anouncement->description }}
										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											{!! '<br> Created on '.$announces_date !!}
										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
								</div>
							@endforeach
						@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					{{--<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">--}}
					{{--<div class="container-fluid container-posts">--}}

					<h3 class="panel-title">
						{{ trans('devloper.med_announcements') }}

						<span class="pull-right">
				<a href="{{ url('devloper/announcements/create_medicine') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th>{{ trans('devloper.title') }}</th>
							<th>{{ trans('common.description') }}</th>
							<th>{{ trans('devloper.start_date') }}</th>
							<th>{{ trans('devloper.end_date') }}</th>
							<th>{{ trans('common.status') }}</th>
							<th>{{ trans('devloper.action') }}</th>
							</thead>
							<tbody>
							@foreach($med_announcements as $announcement)
								<tr>
									<td>{{ $announcement->title }}</td>
									<td>
			            	<span class="description">
			            		{{ $announcement->description }}
								{{-- <div class="time">			 --}}
								{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
								{{-- {{ $announce_date }}
                            </div> --}}
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										{{ $announcement->start_date }}
									</td>
									<td>
										{{ $announcement->end_date }}
									</td>
									{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
									<td>
										@if($announcement->value == "1")
											<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
										@else
											<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
										@endif
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="pagination-holder">
							{{ $announcements->render() }}
						</div>
					</div>
				</div>

			</div><!-- end Tab med -->

			<div id="eng" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements_eng != NULL)
							@foreach($current_anouncements_eng as $current_anouncement)
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
</div>


				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
					@if($total_days != 0)
						{{--{{ $total_days }}--}}
						{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										{{  $current_anouncement->description }}
										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											{!! '<br> Created on '.$announces_date !!}
										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
								</div>
							@endforeach
						@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					{{--<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">--}}
					{{--<div class="container-fluid container-posts">--}}

					<h3 class="panel-title">
						{{ trans('devloper.eng_announcements') }}

						<span class="pull-right">
				<a href="{{ url('devloper/announcements/create_eng') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th>{{ trans('devloper.title') }}</th>
							<th>{{ trans('common.description') }}</th>
							<th>{{ trans('devloper.start_date') }}</th>
							<th>{{ trans('devloper.end_date') }}</th>
							<th>{{ trans('common.status') }}</th>
							<th>{{ trans('devloper.action') }}</th>
							</thead>
							<tbody>
							@foreach($eng_announcements as $announcement)
								<tr>
									<td>{{ $announcement->title }}</td>
									<td>
			            	<span class="description">
			            		{{ $announcement->description }}
								{{-- <div class="time">			 --}}
								{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
								{{-- {{ $announce_date }}
                            </div> --}}
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										{{ $announcement->start_date }}
									</td>
									<td>
										{{ $announcement->end_date }}
									</td>
									{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
									<td>
										@if($announcement->value == "1")
											<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
										@else
											<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
										@endif
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="pagination-holder">
							{{ $announcements->render() }}
						</div>
					</div>
				</div>

			</div><!-- end Tab eng -->

			<div id="english" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements_english != NULL)
							@foreach($current_anouncements_english as $current_anouncement)
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
</div>


				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
					@if($total_days != 0)
						{{--{{ $total_days }}--}}
						{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										{{  $current_anouncement->description }}
										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											{!! '<br> Created on '.$announces_date !!}
										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
								</div>
							@endforeach
						@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					{{--<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">--}}
					{{--<div class="container-fluid container-posts">--}}

					<h3 class="panel-title">
						{{ trans('devloper.english_announcements') }}

						<span class="pull-right">
				<a href="{{ url('devloper/announcements/create_english') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th>{{ trans('devloper.title') }}</th>
							<th>{{ trans('common.description') }}</th>
							<th>{{ trans('devloper.start_date') }}</th>
							<th>{{ trans('devloper.end_date') }}</th>
							<th>{{ trans('common.status') }}</th>
							<th>{{ trans('devloper.action') }}</th>
							</thead>
							<tbody>
							@foreach($english_announcements as $announcement)
								<tr>
									<td>{{ $announcement->title }}</td>
									<td>
			            	<span class="description">
			            		{{ $announcement->description }}
								{{-- <div class="time">			 --}}
								{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
								{{-- {{ $announce_date }}
                            </div> --}}
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										{{ $announcement->start_date }}
									</td>
									<td>
										{{ $announcement->end_date }}
									</td>
									{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
									<td>
										@if($announcement->value == "1")
											<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
										@else
											<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
										@endif
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="pagination-holder">
							{{ $announcements->render() }}
						</div>
					</div>
				</div>

			</div><!-- end Tab english -->

			<div id="std" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							{{ trans('devloper.active_announcement') }}
						</h3>
					</div>
					<div class="panel-body">

						@if($current_anouncements_std != null)
							@foreach($current_anouncements_std as $current_anouncement)
								<div class="clearfix"></div>
								<div class="announcement-container">
			<span class="announcement-title">
				<div class="announcement-title">
				{{ $current_anouncement->title }}
							</div>
				<span class="pull-right label label-default expiry-date">
					@if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now())))
						{{$total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now())) }}
					@endif
					@if($total_days != 0)
						{{--{{ $total_days }}--}}
						{{ trans('devloper.days_to_expire') }}
					@else
						{{ trans('devloper.expired') }}

					@endif
				</span>
					</span>
									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										{{  $current_anouncement->description }}
										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											{!! '<br> Created on '.$announces_date !!}
										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : {{ count($current_anouncement->users) }}</a>
				<a href="{{ url('devloper/announcements/'.$current_anouncement->id.'/edit')}}">{{ trans('common.edit') }}</a>
			</span>
								</div>

							@endforeach

								@else
							<div class="alert alert-warning ">{{ trans('messages.no_announcements') }}</div>
						@endif


					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					{{--<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">--}}
					{{--<div class="container-fluid container-posts">--}}

					<h3 class="panel-title">
						{{ trans('devloper.student_announcements') }}

						<span class="pull-right">
				<a href="{{ url('devloper/announcements/create_std') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th>{{ trans('devloper.title') }}</th>
							<th>{{ trans('common.description') }}</th>
							<th>{{ trans('devloper.start_date') }}</th>
							<th>{{ trans('devloper.end_date') }}</th>
							<th>{{ trans('common.status') }}</th>
							<th>{{ trans('devloper.action') }}</th>
							</thead>
							<tbody>
							@foreach($std_announcements as $announcement)
								<tr>
									<td>{{ $announcement->title }}</td>
									<td>
			            	<span class="description">
			            		{{ $announcement->description }}
								{{-- <div class="time">			 --}}
								{{--*/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /*--}}
								{{-- {{ $announce_date }}
                            </div> --}}
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> {{ count($announcement->users) }}
			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										{{ $announcement->start_date }}
									</td>
									<td>
										{{ $announcement->end_date }}
									</td>
									{{--*/ $status = $announcement->id == Setting::get('announcement') ? trans('devloper.active') : trans('devloper.inactive') /*--}}
									<td>
										@if($announcement->value == "1")
											<a href="{{ url('devloper/inactivate/'.$announcement->id)}}" class="btn btn-success announcement-status" >Active</a>
										@else
											<a href="{{ url('devloper/activate/'.$announcement->id)}}" class="btn btn-default announcement-status" >Inactivate</a>
										@endif
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="{{ url('devloper/announcements/'.$announcement->id.'/edit')}}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="{{ $announcement->id }}" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						<div class="pagination-holder">
							{{ $announcements->render() }}
						</div>
					</div>
				</div>

			</div><!-- end Tab student center -->

			</div><!-- Close Tab Content-->

	</div><!--Close content posts-->


{!! Theme::asset()->container('footer')->usePath()->add('devloper', 'js/devloper.js') !!}
