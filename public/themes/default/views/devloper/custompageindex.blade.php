<div class="panel panel-default">
@include('flash::message')
<div class="panel-heading no-bg panel-settings">
	<h3 class="panel-title">
		{{ trans('devloper.custom_pages') }}
		<div class="pull-right">
			<a class="btn btn-success" href="{{ url('devloper/custom-pages/create') }}">Create</a>
		</div>
	</h3>

</div>
<div class="panel-body">	
	<div class="announcement-container">	
		<table class="table table-responsive" id="timelines-table">
		    <thead>
		    	<th>{{ trans('devloper.title') }}</th>
		        <th>{{ trans('common.description') }}</th>
		        <th>{{ trans('common.status') }}</th>
		        <th colspan="3">{{ trans('devloper.action') }}</th>
		    </thead>
		    <tbody>
		    @foreach($staticpages as $staticpage)
		        <tr>	        	
		        	<td>{{ $staticpage->title }}</td>
		            <td>{{ substr($staticpage->description,0,50) }}</td>
		            {{-- */ $status = $staticpage->active == 1 ? trans('devloper.active') : trans('devloper.inactive') /* --}}
		            <td>{{ $status }}</td>
					<td><a href="{{ url('devloper/custom-pages/'.$staticpage->id.'/edit')}}">{{ trans('common.edit') }}</a></td>
		        </tr>
		    @endforeach			    
		    </tbody>
		</table>			
	</div>
</div>
</div>