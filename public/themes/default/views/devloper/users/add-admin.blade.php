
<div class="panel panel-default">
    <div class="panel-body">
        @include('flash::message')
        <div class="panel-heading no-bg panel-settings">
            <h3 class="panel-title">
                {{ trans('common.add_admin') }}
            </h3>
        </div>
        @if($mode =="create_admin")
            <form method="POST" action="{{ url('devloper/users/add-devloper') }}" class="socialite-form">
                {{ csrf_field() }}

                <fieldset class="form-group required {{ $errors->has('username') ? ' has-error' : '' }}">
                    {{ Form::label('username', trans('common.username'), ['class' => 'control-label']) }}

                    <input type="text" class="form-control content-form" placeholder="{{ trans('common.username') }}" name="username" required>
                    {{--<small class="text-muted">{{ trans('devloper.user_username_text') }}</small>--}}
                    @if ($errors->has('username'))
                        <span class="help-block">
					<strong>{{ $errors->first('username') }}</strong>
				</span>
                    @endif
                </fieldset>

                <fieldset class="form-group required {{ $errors->has('email') ? ' has-error' : '' }}">
                    {{ Form::label('email', trans('auth.email_address'), ['class' => 'control-label']) }}
                    <input type="email" class="form-control" name="email" placeholder="{{ trans('common.email') }}" required>
                    {{--<small class="text-muted">{{ trans('devloper.user_email_text') }}</small>--}}
                    @if ($errors->has('email'))
                        <span class="help-block">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
                    @endif
                </fieldset>

                <fieldset class="form-group required {{ $errors->has('password') ? ' has-error' : '' }}">
                    {{ Form::label('password', trans('common.password'), ['class' => 'control-label']) }}
                    <input type="password" class="form-control" name="password" placeholder="{{ trans('common.new_password') }}"required>
                    {{--<small class="text-muted">{{ trans('common.new_password_text') }}</small>--}}
                    @if ($errors->has('password'))
                        <span class="help-block">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
                    @endif
                </fieldset>
                <fieldset class="form-group required {{ $errors->has('name') ? ' has-error' : '' }}">
                    {{ Form::label('name', trans('auth.name'), ['class' => 'control-label']) }}
                    <input type="text" class="form-control require-if-active" name="name"  placeholder="Name" required>
                    {{--<small class="text-muted">{{ trans('devloper.user_name_text') }}</small>--}}
                    @if ($errors->has('name'))
                        <span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
                    @endif
                </fieldset>

                <fieldset class="form-group">
                    {{ Form::label('gender', trans('common.gender'), ['class' => 'control-label']) }}
                    {{ Form::select('gender', array('male' => trans('common.male'),'female' => trans('common.female'),'other' => trans('common.other')) , null, ['class' => 'form-control']) }}
                    {{--<small class="text-muted">{{ trans('devloper.user_gender_text') }}</small>--}}
                </fieldset>

                <fieldset class="form-group">

                    {{ Form::hidden('school_id', Auth::user()->school_id) }}
                    {{--<small class="text-muted">{{ trans('devloper.user_gender_text') }}</small>--}}
                </fieldset>

                <fieldset class="form-group">

                    {{ Form::hidden('type', 1) }}
                    {{--<small class="text-muted">{{ trans('devloper.user_gender_text') }}</small>--}}
                </fieldset>

                @if(Setting::get('birthday') == "on")
                    <fieldset class="form-group">
                        {{ Form::label('birthday', trans('common.birthday'), ['class' => 'control-label']) }}
                        <input class="datepicker form-control hasDatepicker" size="16" id="datepick2" name="birthday" type="text" value="" data-date-format="yyyy-mm-dd">
                    </fieldset>
                @endif
                {{--<fieldset class="form-group">--}}
                {{--{{ Form::label('School_id', trans('devloper.school'), ['class' => 'col-sm-2 control-label']) }}--}}


                {{--<i class="fa fa-user">@$school->name</i>--}}{{----}}{{--<h4>({{$school->name}})</h4>--}}
                {{--{{ Form::hidden('school_id', Auth::user()->school_id) }}--}}
                {{--{{ Form::text('school_id',Auth::user()->school_id, ['class' => 'form-control', 'readonly' => 'true']) }}--}}
                {{--{{ Form::text('title',null,['class' => 'form-control']) }}--}}
                {{--{{ Form::number('user_id',null,['class' => 'form-control']) }}--}}
                {{--@else--}}
                {{--{{ Form::text('title', $announcement->user_id, ['class' => 'form-control']) }}--}}
                {{--@endif--}}
                {{--</div>--}}

                {{--</fieldset>--}}



                {{--<fieldset class="form-group">--}}
                {{--{{ Form::label('about', trans('common.about'), ['class' => 'control-label']) }}--}}
                {{--<textarea class="form-control about-form" name="about" rows="3" value="" placeholder="{{ trans('common.about') }}"></textarea>--}}
                {{--<small class="text-muted">{{ trans('devloper.user_about_text') }}</small>--}}
                {{--</fieldset>--}}
                @if(Setting::get('city') == "on")
                    <fieldset class="form-group">
                        {{ Form::label('city', trans('common.current_city'), ['class' => 'control-label']) }}
                        <input type="text" class="form-control" name="city" value="" placeholder="{{ trans('common.current_city') }}">
                        <small class="text-muted">{{ trans('devloper.user_city_text') }}</small>
                    </fieldset>
                @endif

                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm">{{ trans('common.add') }}</button>
                </div>
            </form>

    </div>
</div>
@endif
