<style>

	/* change border radius for the tab , apply corners on top*/

	#exTab3 .nav-pills > li > a {
		border-radius: 4px 4px 0 0 ;
	}

	#exTab3 .tab-content {
		color : white;
		background-color: #428bca;
		padding : 5px 15px;
	}

</style>
<div class="panel panel-default">
	<div id="exTab3" class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			{{ trans('common.manage_users') }}

			@if(Auth::user()->language =='ar')
			<ul class="list-inline">

				<li class="pull-left">
					<span class="pull-right">
				<a href="{{ url('devloper/users/add/create') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
				</li>
				<li class="pull-left">

			<span class="pull-right">
				<a href="{{ url('devloper/users/add/create_devloper') }}" class="btn btn-success">{{ trans('common.create_devloper') }}</a>
			</span>
					</span>
				</li>
			</ul>
			@else
				<ul class="list-inline">

					<li class="pull-right">
					<span class="pull-right">
				<a href="{{ url('devloper/users/add/create') }}" class="btn btn-success">{{ trans('common.create') }}</a>
			</span>
					</li>
					<li class="pull-right">

			<span class="pull-right">
				<a href="{{ url('devloper/users/add/create_devloper') }}" class="btn btn-success">{{ trans('common.create_devloper') }}</a>
			</span>
						</span>
					</li>
				</ul>
				@endif

		</h3>
		<br>
		<br>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#users">{{ trans('common.all_users') }}</a></li>
		<li><a data-toggle="tab" href="#business">{{ trans('common.bus_users') }}</a></li>
		<li><a data-toggle="tab" href="#art">{{ trans('common.art_users') }}</a></li>
		<li><a data-toggle="tab" href="#it">{{ trans('common.it_users') }}</a></li>
		<li><a data-toggle="tab" href="#med">{{ trans('common.mid_users') }}</a></li>
		<li><a data-toggle="tab" href="#eng">{{ trans('common.eng_users') }}</a></li>
		<li><a data-toggle="tab" href="#english">{{ trans('common.english_users') }}</a></li>
		{{--<li><a data-toggle="tab" href="#std">Student Center</a></li>--}}
	</ul>
	<br>
	@include('flash::message')
	<div class="tab-content clearfix">
		<div id="users" class="tab-pane fade in active">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student">{{ trans('common.students') }}</a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#inst">{{ trans('common.instructors') }}</a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#dean">{{ trans('common.deans') }}</a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#devloper">{{ trans('common.devlopers') }}</a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#validator">{{ trans('common.validator') }}</a></li>
				</ul>

				@if(count($users) > 0)
					<div class="tab-content" dir="trl">
						<div id="student" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>

									@foreach($users as $user)
										@foreach($user->roles as $role)
											@if($role->name =='student')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>
													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end students--}}


						<div id="inst" class="tab-pane fade">
							@if(count($users) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users as $user)
											@foreach($user->roles as $role)
												@if($role->name =='instructor')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>
														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>
						{{--end istroctors--}}

						<div id="dean" class="tab-pane fade">
							@if(count($users) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users as $user)
											@foreach($user->roles as $role)
												@if($role->name =='dean')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>
						{{--end Deans--}}

						<div id="validator" class="tab-pane fade">
							@if(count($users) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users as $user)
											@foreach($user->roles as $role)
												@if($role->name =='validator')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>
														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>
						{{--end validator--}}

						<div id="devloper" class="tab-pane fade">
							@if(count($users) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users as $user)
											@foreach($user->roles as $role)
												@if($role->name =='devloper')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>
						{{--end devlopers--}}

					</div>


			</div>
		</div>{{--end of all users--}}


		<div id="business" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_bu">{{ trans('common.students') }}</a></li>
					<li><a data-toggle="pill" href="#inst_bu">{{ trans('common.instructors') }}</a></li>
					<li><a data-toggle="pill" href="#dean_bu">{{ trans('common.deans') }}</a></li>

				</ul>
				@include('flash::message')
				@if(count($users) > 0)
					<div class="tab-content">
						<div id="student_bu" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_bu as $user)
										@foreach($user->roles as $role)
											@if($role->name =='student')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>


													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>
													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end students--}}


						<div id="inst_bu" class="tab-pane fade">
							@if(count($users) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_bu as $user)
											@foreach($user->roles as $role)
												@if($role->name =='instructor')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end istroctors--}}

						<div id="dean_bu" class="tab-pane fade">
							@if(count($users) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_bu as $user)
											@foreach($user->roles as $role)
												@if($role->name =='dean')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end Deans--}}

					</div>


			</div>
		</div>{{--end of business users--}}

		<div id="art" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_art">{{ trans('common.students') }}</a></li>
					<li><a data-toggle="pill" href="#inst_art">{{ trans('common.instructors') }}</a></li>
					<li><a data-toggle="pill" href="#dean_art">{{ trans('common.deans') }}</a></li>

				</ul>
				@include('flash::message')
				@if(count($users_art) > 0)
					<div class="tab-content">
						<div id="student_art" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_art as $user)
										@foreach($user->roles as $role)
											@if($role->name =='student')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end students--}}


						<div id="inst_art" class="tab-pane fade">
							@if(count($users_art) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_art as $user)
											@foreach($user->roles as $role)
												@if($role->name =='instructor')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end istroctors--}}

						<div id="dean_art" class="tab-pane fade">
							@if(count($users_art) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_art as $user)
											@foreach($user->roles as $role)
												@if($role->name =='dean')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end Deans--}}

					</div>


			</div>
		</div>{{--end of art users--}}

		<div id="it" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_it">{{ trans('common.students') }}</a></li>
					<li><a data-toggle="pill" href="#inst_it">{{ trans('common.instructors') }}</a></li>
					<li><a data-toggle="pill" href="#dean_it">{{ trans('common.deans') }}</a></li>

				</ul>
				@include('flash::message')
				@if(count($users_it) > 0)
					<div class="tab-content">
						<div id="student_it" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_it as $user)
										@foreach($user->roles as $role)
											@if($role->name =='student')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end students--}}


						<div id="inst_it" class="tab-pane fade">
							@if(count($users_it) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_it as $user)
											@foreach($user->roles as $role)
												@if($role->name =='instructor')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>
														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

								{{--@else--}}
								{{--<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>--}}
							@endif
						</div>{{--end istroctors--}}

						<div id="dean_it" class="tab-pane fade">
							@if(count($users_it) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_it as $user)
											@foreach($user->roles as $role)
												@if($role->name =='dean')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

								{{--@else--}}
								{{--<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>--}}
							@endif
						</div>{{--end Deans--}}

					</div>


			</div>
		</div>{{--end of IT users--}}

		<div id="med" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_med">{{ trans('common.students') }}</a></li>
					<li><a data-toggle="pill" href="#inst_med">{{ trans('common.instructors') }}</a></li>
					<li><a data-toggle="pill" href="#dean_med">{{ trans('common.deans') }}</a></li>

				</ul>
				@include('flash::message')

				<div class="tab-content">
					<div id="student_med" class="tab-pane fade in active">
						@if(count($users_med) > 0)
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_med as $user)
										@foreach($user->roles as $role)
											@if($role->name =='student')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

						@else
							<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
						@endif
					</div>{{--end students--}}

					<div id="inst_med" class="tab-pane fade">
						@if(count($users_med) > 0)
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_med as $user)
										@foreach($user->roles as $role)
											@if($role->name =='instructor')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>
													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

						@else
							<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
						@endif
					</div>{{--end istroctors--}}

					<div id="dean_med" class="tab-pane fade">
						@if(count($users_med) > 0)
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_med as $user)
										@foreach($user->roles as $role)
											@if($role->name =='dean')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>
													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

						@else
							<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
						@endif
					</div>{{--end Deans--}}

				</div>


			</div>
		</div>{{--end of medical users--}}

		<div id="eng" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_eng">{{ trans('common.students') }}</a></li>
					<li><a data-toggle="pill" href="#inst_eng">{{ trans('common.instructors') }}</a></li>
					<li><a data-toggle="pill" href="#dean_eng">{{ trans('common.deans') }}</a></li>

				</ul>
				@include('flash::message')
				@if(count($users_eng) > 0)
					<div class="tab-content">
						<div id="student_eng" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_eng as $user)
										@foreach($user->roles as $role)
											@if($role->name =='student')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end students--}}


						<div id="inst_eng" class="tab-pane fade">
							@if(count($users_eng) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_eng as $user)
											@foreach($user->roles as $role)
												@if($role->name =='instructor')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end istroctors--}}

						<div id="dean_eng" class="tab-pane fade">
							@if(count($users_eng) > 0)
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_eng as $user)
											@foreach($user->roles as $role)
												@if($role->name =='dean')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							@else
								<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
							@endif
						</div>{{--end Deans--}}

					</div>


			</div>
		</div>{{--end of enginnring users--}}

		<div id="english" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_english">{{ trans('common.students') }}</a></li>
					<li><a data-toggle="pill" href="#inst_english">{{ trans('common.instructors') }}</a></li>
					<li><a data-toggle="pill" href="#dean_english">{{ trans('common.deans') }}</a></li>

				</ul>
				@include('flash::message')
				@if(count($users_english) > 0)
					<div class="tab-content">
						<div id="student_english" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th>{{ trans('devloper.id') }}</th>
										<th>{{ trans('auth.username') }}</th>
										<th>{{ trans('auth.name') }}</th>
										<th>{{ trans('common.email') }}</th>
										<th>{{ trans('common.status') }}</th>
										<th>{{ trans('devloper.options') }}</th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									@foreach($users_english as $user)
										@foreach($user->roles as $role)
											@if($role->name =='student')
												<tr>
													<td>&nbsp;</td>
													<td>{{ $user->id }}</td>
													<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

													<td>{{ $user->timeline->name }}</td>
													<td>{{ $user->email }}</td>
													<td>
														@if($user->active == "1")
															<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
														@else
															<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
														@endif
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											@endif
										@endforeach
									@endforeach
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								{{ $users->render() }}
							</div>

							{{--@else--}}
								{{--<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>--}}
							{{--@endif--}}
						</div>{{--end students--}}


						<div id="inst_english" class="tab-pane fade">

								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_english as $user)
											@foreach($user->roles as $role)
												@if($role->name =='instructor')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							{{--@else--}}
								{{--<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>--}}
							{{--@endif--}}
						</div>{{--end istroctors--}}

						<div id="dean_english" class="tab-pane fade">

								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>{{ trans('devloper.id') }}</th>
											<th>{{ trans('auth.username') }}</th>
											<th>{{ trans('auth.name') }}</th>
											<th>{{ trans('common.email') }}</th>
											<th>{{ trans('common.status') }}</th>
											<th>{{ trans('devloper.options') }}</th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										@foreach($users_english as $user)
											@foreach($user->roles as $role)
												@if($role->name =='dean')
													<tr>
														<td>&nbsp;</td>
														<td>{{ $user->id }}</td>
														<td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->username }}</a></td>

														<td>{{ $user->timeline->name }}</td>
														<td>{{ $user->email }}</td>
														<td>
															@if($user->active == "1")
																<a href="{{ url('devloper/deactivate-user/'.$user->id)}}" class="btn btn-success announcement-status" onclick="return confirm('{{ trans("messages.are_you_sure_deactivate") }}')">Activate</a>
															@else
																<a href="{{ url('devloper/activate-user/'.$user->id)}}" class="btn btn-danger announcement-status" >Deactivate</a>
															@endif
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="{{ url('devloper/users/'.$user->timeline->username.'/edit') }}"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																{{--<li><a href="{{ url('devloper/users/'.$user->id.'/delete')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>--}}
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												@endif
											@endforeach
										@endforeach
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									{{ $users->render() }}
								</div>

							{{--@else--}}
								{{--<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>--}}
							{{--@endif--}}
						</div>{{--end Deans--}}

					</div>
				@else
					<div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
				@endif
			</div>



			</div>{{--end of english users--}}

		</div>
	</div>
</div>
