<!-- main-section -->
	<!-- <div class="main-content"> -->
		<div class="container">
			<div class="row">
				<div class="visible-lg col-lg-2">
					{!! Theme::partial('home-leftbar',compact('trending_tags')) !!}
				</div>



				<div class="1">
							<div class="crp" style="float: right;



position: relative;width: 33%;" >

						{!! Theme::partial('create-post',compact('timeline','block_user','block_users')) !!}
					</div>
                <div class="col-md-7 col-lg-6">
			   		@if (Session::has('message'))
				        <div class="alert alert-{{ Session::get('status') }}" role="alert">
				            {{ Session::get('message') }}
				        </div>
				    @endif

						@if($active_announcements_admin !=null)
						@foreach($active_announcements_admin as $announcement)
							{{--{{$chk_isExpire = $announcement->chkAnnouncementExpire($announcement->id)}}--}}
						@if($announcement->chkAnnouncementExpire($announcement->id)== "notexpired")
							{{--{{$active_announcement = $announcement}}--}}
									@if(!$announcement->users->contains(Auth::user()->id))
										{{$announcement->users()->attach(Auth::user()->id)}}
									@endif
						<div class="announcement alert alert-danger">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<h3>{{ $announcement->title }}</h3>
							<p>{{ $announcement->description }}</p>
							@if($announcement->school_id == 1)
							<p>Admin</p>

						</div>
								@endif
								@endif
							@endforeach
					@endif
						@if($active_announcements_dean !=null)
							@foreach($active_announcements_dean as $announcement)
								@if($announcement->school_id != 1)

								{{--{{$chk_isExpire = $announcement->chkAnnouncementExpire($announcement->id)}}--}}
								@if($announcement->chkAnnouncementExpire($announcement->id)== "notexpired")
									{{--{{$active_announcement = $announcement}}--}}
									@if(!$announcement->users->contains(Auth::user()->id))
										{{$announcement->users()->attach(Auth::user()->id)}}
									@endif
									<div class="announcement alert alert-success">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<h3>{{ $announcement->title }}</h3>
										<p>{{ $announcement->description }}</p>
										@if($announcement->school_id == Auth::user()->school_id)
											<p>Dean</p>
										@endif

									</div>
									@endif
								@endif
							@endforeach
						@endif

						@if($active_announcements_std !=null)
							@foreach($active_announcements_std as $announcement)
								{{--{{$chk_isExpire = $announcement->chkAnnouncementExpire($announcement->id)}}--}}
								@if($announcement->chkAnnouncementExpire($announcement->id)== "notexpired")
									{{--{{$active_announcement = $announcement}}--}}
									@if(!$announcement->users->contains(Auth::user()->id))
									{{$announcement->users()->attach(Auth::user()->id)}}
									@endif
									<div class="announcement alert alert-info">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<h3>{{ $announcement->title }}</h3>
										<p>{{ $announcement->description }}</p>
										@if($announcement->school_id == 8)
											<p>Student Center</p>
										@endif
									</div>

								@endif
							@endforeach
						@endif
						{{--@if(isset($active_announcement2))--}}
							{{--<div class="announcement alert alert-info">--}}
								{{--<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>--}}
								{{--<h3>{{ $active_announcement2->title }}</h3>--}}
								{{--<p>{{ $active_announcement2->description }}</p>--}}
								{{--<p>{{ $active_announcement2->school_id }}</p>--}}
							{{--</div>--}}
						{{--@endif--}}





					<div class="timeline-posts">
						@if($posts->count() > 0)
							@foreach($posts as $post)
								{!! Theme::partial('post',compact('post','timeline','next_page_url')) !!}
							@endforeach
						@else
							<div class="no-posts alert alert-warning" style="min-height: 200px;min-height: 200px;

padding-top: 50px;
font-size: 20px;">{{ trans('common.no_posts') }}</div>
						@endif
							<div class="homeleftbar2" style="">
							{!! Theme::partial('home-rightbar',compact('suggested_users')) !!}
							</div>
					</div>
				</div></div>

				<div class="homeleftbar1" style="">
						<div class="col-md-5 col-lg-4" style="">


							{!! Theme::partial('home-rightbar',compact('suggested_users')) !!}


						</div><!-- /col-md-6 -->
						</div>



			</div>
		</div>


    {{--/--}}


