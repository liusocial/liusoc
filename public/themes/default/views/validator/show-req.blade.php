<div class="panel panel-default">
    <div class="panel-heading no-bg panel-settings">
        <h3 class="panel-title">
            {{ trans('admin.users_acc_req') }}
        </h3>
    </div>
    <div class="panel-body timeline">
        @include('flash::message')
        @if(count($users) > 0)
            <div class="table-responsive manage-table">
                <table class="table existing-products-table liusocial">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>{{ trans('auth.st_id') }}</th>
                        <th>{{ trans('auth.name') }}</th>
                        <th>{{ trans('auth.major') }}</th>

                        <th>{{ trans('common.email') }}</th>
                        <th>{{ trans('admin.user_req_date') }}</th>

                        <th>{{ trans('admin.approve_req') }}</th>
                        <th>{{ trans('admin.reject_req') }}</th>


                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>&nbsp;</td>
                            <td>{{ $user->st_id }}</td>
                            <td><a href="#"><img src="{{ $user->avatar }}" alt="images"></a><a href="{{ url($user->timeline->username) }}"> {{ $user->timeline->name }}</a></td>
                            <td>{{ $user->school->name }}</td>

                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>

                                <a href="{{ url('validator/requests/'.$user->id.'/approve')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class=" btn btn  btn-info btn-lg"><i class="glyphicon glyphicon-ok" aria-hidden="true">Accept</i></span></a>
                            </td>
                            <td>
                                   <a href="{{ url('validator/requests/'.$user->id.'/reject')}}" onclick="return confirm('{{ trans("messages.are_you_sure") }}')"><span class="btn btn btn-danger btn-lg"><i class="glyphicon glyphicon-remove" aria-hidden="true">Reject</i></span></a>



                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagination-holder">
                {{ $users->render() }}
            </div>
        @else
            <div class="alert alert-warning">{{ trans('messages.no_users') }}</div>
        @endif
    </div>
</div>
