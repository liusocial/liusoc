<?php
/*************************************************************************
 Generated via "php artisan localization:missing" at 2016/08/06 13:41:49
*************************************************************************/

return  [
  //==================================== Translations ====================================//
  'affiliate_code'            => 'Affiliate code',
  'already_have_an_account'   => 'هذا الحساب موجود',
  'confirm_password'          => 'تأكيد كلمة السر',
  'dont_have_an_account_yet'  => 'لا تملك حسابا حتى الآن؟',
  'email_address'             => 'عنوان الايميل الالكتروني',
  'enter_email_or_username'   => 'أدخل البريد الإلكتروني أو اسم المستخدم',
  'forgot_password'           => 'هل نسيت كلمة السر',
  'get_started'               => 'ابدء',
  'login_failed'              => 'اعتماد تسجيل الدخول غير صالح.',
  'login_success'             => 'النجاح في تسجيل الدخول',
  'login_via_social_networks' => 'تسجيل الدخول عبر الشبكات الاجتماعية',
  'login_welcome_heading'     => 'مرحبا بعودتك! الرجاء تسجيل الدخول.',
  'reset_welcome_heading'     => 'إعاد ضبط كلمة مرورك.',
  'name'                      => 'الأسم',
    'username'                => 'الأسم الحساب',
  'password'                  => 'كلمة المرور',
  'register'                  => 'تسجيل',
  'login'                     => 'تسجيل الدخول',
  'registered_verify_email'   => 'لقد قمت بالتسجيل بنجاح!<br>يرجى التحقق من بريدك الالكتروني قبل تسجيل الدخول.',
  'reset_password'            => 'إعادة ضبط كلمة المرور',
  'reset_your_password'       => 'إعاد ضبط كلمة مرورك.',
  'select_gender'             => 'حدد نوع الجنس',
  'send_password_reset_link'  => 'إرسال رابط إعادة تعيين كلمة المرور',
  'sign_in'                   => 'تسجيل الدخول',
  'signin_to_dashboard'       => 'تسجيل الدخول إلى Dashboard',
  'signup_to_dashboard'       => 'سجل',
  'verify_email'              => 'يرجى التحقق من بريدك الالكتروني.',
  'welcome_to'                => 'مرحبا بك في',
  'subject'                   => 'موضوع',
  'submit'                    => 'قدم',
    'st_id'                    => 'الرقم الجامعي للطالب',
    'dep'                    => 'قسم',
];
