<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'no_followers'              => 'لا يوجد متابعين بعد!',
    'no_following'              => 'لا يتابع أي شخص!',
    'no_likes'                  => 'لم يتم تلقي أي إعجاب حتى الآن!',
    'no_posts'                  => 'لم يتم العثور على منشورات ...',
    'no_tags'                   => 'لا توجد أشارات بعد!',
    'no_suggested_users'        => 'لا يوجد مستخدمون مقترحون بعد!',
    'search_people_placeholder' => 'البحث عن اشخاص',
    'search_placeholder'        => 'ابحث عن الأشخاص او تداولات اوالصفحات اوالمجموعات',
    'post-placeholder'          => 'اكتب شيئا .....#هاشتاج @أشارات',
    'view-previous-messages'    => 'عرض الرسائل السابقة',
    'whats-going-on'            => 'شارك أحداثك الخاصة؟',
    'click-earn-revenue'        => 'انقر فوق هذه الأزرار وكسب المزيد من الإيرادات!',
    'pages-manage'              => 'الصفحات التي تديرها',
    'pages-member-in'           => 'عضو في الصفحات التالية ',
    'groups-manage'             => 'المجموعات التي تديرها',
    'groups-member-in'             => 'عضو في المجموعات التالية ',
    'no-groups'                 => 'ليس لديك أي مجموعات لإدارتها',
    'no-liked-pages'            => 'لم تعجب بأي صفحات حتى الآن!',
    'no-joined-goups'           => 'لم تنضم الى أي مجموعات حتى الآن!',
    'no_requests'               => 'لا توجد طلبات بعد!',
    'no_notifications'          => 'ليس لديك أي إشعارات',
    'no_messages'               => 'ليس لديك أي رسائل',
    'no_members'                => 'لا يوجد أعضاء بعد!',
    'no_users'                  => 'لم تتم إضافة أي مستخدم حتى الآن!',
    'no_pages'                  => 'لا توجد صفحات أضيفت بعد!',
    'no_groups'                 => 'لا توجد مجموعات أضيفت بعد!',
    'no_reports'                => 'لا توجد بلاغات لمعاجتها!',
    'no_affliates'              => 'No affliates yet!',
    'no_description'            => 'لا يوجد وصف مضاف!',
    'comment_placeholder'       => 'اكتب تعليقًا ... اضغط على Enter للنشر',
    'get_more_posts'            => 'الحصول على المزيد من المنشورات',
    'create_page_placeholder'   => 'اكتب عن صفحتك ...',
    'create_group_placeholder'  => 'اكتب عن مجموعتك ...',
    'about_page_heading1'       => 'العلامة التجارية لصفحتك',
    'about_page_content1'       => 'أضف صورة غلاف فريدة واعرض أهم الأخبار في بروفايل الصفحة.',
    'about_page_heading2'       => 'تواصل مع مجموعة واسعة من الناس',
    'about_page_content2'       => 'نشر تحديثات جديدة حتى يعرف الأشخاص الذين يهتمون بما يجري وما يهم.',

    'about_group_heading1'        => 'شارك أشياء مختلفة مع أشخاص مختلفين',
    'about_group_heading2'        => 'من الذي يمكنك إضافته إلى مجموعة؟',
    'about_group_heading3'        => 'من يمكنه الانضمام إلى مجموعتك؟',
    'about_group_content1'        => 'تتيح لك المجموعات مشاركة الأشياء مع الأشخاص الذين سيهتمون بهم كثيرًا. من خلال إنشاء مجموعة لكل جزء من أجزاء حياتك المهمة - الزملاء في الفريق وزملاء الدراسة - يمكنك تحديد من يشاهد ما تشاركه.',
    'about_group_content2'        => 'يمكنك إضافة أي شخص تتابعه / صديق معه.',
    'about_group_content3'        => 'يمكن لأي شخص الانضمام إلى مجموعتك إذا تم تعيين خصوصية مجموعتك على "فتح". بالنسبة للمجموعات المغلقة ، يمكن لأي شخص طلب الانضمام ولكن يجب قبول الطلبات من قِبل المشرفين أولاً. المجموعات السرية غير مرئية لأي شخص ولكن فقط أعضاء المجموعة.',
    'radio_open_group'            => 'يمكن لأي شخص مشاهدة المجموعة والانضمام إليها.',
    'radio_closed_group'          => 'يمكن لأي شخص مشاهدة وطلب الانضمام إلى المجموعة. الطلبات يمكن قبولها أو رفضها من قبل المشرفين.',
    'radio_secret_group'          => 'يمكن للأعضاء فقط الوصول إلى المجموعة.',
    'learn_more_about_groups'     => 'تعرف على المزيد حول المجموعات',
    'about_user_placeholder'      => 'أدخل وصفًا عنك',
    'enter_old_password'          => 'أدخل كلمة المرور القديمة',
    'enter_new_password'          => 'أدخل كلمة المرور الجديدة',
    'confirm_deactivate_question' => 'هل أنت متأكد من أنك تريد إلغاء تنشيط حسابك؟',
    'yes_deactivate'              => 'نعم ، إلغاء التنشيط',
    'menu_message_general'        => 'يمكنك تغيير الإعدادات العامة',
    'menu_message_privacy'        => 'يمكنك تغيير اعدادات الخصوصية',
    'menu_message_notifications'  => 'إدارة إشعارات البريد الإلكتروني الخاصة بك',
    'menu_message_affiliates'     => 'List of your affiliates',
    'menu_message_connections'    => 'يمكنك الاتصال بالخدمات',
    'menu_message_deactivate'     => 'يمكنك إلغاء تفعيل حسابك',
    'menu_message_admin_roles'    => 'أدر المهام هنا',
    'menu_message_messages'       => 'قائمة الرسائل',
    'menu_message_page_likes'     => 'قائمة المعجبين بها',
    'no_admin'                    => 'لا مشرفين حتى الآن!',
    'who_are_you_with'            => 'مع من انت؟',
    'what_are_you_watching'       => 'ماذا تشاهد الان؟',
    'upload_file'                 => 'ما رابط الملف الذي تود مشاركته؟',
    'what_are_you_listening_to'   => 'الى ماذا تستمع؟',
    'where_are_you'               => 'أين أنت؟',
    'no_announcements'            => 'لا تنبيهات حتى الآن!',
    'mark_all_read'               => 'ضع علامة بأنه تم القراءة',

    /****** Album form ******/
    'create_a_new_album'       => 'إنشاء ألبوم جديد',
    'create_a_new_message'     => 'إنشاء رسالة جديد',
    'enter_album_name'         => 'ادخل أسم الألبوم',
    'name_of_the_album'        => 'أسم الألبوم',
    'description_about_album'  => 'وصف حول الألبوم',
    'location_of_the_album'    => 'موقع الألبوم',
    'click_below_to_upload'    => 'انقر أدناه للتحميل',
    'only_jpeg_upto_20mb_each' => 'فقط JPEG تصل إلى 20 MB ',
    'get_notification_text'    => 'سيتم إعلامك للاشعارات والتعليقات والمشاركات',
    'stop_notification_text'   => 'لن يتم إشعارك',
    'edit_text'                => 'يمكنك تعديل منشورك',
    'delete_text'              => 'سيتم حذف هذ المنشور',
    'report_text'              => 'سيتم الإبلاغ عن هذا المنشور',
    'manage_roles_text'        => 'إدارة المهام لأعضاء صفحتك',
    'no_members_to_admin'      => 'لا يوجد أعضاء لإضافة كمشرف!',
    'name_placeholder'         => 'أدخل أسمك',
    'email_placeholder'        => 'أدخل البريد الإلكتروني الأصلي',
    'subject_placeholder'      => 'الموضوع الرئيسي',
    'message_placeholder'      => 'أدخل رسالتك',

    'are_you_sure'                        => 'هل أنت متأكد؟',
    'are_you_sure_deactivate'             => 'هل أنت متأكد من تعطيل هذا الحساب؟',
    'role_assigned_success'               => 'المهمة عينت بنجاح',
    'role_assigned_failure'               => ' هناك مشكلة  في تعيين المهمة!',
    'request_accepted'                    => 'تم قبول الطلب',
    'request_rejected'                    => 'تم رفض الطلب',
    'page_created_success'                => 'تم أنشاء الصفحة بنجاح',
    'page_updated_success'                => 'تم تحديث الصفحة بنجاح',
    'page_deleted_success'                => 'تم حذف الصفحة بنجاح',
    'group_created_success'               => 'تم أنشاء المجموعة بنجاح',
    'group_updated_success'               => 'تم تحديث المجموعة بنجاح',
    'group_deleted_success'               => 'تم حذف المجموعة بنجاح',
    'settings_updated_success'            => 'تم تحديث الإعدادات بنجاح',
    'user_settings_updated_success'       => 'تم تحديث إعدادات المستخدم بنجاح',
    'user_updated_success'                => 'تم تحديث المستخدم بنجاح',
    'user_added_success'                  => 'تم اضافة المستخدم بنجاح',
    'user_approved_success'               => 'تمت الموافقة على حساب المستخدم بنجاح',
    'user_reject'                         => 'تم رفض طلب حساب المستخدم',

    'user_deleted_success'                => 'تم حذف المستخدم بنجاح',
    'password_updated_success'            => 'تم تحديث كلمة المرور بنجاح',
    'page_settings_updated_success'       => 'تم تحديث إعدادات الصفحة بنجاح',
    'group_settings_updated_success'      => 'تم تحديث إعدادات المجموعة بنجاح',
    'announcement_updated_success'        => 'تم تحديث التنبية بنجاح',
    'announcement_deleted_success'        => 'تم حذف التنبية بنجاح',
    'chat_deleted_success'                => 'تم حذف المحادثة بنجاح',
    'announcement_activated_success'      => 'تم تفعيل التنبية بنجاح',
    'account_activated_success'           => 'تم تفعيل الحساب بنجاح',
    'announcement_inactivated_success'    => 'تم الغاء التفعيل التنبية بنجاح',
    'account_deactivated_success'         => 'تم الغاء التفعيل الحساب بنجاح',
    'new_announcement_added'              => 'تم أضافة تنبية جديد بنجاح',
    'new_block_added'                     => 'تم أضافة حظر جديد بنجاح',
    'block_updated_success'               => 'تم تحديث الحظر بنجاح',
    'general_settings_updated_success'    => 'تم تحديث الإعدادات العامة بنجاح',
    'privacy_settings_updated_success'    => 'تم تحديث الإعدادات الخاصة بنجاح',
    'new_password_updated_success'        => 'تم تحديث كلمة مرورك الجديدة بنجاح',
    'password_no_match'                   => 'كلمة المرور الجديدة غير متطابقة مع كلمة المرور الحالية',
    'old_password_no_match'               => 'كلمة المرور القديمة غير متطابقة',
    'email_notifications_updated_success' => 'تم تحديث إشعارات البريد الإلكتروني بنجاح',
    'timeline_saved_success'              => 'تم حفظ الملف الشخصي بنجاح',
    'timeline_deleted_success'            => 'تم حذف الملف الشخصي بنجاح',
    'report_mark_safe'                    => 'وضع البلاغ على أنه آمن',
    'report_deleted_success'              => 'تم حذف البلاغ بنجاح',
    'ads_updated_success'                 => 'تم تحديث الإعلانات العامة بنجاح',
    'page_active'                         => 'هل ترغب في جعل الصفحة نشطة؟',
    'no_deans'                            => 'لم يتم اضافة عميد لهذة الصفحة بعد.... ',

    /*
     *
     * Shared translations.
     *
     */
    'title'   => 'Laravel Installer',
    'next'    => 'Next Step',
    'install' => 'Install',


    /*
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Welcome To The Installer',
        'message' => 'Welcome to the setup wizard.',
    ],


    /*
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requirements',
    ],


    /*
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissions',
    ],


    /*
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title'   => 'Environment Settings',
        'save'    => 'Save .env',
        'success' => 'Your .env file settings have been saved.',
        'errors'  => 'Unable to save the .env file, Please create it manually.',
    ],


    /*
     *
     * Final page translations.
     *
     */
    'final' => [
        'title'    => 'Finished',
        'finished' => 'Application has been successfully installed.',
        'exit'     => 'Click here to exit',
    ],

];
