<!-- <div class="main-content"> -->
<?php if(Auth::user()->hasRole('admin')): ?>




            <div class="panel panel-default">
                <div class="panel-heading no-bg panel-settings">

                    <h3 class="panel-title"><?php echo e(trans('common.create_page')); ?></h3>
                </div>
                <?php if($mode =="create"): ?>
                    <div class="panel-body">
                        <div class="liusocial-form">
                            <?php if(isset($message)): ?>
                                <div class="alert alert-success">
                                    <?php echo e($message); ?>

                                </div>
                            <?php endif; ?>

                            <form class="margin-right" method="POST" action="<?php echo e(url('admin/pages')); ?>">
                                <?php echo e(csrf_field()); ?>


                                <fieldset class="form-group required <?php echo e($errors->has('category') ? ' has-error' : ''); ?>">
                                    <?php echo e(Form::label('category', trans('common.category'), ['class' => 'control-label'])); ?>


                                    <?php echo e(Form::select('category', array('' => trans('common.select_category'))+ $category_options, '', array('class' => 'form-control'))); ?>

                                    <?php if($errors->has('category')): ?>
                                        <span class="help-block">
										<strong><?php echo e($errors->first('category')); ?></strong>
									</span>
                                    <?php endif; ?>

                                </fieldset>

                                <fieldset class="form-group">
                                    <?php echo e(Form::label('school', trans('common.school'), ['class' => 'control-label'])); ?>


                                    <?php echo e(Form::select('school_id', array('' => trans('common.select_school'))+ $school_options, '', array('class' => 'form-control'))); ?>

                                    <?php if($errors->has('school')): ?>
                                        <span class="help-block">
										<strong><?php echo e($errors->first('school')); ?></strong>
									</span>
                                    <?php endif; ?>

                                    <?php /*<?php echo e(Form::hidden('school_id', Auth::user()->school_id)); ?>*/ ?>
                                    <?php /*<?php echo e(Form::label('school', trans('common.school'), ['class' => 'control-label'])); ?>*/ ?>
                                    <?php /*<?php echo e(Form::select('school', array('7' => trans('common.business'),'2' => trans('common.art'),'3' => trans('common.engineering'),'4' => trans('common.it'),'5' => trans('common.medicine'),'6' => trans('common.english')) , null, ['class' => 'form-control'])); ?>*/ ?>
                                    <?php /*<?php echo e(Form::text('school_id',Auth::user()->school_id, ['class' => 'form-control', 'readonly' => 'true'])); ?>*/ ?>
                                </fieldset>

                                <fieldset class="form-group required <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <?php echo e(Form::label('name', trans('auth.name'), ['class' => 'control-label'])); ?>

                                    <?php echo e(Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('common.name_of_your_page')])); ?>

                                    <?php if($errors->has('name')): ?>
                                        <span class="help-block">
										<strong><?php echo e($errors->first('name')); ?></strong>
									</span>
                                    <?php endif; ?>
                                </fieldset>
                                <fieldset class="form-group required <?php echo e($errors->has('username') ? ' has-error' : ''); ?>">
                                    <?php echo e(Form::label('username', trans('common.username'), ['class' => 'control-label'])); ?>

                                    <?php echo e(Form::text('username', old('username'), ['class' => 'form-control','maxlength' => '26', 'placeholder' => trans('common.username')])); ?>

                                    <?php if($errors->has('username')): ?>
                                        <span class="help-block">
										<strong><?php echo e($errors->first('username')); ?></strong>
									</span>
                                    <?php endif; ?>

                                </fieldset>
                                <fieldset class="form-group">
                                    <?php echo e(Form::label('about', trans('common.about'), ['class' => 'control-label'])); ?>

                                    <?php echo e(Form::textarea('about', old('about'), ['class' => 'form-control', 'placeholder' => trans('messages.create_page_placeholder'), 'rows' => '4', 'cols' => '20'])); ?>

                                </fieldset>

                                <div class="pull-right">
                                    <?php echo e(Form::submit(trans('common.create_page'), ['class' => 'btn btn-success'])); ?>

                                </div>
                                <div class="clearfix"></div>

                            </form>
                        </div>
                    </div><!-- /panel-body -->
            </div>


        <?php /*<div class="col-md-4">*/ ?>
            <?php /*<div class="panel panel-default">*/ ?>
                <?php /*<div class="panel-heading no-bg panel-settings">*/ ?>
                    <?php /*<h3 class="panel-title"><?php echo e(trans('common.about').' '.trans('common.pages')); ?></h3>*/ ?>
                <?php /*</div>*/ ?>
                <?php /*<div class="panel-body right-panel">*/ ?>
                    <?php /*<div class="privacy-question">*/ ?>
                        <?php /*<ul class="list-group right-list-group">*/ ?>
                            <?php /*<li href="#" class="list-group-item">*/ ?>
                                <?php /*<div class="holder">*/ ?>
                                    <?php /*<div class="about-page">*/ ?>
                                        <?php /*<?php echo e(Form::label('about_page_heading1', trans('messages.about_page_heading1'), ['class' => 'right-side-label'])); ?>*/ ?>
                                    <?php /*</div>*/ ?>
                                    <?php /*<div class="page-description">*/ ?>
                                        <?php /*<?php echo e(trans('messages.about_page_content1')); ?>*/ ?>
                                    <?php /*</div>*/ ?>
                                <?php /*</div>*/ ?>
                            <?php /*</li>*/ ?>
                            <?php /*<li href="#" class="list-group-item">*/ ?>
                                <?php /*<div class="holder">*/ ?>
                                    <?php /*<div class="about-page">*/ ?>
                                        <?php /*<?php echo e(Form::label('about_page_heading2', trans('messages.about_page_heading2'), ['class' => 'right-side-label'])); ?>*/ ?>
                                    <?php /*</div>*/ ?>
                                    <?php /*<div class="page-description">*/ ?>
                                        <?php /*<?php echo e(trans('messages.about_page_content2')); ?>*/ ?>
                                    <?php /*</div>*/ ?>
                                <?php /*</div>*/ ?>
                            <?php /*</li>*/ ?>
                        <?php /*</ul><!-- /list-group -->*/ ?>
                    <?php /*</div>*/ ?>
                <?php /*</div><!-- /panel-body -->*/ ?>
            <?php /*</div>*/ ?>

            <?php /*<?php if(Setting::get('createpage_ad') != NULL): ?>*/ ?>
                <?php /*<div id="link_other" class="page-image">*/ ?>
                    <?php /*<?php echo htmlspecialchars_decode(Setting::get('createpage_ad')); ?>*/ ?>
                <?php /*</div>*/ ?>
            <?php /*<?php endif; ?>*/ ?>
        <?php /*</div><!-- /col-md-4 -->*/ ?>


<!-- </div> -->
<?php endif; ?>

<?php endif; ?>
