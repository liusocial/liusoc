<div class="panel panel-default">
	<div class="panel-body">
	<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="panel-heading no-bg panel-settings">
			<h3 class="panel-title">
				<?php echo e(trans('admin.edit_page')); ?> (<?php echo e($timeline->name); ?>)
			</h3>
		</div>
		<form class="socialite-form"  method="POST" action="<?php echo e(url('dean/pages/'.$username.'/edit')); ?>">
			<?php echo e(csrf_field()); ?>

			<fieldset class="form-group">
				<label for="verified"><?php echo e(trans('admin.verified')); ?></label>	
				<?php echo e(Form::select('verified', array('1' => 'Yes','0' => 'No'),$page->verified, ['class' => 'form-control', 'placeholder' => trans('admin.please_select') ])); ?>

				<small class="text-muted"><?php echo e(trans('admin.verified_page_text')); ?></small>				
			</fieldset>

			<fieldset class="form-group">
				<label for="active"><?php echo e(trans('admin.active')); ?></label>	
				<?php echo e(Form::select('active', array('1' => 'Yes','0' => 'No'),$page->active, ['class' => 'form-control', 'placeholder' => trans('admin.please_select') ])); ?>

				<small class="text-muted"><?php echo e(trans('messages.page_active')); ?></small>				
			</fieldset>

			<fieldset class="form-group required <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
				<label for="name"><?php echo e(trans('auth.name')); ?></label>
				<input type="text" class="form-control" name="name" value="<?php echo e($timeline->name); ?>" placeholder="<?php echo e(trans('auth.name')); ?>">
				<small class="text-muted"><?php echo e(trans('admin.page_name_placeholder')); ?></small>
				<?php if($errors->has('name')): ?>
				<span class="help-block">
					<strong><?php echo e($errors->first('name')); ?></strong>
				</span>
				<?php endif; ?>
			</fieldset>

			<fieldset class="form-group">
				<label for="username"><?php echo e(trans('common.username')); ?></label>
				<input type="text" name="username" class="form-control content-form" value="<?php echo e($timeline->username); ?>" placeholder="<?php echo e(trans('common.username')); ?>" readonly>
				<small class="text-muted"><?php echo e(trans('admin.page_username_text')); ?></small>
			</fieldset>	

			<fieldset class="form-group">
				<label for="about"><?php echo e(trans('common.about')); ?></label>
				<textarea class="form-control about-form" name="about" placeholder="<?php echo e(trans('messages.create_page_placeholder')); ?>" rows="3"><?php echo e($timeline->about); ?></textarea>
				<small class="text-muted"><?php echo e(trans('admin.page_about_text')); ?></small>
			</fieldset>

			<fieldset class="form-group required <?php echo e($errors->has('category_id') ? ' has-error' : ''); ?>">
				<label for="category_id"><?php echo e(trans('common.category')); ?></label>
				<?php echo e(Form::select('category_id', array('' => trans('admin.please_select'))+ $category_options, $page->category_id , array('class' => 'form-control'))); ?>

				<small class="text-muted"><?php echo e(trans('admin.page_category_text')); ?></small>
				<?php if($errors->has('category_id')): ?>
				<span class="help-block">
					<strong><?php echo e($errors->first('category_id')); ?></strong>
				</span>
				<?php endif; ?>
			</fieldset>

			<fieldset class="form-group">
				<label for="thisissometext"><?php echo e(trans('common.address')); ?></label>
				<input type="text" class="form-control" name="address" value="<?php echo e($page->address); ?>" placeholder="<?php echo e(trans('common.address')); ?>">
				<small class="text-muted"><?php echo e(trans('admin.page_address_text')); ?></small>
			</fieldset>		

			<fieldset class="form-group">
				<label for="thisissometext"><?php echo e(trans('common.phone')); ?></label>
				<input type="text" name="phone" class="form-control content-form" value="<?php echo e($page->phone); ?>" placeholder="<?php echo e(trans('common.phone')); ?>">
				<small class="text-muted"><?php echo e(trans('admin.page_phone_text')); ?></small>
			</fieldset>		

			<fieldset class="form-group">
				<label for="thisissometext"><?php echo e(trans('common.website')); ?></label>
				<input type="text" name="website" class="form-control content-form" value="<?php echo e($page->website); ?>" placeholder="<?php echo e(trans('common.website')); ?>">
				<small class="text-muted"><?php echo e(trans('admin.page_website_text')); ?></small>
			</fieldset>

			<fieldset class="form-group">
				<label for="thisissometext"><?php echo e(trans('admin.timeline_post_privacy')); ?></label>
					<?php echo e(Form::select('timeline_post_privacy', array('everyone' => trans('common.everyone'), 'only_admins' => trans('admin.only_admins'), 'none' => trans('common.no_one')), $page->timeline_post_privacy, ['class' => 'form-control'])); ?>	
				<small class="text-muted"><?php echo e(trans('admin.timeline_post_privacy_page_text')); ?></small>				
			</fieldset>

			<fieldset class="form-group">
				<label for="thisissometext"><?php echo e(trans('admin.add_privacy')); ?></label>
				<?php echo e(Form::select('member_privacy', array('members' => 'Members','only_admins' => 'Only admins') , $page->member_privacy , ['class' => 'form-control'])); ?>

				<small class="text-muted"><?php echo e(trans('admin.add_privacy_text')); ?></small>				
			</fieldset>

			<div class="pull-right">
				<button type="submit" class="btn btn-primary btn-sm"><?php echo e(trans('common.save_changes')); ?></button>
			</div>
		</form>
	</div>
</div>
