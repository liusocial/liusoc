
<div class="panel panel-default">
	<div class="panel-heading no-bg panel-settings">
	<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<h3 class="panel-title">
			<?php echo e(trans('admin.create_announcement')); ?>

		</h3>
	</div>
	<div class="panel-body">		
	<?php if($mode=="create"): ?>
		<form method="POST" class="liusocial-form" action="<?php echo e(url('admin/announcements')); ?>">
	<?php else: ?>
		<form method="POST" class="liusocial-form" action="<?php echo e(url('admin/announcements/'.$announcement->id.'/update')); ?>">
	<?php endif; ?>		    
	
	<?php echo e(csrf_field()); ?>

		<div class="form-horizontal announcements">
			<div class="form-group required <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
			    <?php echo e(Form::label('title', trans('admin.title'), ['class' => 'col-sm-2 control-label'])); ?>

			    <div class="col-sm-10">
			      <?php if($mode == "create"): ?>
			      	<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

			      <?php else: ?>
			      	<?php echo e(Form::text('title', $announcement->title, ['class' => 'form-control'])); ?>

			      <?php endif; ?>
			      
			      <?php if($errors->has('title')): ?>
			      <span class="help-block">
			      	<strong><?php echo e($errors->first('title')); ?></strong>
			      </span>
			      <?php endif; ?>
			    </div>
			</div>
			<div class="form-group required <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
			    <?php echo e(Form::label('description', trans('common.description'), ['class' => 'col-sm-2 control-label'])); ?>

			    <div class="col-sm-10">
			     	<?php if($mode =="create"): ?>
			     		<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

			     	<?php else: ?>
			     	<?php echo e(Form::textarea('description', $announcement->description, ['class' => 'form-control'])); ?>

			     	<?php endif; ?>

			     	<?php if($errors->has('description')): ?>
					<span class="help-block">
						<strong><?php echo e($errors->first('description')); ?></strong>
					</span>
					<?php endif; ?>		     	
			    </div>
			</div>
			
			<div class="form-group required <?php echo e($errors->has('start_date') || $errors->has('end_date') ? ' has-error' : ''); ?>">
				<div class="row">
					<div class="col-md-6">
					 	<?php echo e(Form::label('start_date', trans('admin.start_date'), ['class' => 'col-sm-4 control-label'])); ?>


					 	<div class="input-group date datepicker col-sm-8">
                            <span class="input-group-addon addon-left calendar-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                            <?php if($mode=="create"): ?>
                            	<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
                            <?php else: ?>
                            	<input type="text" class="form-control" name="start_date" id="datepicker1" value="<?php echo e($announcement->start_date); ?>">
                            <?php endif; ?>                            
                            <span class="input-group-addon addon-right angle-addon">
                                <span class="fa fa-angle-down"></span>
                            </span>
                        </div>
                        <?php if($errors->has('start_date')): ?>
                        <span class="help-block">
                        	<strong><?php echo e($errors->first('start_date')); ?></strong>
                        </span>
                        <?php endif; ?>
					</div>
					<div class="col-md-6">
					 	<?php echo e(Form::label('end_date', trans('admin.end_date'), ['class' => 'col-sm-4 control-label'])); ?>

					 	<div class="input-group date datepicker col-sm-8">
                            <span class="input-group-addon addon-left calendar-addon">
                                <span class="fa fa-calendar"></span>
                            </span>                           
                            <?php if($mode=="create"): ?>
                            	<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
                            <?php else: ?>
                            	<input type="text" class="form-control" name="end_date" id="datepicker2" value="<?php echo e($announcement->end_date); ?>">
                            <?php endif; ?> 
                             <span class="input-group-addon addon-right angle-addon">
                                <span class="fa fa-angle-down"></span>
                            </span>
                        </div>
                        <?php if($errors->has('end_date')): ?>
                        <span class="help-block">
                        	<strong><?php echo e($errors->first('end_date')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
				</div>
			</div>
			
			<div class="form-group">
			    <div class="text-center">
			      <?php if($mode=="create"): ?>
			      	<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
			      <?php else: ?>
			      	<button type="submit" class="btn btn-success"><?php echo e(trans('common.save_changes')); ?></button>
			      <?php endif; ?>
			    </div>
			</div>
		</div><!-- /announcements -->
		</form>
	</div>
</div>












