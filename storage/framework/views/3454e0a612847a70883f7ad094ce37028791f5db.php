<!-- <div class="main-content"> -->
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="post-filters">
				<?php echo Theme::partial('pagemenu-settings',compact('timeline')); ?>

			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-default">
			<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<div class="panel-heading no-bg panel-settings">
					<h3 class="panel-title">
						<?php echo e(trans('common.privacy_settings')); ?>

					</h3>
				</div>
				<div class="panel-body nopadding">
					<div class="liusocial-form">						
						<form class="form-inline" action="<?php echo e(url('/'.$username.'/page-settings/privacy')); ?>" method="POST">
							<?php echo e(csrf_field()); ?>


							<div class="privacy-question">

								<ul class="list-group">
									<li href="#" class="list-group-item">
										<fieldset class="form-group">
											<?php echo e(Form::label('timeline_post_privacy', trans('common.label_page_timeline_post_privacy'), ['class' => 'control-label'])); ?>


											<?php echo e(Form::select('timeline_post_privacy', array('everyone' => trans('common.everyone'), 'only_admins' => trans('common.admins')), $page_details->timeline_post_privacy, array('class' => 'form-control col-sm-6'))); ?>

										</fieldset>
									</li>

									<li href="#" class="list-group-item">
										<fieldset class="form-group">
											<?php echo e(Form::label('member_privacy', trans('common.label_page_member_privacy'))); ?>

											<?php echo e(Form::select('member_privacy', array('members' => trans('common.members'), 'only_admins' => trans('common.admins')), $page_details->member_privacy, array('class' => 'form-control col-sm-6'))); ?>

										</fieldset>
									</li>
								</ul>
								<div class="pull-right">
									<?php echo e(Form::submit(trans('common.save_changes'), ['class' => 'btn btn-success'])); ?>

								</div>
								<div class="clearfix"></div>
							</div>	
						</form>

					</div><!-- /socialite-form -->
				</div>
			</div><!-- /panel -->
			
		</div>
	</div><!-- /row -->
</div>
<!-- </div> --><!-- /main-content -->