<!-- <div class="main-content"> -->
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="post-filters">
				<?php echo Theme::partial('pagemenu-settings',compact('timeline')); ?>

			</div>
		</div>
		<div class="col-md-8">						
			<div class="post-filters">
				<div class="panel panel-default">
				<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo e(trans('common.manage_roles')); ?></h3>
					</div>
					<div class="panel-body">						
						<div class="holder">
							<p><?php echo e(trans('messages.manage_roles_text')); ?></p>
						</div>
						<?php if(count($page_members) > 0): ?>
						<ul class="list-group page-likes">
							
							<?php foreach($page_members as $page_member): ?>
							<li class="list-group-item holder">
								<div class="connect-list">
									<div class="connect-link pull-left">
										<div class="follower">
											<a href="<?php echo e(url(url($page_member->username))); ?>">
												<img src="<?php echo e($page_member->avatar); ?>" alt="<?php echo e($page_member->name); ?>" class="img-icon img-30" title="<?php echo e($page_member->name); ?>">
												<?php echo e($page_member->name); ?>

											</a>
										</div>
									</div>
									<?php if($page->is_admin(Auth::user()->id)): ?>
									<div class="pull-right follow-links">
										<div class="row">	

											<form class="margin-right" method="POST" action="<?php echo e(url('/member/updatepage-role/')); ?>">
												<?php echo e(csrf_field()); ?>


												<div class="col-md-5 col-sm-5 col-xs-5 padding-5">
													<?php echo e(Form::select('member_role', $roles, $page_member->pivot->role_id , array('class' => 'form-control'))); ?>

												</div>

												<?php echo e(Form::hidden('user_id', $page_member->id)); ?>

												<?php echo e(Form::hidden('page_id', $page->id)); ?>


												<div class="col-md-3 col-sm-3 col-xs-3 padding-5">
													<?php echo e(Form::submit(trans('common.assign'), array('class' => 'btn btn-to-follow btn-default'))); ?>

												</div>

												<div class="col-md-4 col-sm-4 col-xs-4 padding-5">
													<a href="#" class="btn btn-to-follow btn-default remove-pagemember remove" data-user-id="<?php echo e($page_member->id); ?> - <?php echo e($page->id); ?>">
														<i class="fa fa-trash"></i> <?php echo e(trans('common.remove')); ?> 
													</a>
												</div>
											</form>	

											
										</div>
									</div>
									<?php endif; ?>
									<div class="clearfix"></div>
								</div>
							</li>
							<?php endforeach; ?>
						</ul>
						<?php else: ?>
						<div class="alert alert-warning">
							<?php echo e(trans('messages.no_members_to_admin')); ?>

						</div>
						<?php endif; ?>	
						
					</div>
				</div><!-- /panel -->
			</div>
		</div><!-- /col-md-8 -->
	</div>
	</div><!-- /container -->