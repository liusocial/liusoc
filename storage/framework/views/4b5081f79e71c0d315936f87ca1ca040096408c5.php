<div class="panel panel-default">
    <div class="panel-heading no-bg panel-settings">
        <h3 class="panel-title">
            <?php echo e(trans('admin.users_acc_req')); ?>

        </h3>
    </div>
    <div class="panel-body timeline">
        <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php if(count($users) > 0): ?>
            <div class="table-responsive manage-table">
                <table class="table existing-products-table liusocial">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th><?php echo e(trans('auth.st_id')); ?></th>
                        <th><?php echo e(trans('auth.name')); ?></th>
                        <th><?php echo e(trans('auth.major')); ?></th>

                        <th><?php echo e(trans('common.email')); ?></th>
                        <th><?php echo e(trans('admin.user_req_date')); ?></th>

                        <th><?php echo e(trans('admin.approve_req')); ?></th>
                        <th><?php echo e(trans('admin.reject_req')); ?></th>


                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($users as $user): ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td><?php echo e($user->st_id); ?></td>
                            <td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>                            <td><?php echo e($user->school->name); ?></td>

                            <td><?php echo e($user->email); ?></td>
                            <td><?php echo e($user->created_at); ?></td>
                            <td>

                                <a href="<?php echo e(url('admin/requests/'.$user->id.'/approve')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class=" btn btn  btn-info btn-lg" style=""><i class="glyphicon glyphicon-ok" aria-hidden="true">Accept</i></span></a>
                            </td>
                            <td>
                                   <a href="<?php echo e(url('admin/requests/'.$user->id.'/reject')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="btn btn btn-danger btn-lg"><i class="glyphicon glyphicon-remove" aria-hidden="true">Reject</i></span></a>



                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="pagination-holder">
                <?php echo e($users->render()); ?>

            </div>
        <?php else: ?>
            <div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
        <?php endif; ?>
    </div>
</div>
