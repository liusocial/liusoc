<div class="login-block">
    <div class="panel panel-default">
        <div class="panel-body nopadding">
            <div class="login-head">
                <?php echo e(trans('auth.reset_welcome_heading')); ?>

                <div class="header-circle"><i class="fa fa-paper-plane" aria-hidden="true"></i></div>
                <div class="header-circle login-progress hidden"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></div>
            </div>
            <div class="login-bottom">
                 <?php if(session('status')): ?>
                        <div class="alert alert-success">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
                  <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/password/email')); ?>">
                        <?php echo e(csrf_field()); ?>


                    <fieldset class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                        <?php echo e(Form::label('email',trans('auth.email_address'))); ?>

                        <?php echo e(Form::text('email', NULL, ['class' => 'form-control', 'id' => 'email', 'placeholder'=> trans('auth.email_address')])); ?>

                    </fieldset>
                    <?php echo e(Form::button( 'Send Password Reset Link' , ['type' => 'submit','class' => 'btn btn-success btn-submit'])); ?>

                </form>
            </div>  
           
        </div>
    </div>
    <div class="problem-login">
        <div class="pull-left"><?php echo e(trans('auth.dont_have_an_account_yet')); ?><a href="<?php echo e(url('/register')); ?>"> <?php echo e(trans('auth.get_started')); ?></a></div>
        <div class="pull-right"><a href="<?php echo e(url('/login')); ?>"><?php echo e(trans('auth.login')); ?></a></div>
        <div class="clearfix"></div>
    </div>
</div><!-- /login-block -->
