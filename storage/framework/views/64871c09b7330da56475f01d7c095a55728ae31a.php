<div class="panel panel-default">
	<div class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			<?php echo e(trans('common.manage_users')); ?>

			<?php if(Auth::user()->language =='ar'): ?>
				<span class="pull-left">
				<a href="<?php echo e(url('dean/users/add/create')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					<?php else: ?>
				<span class="pull-right">
				<a href="<?php echo e(url('dean/users/add/create')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
							<?php endif; ?>

		</h3>
	</div>
	<br>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#students"><?php echo e(trans('common.students')); ?></a></li>
		<li><a data-toggle="tab" href="#inst"><?php echo e(trans('common.instructors')); ?></a></li>
		<?php /*<li><a data-toggle="tab" href="#std">Student Center</a></li>*/ ?>
	</ul>
	<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="tab-content clearfix">

		<div id="students" class="tab-pane fade in active">
			<?php if(count($users) > 0): ?>
			<div class="table-responsive manage-table">
				<table class="table existing-products-table liusocial">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th><?php echo e(trans('auth.st_id')); ?></th>
							<th><?php echo e(trans('auth.username')); ?></th>
							<th><?php echo e(trans('auth.name')); ?></th>
							<th><?php echo e(trans('common.user_type')); ?></th>
							<th><?php echo e(trans('common.email')); ?></th>
							<th><?php echo e(trans('common.status')); ?></th>
							<th><?php echo e(trans('admin.options')); ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach($users as $user): ?>
							<?php foreach($user->roles as $role): ?>
								<?php if($role->name =='student'): ?>
						<tr>
							<td>&nbsp;</td>	
							<td><?php echo e($user->st_id); ?></td>
							<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>
							<?php foreach($user->roles as $role): ?>
								<td><?php echo e($user->timeline->name); ?></td>
								<td><?php echo e($role->name); ?></td>
							<?php endforeach; ?>
							<td><?php echo e($user->email); ?></td>
							<td>
								<?php if($user->active == "1"): ?>
									<a href="<?php echo e(url('dean/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
								<?php else: ?>
									<a href="<?php echo e(url('dean/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
								<?php endif; ?>
							</td>
							<td>
								<ul class="list-inline">
									<li><a href="<?php echo e(url('dean/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
									<?php /*<li><a href="<?php echo e(url('dean/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
								</ul>

							</td>
							<td>&nbsp;</td> 
						</tr>
						<?php endif; ?>
								<?php endforeach; ?>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<div class="pagination-holder">
					<?php echo e($users->render()); ?>

				</div>	
			<?php else: ?>
				<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
			<?php endif; ?>
		</div><?php /*end students*/ ?>



	<div id="inst" class="tab-pane fade">
		<?php if(count($users) > 0): ?>
			<div class="table-responsive manage-table">
				<table class="table existing-products-table liusocial">
					<thead>
					<tr>
						<th>&nbsp;</th>
						<th><?php echo e(trans('admin.id')); ?></th>
						<th><?php echo e(trans('auth.username')); ?></th>
						<th><?php echo e(trans('auth.name')); ?></th>
						<th><?php echo e(trans('common.user_type')); ?></th>
						<th><?php echo e(trans('common.email')); ?></th>
						<th><?php echo e(trans('common.status')); ?></th>
						<th><?php echo e(trans('admin.options')); ?></th>
						<th>&nbsp;</th>
					</tr>
					</thead>
					<tbody>

					<?php foreach($users as $user): ?>
						<?php foreach($user->roles as $role): ?>
							<?php if($role->name =='instructor'): ?>
						<tr>
							<td>&nbsp;</td>
							<td><?php echo e($user->id); ?></td>
							<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

								<td><?php echo e($user->timeline->name); ?></td>
								<td><?php echo e($role->name); ?></td>

							<td><?php echo e($user->email); ?></td>
							<td>
								<?php if($user->active == "1"): ?>
									<a href="<?php echo e(url('dean/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
								<?php else: ?>
									<a href="<?php echo e(url('dean/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
								<?php endif; ?>
							</td>
							<td>
								<ul class="list-inline">
									<li><a href="<?php echo e(url('dean/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
									<?php /*<li><a href="<?php echo e(url('dean/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
								</ul>

							</td>
							<td>&nbsp;</td>
						</tr>
						<?php endif; ?>
							<?php endforeach; ?>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<div class="pagination-holder">
				<?php echo e($users->render()); ?>

			</div>
		<?php else: ?>
			<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
		<?php endif; ?>
	</div>
	</div>
	</div>
