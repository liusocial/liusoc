
<div class="panel panel-default">
    <div class="panel-body">
        <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="panel-heading no-bg panel-settings">
            <h3 class="panel-title">
                <?php echo e(trans('common.add_admin')); ?>

            </h3>
        </div>
        <?php if($mode =="create_admin"): ?>
            <form method="POST" action="<?php echo e(url('admin/users/add-admin')); ?>" class="socialite-form">
                <?php echo e(csrf_field()); ?>


                <fieldset class="form-group required <?php echo e($errors->has('username') ? ' has-error' : ''); ?>">
                    <?php echo e(Form::label('username', trans('common.username'), ['class' => 'control-label'])); ?>


                    <input type="text" class="form-control content-form" placeholder="<?php echo e(trans('common.username')); ?>" name="username" required>
                    <?php /*<small class="text-muted"><?php echo e(trans('admin.user_username_text')); ?></small>*/ ?>
                    <?php if($errors->has('username')): ?>
                        <span class="help-block">
					<strong><?php echo e($errors->first('username')); ?></strong>
				</span>
                    <?php endif; ?>
                </fieldset>

                <fieldset class="form-group required <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                    <?php echo e(Form::label('email', trans('auth.email_address'), ['class' => 'control-label'])); ?>

                    <input type="email" class="form-control" name="email" placeholder="<?php echo e(trans('common.email')); ?>" required>
                    <?php /*<small class="text-muted"><?php echo e(trans('admin.user_email_text')); ?></small>*/ ?>
                    <?php if($errors->has('email')): ?>
                        <span class="help-block">
					<strong><?php echo e($errors->first('email')); ?></strong>
				</span>
                    <?php endif; ?>
                </fieldset>

                <fieldset class="form-group required <?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                    <?php echo e(Form::label('password', trans('common.password'), ['class' => 'control-label'])); ?>

                    <input type="password" class="form-control" name="password" placeholder="<?php echo e(trans('common.new_password')); ?>"required>
                    <?php /*<small class="text-muted"><?php echo e(trans('common.new_password_text')); ?></small>*/ ?>
                    <?php if($errors->has('password')): ?>
                        <span class="help-block">
					<strong><?php echo e($errors->first('password')); ?></strong>
				</span>
                    <?php endif; ?>
                </fieldset>
                <fieldset class="form-group required <?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                    <?php echo e(Form::label('name', trans('auth.name'), ['class' => 'control-label'])); ?>

                    <input type="text" class="form-control require-if-active" name="name"  placeholder="Name" required>
                    <?php /*<small class="text-muted"><?php echo e(trans('admin.user_name_text')); ?></small>*/ ?>
                    <?php if($errors->has('name')): ?>
                        <span class="help-block">
					<strong><?php echo e($errors->first('name')); ?></strong>
				</span>
                    <?php endif; ?>
                </fieldset>

                <fieldset class="form-group">
                    <?php echo e(Form::label('gender', trans('common.gender'), ['class' => 'control-label'])); ?>

                    <?php echo e(Form::select('gender', array('male' => trans('common.male'),'female' => trans('common.female'),'other' => trans('common.other')) , null, ['class' => 'form-control'])); ?>

                    <?php /*<small class="text-muted"><?php echo e(trans('admin.user_gender_text')); ?></small>*/ ?>
                </fieldset>

                <fieldset class="form-group">

                    <?php echo e(Form::hidden('school_id', Auth::user()->school_id)); ?>

                    <?php /*<small class="text-muted"><?php echo e(trans('admin.user_gender_text')); ?></small>*/ ?>
                </fieldset>

                <fieldset class="form-group">

                    <?php echo e(Form::hidden('type', 1)); ?>

                    <?php /*<small class="text-muted"><?php echo e(trans('admin.user_gender_text')); ?></small>*/ ?>
                </fieldset>

                <?php if(Setting::get('birthday') == "on"): ?>
                    <fieldset class="form-group">
                        <?php echo e(Form::label('birthday', trans('common.birthday'), ['class' => 'control-label'])); ?>

                        <input class="datepicker form-control hasDatepicker" size="16" id="datepick2" name="birthday" type="text" value="" data-date-format="yyyy-mm-dd">
                    </fieldset>
                <?php endif; ?>
                <?php /*<fieldset class="form-group">*/ ?>
                <?php /*<?php echo e(Form::label('School_id', trans('admin.school'), ['class' => 'col-sm-2 control-label'])); ?>*/ ?>


                <?php /*<i class="fa fa-user">@$school->name</i>*/ ?><?php /**/ ?><?php /*<h4>(<?php echo e($school->name); ?>)</h4>*/ ?>
                <?php /*<?php echo e(Form::hidden('school_id', Auth::user()->school_id)); ?>*/ ?>
                <?php /*<?php echo e(Form::text('school_id',Auth::user()->school_id, ['class' => 'form-control', 'readonly' => 'true'])); ?>*/ ?>
                <?php /*<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>*/ ?>
                <?php /*<?php echo e(Form::number('user_id',null,['class' => 'form-control'])); ?>*/ ?>
                <?php /*<?php else: ?>*/ ?>
                <?php /*<?php echo e(Form::text('title', $announcement->user_id, ['class' => 'form-control'])); ?>*/ ?>
                <?php /*<?php endif; ?>*/ ?>
                <?php /*</div>*/ ?>

                <?php /*</fieldset>*/ ?>



                <?php /*<fieldset class="form-group">*/ ?>
                <?php /*<?php echo e(Form::label('about', trans('common.about'), ['class' => 'control-label'])); ?>*/ ?>
                <?php /*<textarea class="form-control about-form" name="about" rows="3" value="" placeholder="<?php echo e(trans('common.about')); ?>"></textarea>*/ ?>
                <?php /*<small class="text-muted"><?php echo e(trans('admin.user_about_text')); ?></small>*/ ?>
                <?php /*</fieldset>*/ ?>
                <?php if(Setting::get('city') == "on"): ?>
                    <fieldset class="form-group">
                        <?php echo e(Form::label('city', trans('common.current_city'), ['class' => 'control-label'])); ?>

                        <input type="text" class="form-control" name="city" value="" placeholder="<?php echo e(trans('common.current_city')); ?>">
                        <small class="text-muted"><?php echo e(trans('admin.user_city_text')); ?></small>
                    </fieldset>
                <?php endif; ?>

                <div class="pull-right">
                    <button type="submit" class="btn btn-primary btn-sm"><?php echo e(trans('common.add')); ?></button>
                </div>
            </form>

    </div>
</div>
<?php endif; ?>
