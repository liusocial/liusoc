<div class="panel panel-default">
	<div class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
            <?php
            $school= \App\School::find(Auth::user()->school_id);

            ?>

			<?php echo e(trans('admin.last_active_announcement')); ?> for college <?php echo e($school->name); ?>

		</h3>
	</div>
	<div class="panel-body">

		<?php if($current_anouncements != NULL): ?>
			<?php foreach($current_anouncements as $current_anouncement): ?>
				<div class="clearfix"></div>
				<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) <= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days =date('d-m-Y', strtotime(\Carbon\Carbon::now())) - date('d-m-Y', strtotime($current_anouncement->end_date))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

					<div class="clearfix"></div>
					<div class="announcement-description pull-left">
						<?php echo e($current_anouncement->description); ?>

						<div class="time-created">
                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
							<?php echo '<br> Created on '.$announces_date; ?>

						</div>
					</div>
					<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
		<?php endif; ?>
	</div>

</div>

<div class="panel panel-default">
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			<?php echo e(trans('admin.announcements')); ?>

			<span class="pull-right">
				<a href="<?php echo e(url('dean/announcements/create')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
		</h3>
	</div>
	<div class="panel-body">
		<div class="announcement-container table-responsive">	
			<table class="table announcements-table">
				<thead>
			    	<th><?php echo e(trans('admin.title')); ?></th>
			        <th><?php echo e(trans('common.description')); ?></th>	 
			        <th><?php echo e(trans('admin.start_date')); ?></th>
			        <th><?php echo e(trans('admin.end_date')); ?></th>
			        <th><?php echo e(trans('common.status')); ?></th>
			        <th><?php echo e(trans('admin.action')); ?></th>
		    	</thead>
			    <tbody>

				<?php /*<?php if($current_anouncement->user_id == Auth::user()->id): ?>*/ ?>
			     <?php foreach($announcements as $announcement): ?>

			    	<tr>
			        	<td><?php echo e($announcement->title); ?></td>
			            <td> 
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

			            		<?php /* <div class="time">			 */ ?>
			            			<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
			            			<?php /* <?php echo e($announce_date); ?>

			            		</div> */ ?>
			            		<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div> 
			            	</span>
						</td> 
						<td>
							<?php echo e($announcement->start_date); ?>

						</td>
						<td>
							<?php echo e($announcement->end_date); ?>

						</td>
						<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
						<?php /*<td>*/ ?>
							<?php /*<?php if($status == "Active"): ?>*/ ?>
							<?php /*<a href="<?php echo e(url('dean/activate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" ><?php echo e($status); ?></a>*/ ?>
							<?php /*<?php else: ?>*/ ?>
							<?php /*<a href="<?php echo e(url('dean/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" ><?php echo e($status); ?></a>*/ ?>
							<?php /*<?php endif; ?>*/ ?>
						<?php /*</td>*/ ?>
						<td>
							<?php if($announcement->value == "1"): ?>
								<a href="<?php echo e(url('dean/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
							<?php else: ?>
								<a href="<?php echo e(url('dean/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactive</a>
							<?php endif; ?>
						</td>
						<td>
							<ul class="list-inline">	
								<li><a href="<?php echo e(url('dean/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
								<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
							</ul>
						</td>
			        </tr>
			        <?php endforeach; ?>
				<?php /*<?php endif; ?>*/ ?>
			    </tbody>
			</table>
			<div class="pagination-holder">
				<?php echo e($announcements->render()); ?>

			</div>	
		</div>
	</div>
</div>
<?php echo Theme::asset()->container('footer')->usePath()->add('admin', 'js/admin.js'); ?>

