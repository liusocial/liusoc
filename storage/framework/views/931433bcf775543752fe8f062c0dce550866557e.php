<?php /*<style>*/ ?>
	<?php /*.content-posts {*/ ?>
		<?php /*margin-left: 240px;*/ ?>
		<?php /*margin-top: 65px;*/ ?>
	<?php /*}*/ ?>
	<?php /*.container-posts {*/ ?>
		<?php /*padding: 0 130px;*/ ?>
	<?php /*}*/ ?>
	<?php /*.primary-content .container-posts {*/ ?>
		<?php /*padding: 0 40px;*/ ?>
	<?php /*}*/ ?>
	<?php /*.primary-content { width:70%;*/ ?>
		<?php /*float: left; }*/ ?>
	<?php /*@media  screen and (max-width:1199px)  {   header { padding: 15px 30px 10px 220px;  }*/ ?>

		<?php /*.sidebar-nav {  width:190px;  }*/ ?>

		<?php /*.content-posts {  margin-left:190px;}*/ ?>

		<?php /*.container-posts { padding:0px 30px;  }*/ ?>
	<?php /*}*/ ?>
	<?php /*.content-posts  {  margin:0px;*/ ?>
		<?php /*margin-top:120px;}*/ ?>

	<?php /*.container-posts, .primary-content .container-posts { padding:0px 15px;  }*/ ?>
<?php /*</style>*/ ?>



<div class="panel panel-default">
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#admin">Admin Announcements</a></li>
			<li><a data-toggle="tab" href="#business">Business College</a></li>
			<li><a data-toggle="tab" href="#art">Art College</a></li>
			<li><a data-toggle="tab" href="#it">IT College</a></li>
			<li><a data-toggle="tab" href="#med">Medicine College</a></li>
			<li><a data-toggle="tab" href="#eng">Engineering College</a></li>
			<li><a data-toggle="tab" href="#english">English College</a></li>
			<li><a data-toggle="tab" href="#std">Student Center</a></li>
		</ul>
		<div class="tab-content">
			<div id="admin" class="tab-pane fade in active">

				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements != NULL): ?>
							<?php foreach($current_anouncements as $current_anouncement): ?>
								<div class="clearfix"></div>
							<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
						<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
							<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

								<div class="clearfix"></div>
								<div class="announcement-description pull-left">
									<?php echo e($current_anouncement->description); ?>

									<div class="time-created">
                                        <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
										<?php echo '<br> Created on '.$announces_date; ?>

									</div>
								</div>
								<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
							</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>
					</div>
				</div>



	<div class="panel-heading no-bg panel-settings">

			<br>
			<?php /*<div class="content-posts profile-content">*/ ?>
				<?php /*<div class="banner-profile">*/ ?>
				<?php /*</div>*/ ?>
				<?php /*<!-- Tab Panel -->*/ ?>
				<?php /*<ul class="nav nav-tabs" role="tablist">*/ ?>
					<?php /*<li class="active"><a href="#admin" role="tab" id="adminTab" data-toggle="tab" aria-controls="admin" aria-expanded="true">Admin Announcements</a></li>*/ ?>
					<?php /*<li><a href="#business" role="tab" id="businessTab" data-toggle="tab" aria-controls="business" aria-expanded="true">Bussiness College Announcements</a></li>*/ ?>
					<?php /*<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Art College Announcements</a></li>*/ ?>
					<?php /*<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">IT College Announcements</a></li>*/ ?>
					<?php /*<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Midical College Announcements</a></li>*/ ?>
					<?php /*<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Engnniring College Announcements</a></li>*/ ?>
					<?php /*<li><a href="#chat" role="tab" id="chatTab" data-toggle="tab" aria-controls="chat" aria-expanded="true">Student Center Announcements</a></li>*/ ?>
				<?php /*</ul>*/ ?>
				<?php /*<div class="tab-content">*/ ?>
					<?php /*<!-- Tab admin -->*/ ?>
				<?php /*<div class="tab-pane fade active in" role="tabpanel" id="admin" aria-labelledby="adminTab">*/ ?>
					<?php /*<div id="posts-container" class="container-fluid container-posts">*/ ?>

			<h3 class="panel-title">
							<?php echo e(trans('admin.announcements')); ?>


				<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
		</h3>
	</div>
	<div class="panel-body">
		<div class="announcement-container table-responsive">	
			<table class="table announcements-table">
				<thead>
			    	<th><?php echo e(trans('admin.title')); ?></th>
			        <th><?php echo e(trans('common.description')); ?></th>	 
			        <th><?php echo e(trans('admin.start_date')); ?></th>
			        <th><?php echo e(trans('admin.end_date')); ?></th>
			        <th><?php echo e(trans('common.status')); ?></th>
			        <th><?php echo e(trans('admin.action')); ?></th>
		    	</thead>
			    <tbody>
			     <?php foreach($announcements as $announcement): ?>
			    	<tr>
			        	<td><?php echo e($announcement->title); ?></td>
			            <td> 
			            	<span class="description">
			            		<?php echo e($announcement->description); ?> 
			            		<?php /* <div class="time">			 */ ?>
			            			<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
			            			<?php /* <?php echo e($announce_date); ?>

			            		</div> */ ?>
			            		<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div> 
			            	</span>
						</td> 
						<td>
							<?php echo e($announcement->start_date); ?>

						</td>
						<td>
							<?php echo e($announcement->end_date); ?>

						</td>
						<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
						<td>
							<?php if($announcement->value == "1"): ?>
							<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
							<?php else: ?>
							<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
							<?php endif; ?>
						</td>
						<td>
							<ul class="list-inline">	
								<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
								<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
							</ul>
						</td>
			        </tr>			        
			        <?php endforeach; ?>
			    </tbody>
			</table>
			<div class="pagination-holder">
				<?php echo e($announcements->render()); ?>

			</div>	
		</div>
	</div>

</div><!-- end Tab admin -->

			<div id="business" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements_bu != NULL): ?>
							<?php foreach($current_anouncements_bu as $current_anouncement): ?>
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										<?php echo e($current_anouncement->description); ?>

										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											<?php echo '<br> Created on '.$announces_date; ?>

										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
				<!-- Tab business -->
				<?php /*<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">*/ ?>
					<?php /*<div class="container-fluid container-posts">*/ ?>

						<h3 class="panel-title">
							<?php echo e(trans('admin.business_announcements')); ?>


							<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create_business')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
						</h3>
				</div>
					<div class="panel-body">
						<div class="announcement-container table-responsive">
							<table class="table announcements-table">
								<thead>
								<th><?php echo e(trans('admin.title')); ?></th>
								<th><?php echo e(trans('common.description')); ?></th>
								<th><?php echo e(trans('admin.start_date')); ?></th>
								<th><?php echo e(trans('admin.end_date')); ?></th>
								<th><?php echo e(trans('common.status')); ?></th>
								<th><?php echo e(trans('admin.action')); ?></th>
								</thead>
								<tbody>
								<?php foreach($bu_announcements as $announcement): ?>
									<tr>
										<td><?php echo e($announcement->title); ?></td>
										<td>
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

								<?php /* <div class="time">			 */ ?>
								<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
								<?php /* <?php echo e($announce_date); ?>

                            </div> */ ?>
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div>
			            	</span>
										</td>
										<td>
											<?php echo e($announcement->start_date); ?>

										</td>
										<td>
											<?php echo e($announcement->end_date); ?>

										</td>
										<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
										<td>
											<?php if($announcement->value == "1"): ?>
												<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
											<?php else: ?>
												<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
											<?php endif; ?>
										</td>
										<td>
											<ul class="list-inline">
												<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
												<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
											</ul>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
							<div class="pagination-holder">
								<?php echo e($announcements->render()); ?>

							</div>
						</div>
					</div>

			</div><!-- end Tab bussiness -->

			<div id="art" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements_art != NULL): ?>
							<?php foreach($current_anouncements_art as $current_anouncement): ?>
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										<?php echo e($current_anouncement->description); ?>

										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											<?php echo '<br> Created on '.$announces_date; ?>

										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					<?php /*<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">*/ ?>
					<?php /*<div class="container-fluid container-posts">*/ ?>

					<h3 class="panel-title">
						<?php echo e(trans('admin.art_announcements')); ?>


						<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create_art')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th><?php echo e(trans('admin.title')); ?></th>
							<th><?php echo e(trans('common.description')); ?></th>
							<th><?php echo e(trans('admin.start_date')); ?></th>
							<th><?php echo e(trans('admin.end_date')); ?></th>
							<th><?php echo e(trans('common.status')); ?></th>
							<th><?php echo e(trans('admin.action')); ?></th>
							</thead>
							<tbody>
							<?php foreach($art_announcements as $announcement): ?>
								<tr>
									<td><?php echo e($announcement->title); ?></td>
									<td>
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

								<?php /* <div class="time">			 */ ?>
								<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
								<?php /* <?php echo e($announce_date); ?>

                            </div> */ ?>
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										<?php echo e($announcement->start_date); ?>

									</td>
									<td>
										<?php echo e($announcement->end_date); ?>

									</td>
									<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
									<td>
										<?php if($announcement->value == "1"): ?>
											<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
										<?php else: ?>
											<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
										<?php endif; ?>
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						<div class="pagination-holder">
							<?php echo e($announcements->render()); ?>

						</div>
					</div>
				</div>

			</div><!-- end Tab art -->

			<div id="it" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements_it != NULL): ?>
							<?php foreach($current_anouncements_it as $current_anouncement): ?>
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										<?php echo e($current_anouncement->description); ?>

										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											<?php echo '<br> Created on '.$announces_date; ?>

										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					<?php /*<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">*/ ?>
					<?php /*<div class="container-fluid container-posts">*/ ?>

					<h3 class="panel-title">
						<?php echo e(trans('admin.it_announcements')); ?>


						<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create_it')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th><?php echo e(trans('admin.title')); ?></th>
							<th><?php echo e(trans('common.description')); ?></th>
							<th><?php echo e(trans('admin.start_date')); ?></th>
							<th><?php echo e(trans('admin.end_date')); ?></th>
							<th><?php echo e(trans('common.status')); ?></th>
							<th><?php echo e(trans('admin.action')); ?></th>
							</thead>
							<tbody>
							<?php foreach($it_announcements as $announcement): ?>
								<tr>
									<td><?php echo e($announcement->title); ?></td>
									<td>
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

								<?php /* <div class="time">			 */ ?>
								<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
								<?php /* <?php echo e($announce_date); ?>

                            </div> */ ?>
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										<?php echo e($announcement->start_date); ?>

									</td>
									<td>
										<?php echo e($announcement->end_date); ?>

									</td>
									<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
									<td>
										<?php if($announcement->value == "1"): ?>
											<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
										<?php else: ?>
											<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
										<?php endif; ?>
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						<div class="pagination-holder">
							<?php echo e($announcements->render()); ?>

						</div>
					</div>
				</div>

			</div><!-- end Tab it -->

			<div id="med" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements_med != NULL): ?>
							<?php foreach($current_anouncements_med as $current_anouncement): ?>
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										<?php echo e($current_anouncement->description); ?>

										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											<?php echo '<br> Created on '.$announces_date; ?>

										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					<?php /*<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">*/ ?>
					<?php /*<div class="container-fluid container-posts">*/ ?>

					<h3 class="panel-title">
						<?php echo e(trans('admin.med_announcements')); ?>


						<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create_medicine')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th><?php echo e(trans('admin.title')); ?></th>
							<th><?php echo e(trans('common.description')); ?></th>
							<th><?php echo e(trans('admin.start_date')); ?></th>
							<th><?php echo e(trans('admin.end_date')); ?></th>
							<th><?php echo e(trans('common.status')); ?></th>
							<th><?php echo e(trans('admin.action')); ?></th>
							</thead>
							<tbody>
							<?php foreach($med_announcements as $announcement): ?>
								<tr>
									<td><?php echo e($announcement->title); ?></td>
									<td>
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

								<?php /* <div class="time">			 */ ?>
								<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
								<?php /* <?php echo e($announce_date); ?>

                            </div> */ ?>
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										<?php echo e($announcement->start_date); ?>

									</td>
									<td>
										<?php echo e($announcement->end_date); ?>

									</td>
									<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
									<td>
										<?php if($announcement->value == "1"): ?>
											<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
										<?php else: ?>
											<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
										<?php endif; ?>
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						<div class="pagination-holder">
							<?php echo e($announcements->render()); ?>

						</div>
					</div>
				</div>

			</div><!-- end Tab med -->

			<div id="eng" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements_eng != NULL): ?>
							<?php foreach($current_anouncements_eng as $current_anouncement): ?>
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										<?php echo e($current_anouncement->description); ?>

										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											<?php echo '<br> Created on '.$announces_date; ?>

										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					<?php /*<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">*/ ?>
					<?php /*<div class="container-fluid container-posts">*/ ?>

					<h3 class="panel-title">
						<?php echo e(trans('admin.eng_announcements')); ?>


						<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create_eng')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th><?php echo e(trans('admin.title')); ?></th>
							<th><?php echo e(trans('common.description')); ?></th>
							<th><?php echo e(trans('admin.start_date')); ?></th>
							<th><?php echo e(trans('admin.end_date')); ?></th>
							<th><?php echo e(trans('common.status')); ?></th>
							<th><?php echo e(trans('admin.action')); ?></th>
							</thead>
							<tbody>
							<?php foreach($eng_announcements as $announcement): ?>
								<tr>
									<td><?php echo e($announcement->title); ?></td>
									<td>
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

								<?php /* <div class="time">			 */ ?>
								<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
								<?php /* <?php echo e($announce_date); ?>

                            </div> */ ?>
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										<?php echo e($announcement->start_date); ?>

									</td>
									<td>
										<?php echo e($announcement->end_date); ?>

									</td>
									<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
									<td>
										<?php if($announcement->value == "1"): ?>
											<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
										<?php else: ?>
											<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
										<?php endif; ?>
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						<div class="pagination-holder">
							<?php echo e($announcements->render()); ?>

						</div>
					</div>
				</div>

			</div><!-- end Tab eng -->

			<div id="english" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements_english != NULL): ?>
							<?php foreach($current_anouncements_english as $current_anouncement): ?>
								<div class="clearfix"></div>
								<div class="announcement-container">

			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

</div>


				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>

									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										<?php echo e($current_anouncement->description); ?>

										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											<?php echo '<br> Created on '.$announces_date; ?>

										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>
					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					<?php /*<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">*/ ?>
					<?php /*<div class="container-fluid container-posts">*/ ?>

					<h3 class="panel-title">
						<?php echo e(trans('admin.english_announcements')); ?>


						<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create_english')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th><?php echo e(trans('admin.title')); ?></th>
							<th><?php echo e(trans('common.description')); ?></th>
							<th><?php echo e(trans('admin.start_date')); ?></th>
							<th><?php echo e(trans('admin.end_date')); ?></th>
							<th><?php echo e(trans('common.status')); ?></th>
							<th><?php echo e(trans('admin.action')); ?></th>
							</thead>
							<tbody>
							<?php foreach($english_announcements as $announcement): ?>
								<tr>
									<td><?php echo e($announcement->title); ?></td>
									<td>
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

								<?php /* <div class="time">			 */ ?>
								<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
								<?php /* <?php echo e($announce_date); ?>

                            </div> */ ?>
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										<?php echo e($announcement->start_date); ?>

									</td>
									<td>
										<?php echo e($announcement->end_date); ?>

									</td>
									<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
									<td>
										<?php if($announcement->value == "1"): ?>
											<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
										<?php else: ?>
											<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
										<?php endif; ?>
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						<div class="pagination-holder">
							<?php echo e($announcements->render()); ?>

						</div>
					</div>
				</div>

			</div><!-- end Tab english -->

			<div id="std" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading no-bg panel-settings">
						<h3 class="panel-title">
							<?php echo e(trans('admin.active_announcement')); ?>

						</h3>
					</div>
					<div class="panel-body">

						<?php if($current_anouncements_std != null): ?>
							<?php foreach($current_anouncements_std as $current_anouncement): ?>
								<div class="clearfix"></div>
								<div class="announcement-container">
			<span class="announcement-title">
				<div class="announcement-title">
				<?php echo e($current_anouncement->title); ?>

							</div>
				<span class="pull-right label label-default expiry-date">
					<?php if(date('d-m-Y', strtotime($current_anouncement->end_date)) >= date('d-m-Y', strtotime(\Carbon\Carbon::now()))): ?>
						<?php echo e($total_days = date('d-m-Y', strtotime($current_anouncement->end_date)) - date('d-m-Y', strtotime(\Carbon\Carbon::now()))); ?>

					<?php endif; ?>
					<?php if($total_days != 0): ?>
						<?php /*<?php echo e($total_days); ?>*/ ?>
						<?php echo e(trans('admin.days_to_expire')); ?>

					<?php else: ?>
						<?php echo e(trans('admin.expired')); ?>


					<?php endif; ?>
				</span>
					</span>
									<div class="clearfix"></div>
									<div class="announcement-description pull-left">
										<?php echo e($current_anouncement->description); ?>

										<div class="time-created">
                                            <?php $announces_date = date("F d Y, G:i A", strtotime($current_anouncement->created_at));?>
											<?php echo '<br> Created on '.$announces_date; ?>

										</div>
									</div>
									<span class="pull-right announcement-actions">
				<a href="#" class="view-by"><i class="fa fa-eye"></i> Views : <?php echo e(count($current_anouncement->users)); ?></a>
				<a href="<?php echo e(url('admin/announcements/'.$current_anouncement->id.'/edit')); ?>"><?php echo e(trans('common.edit')); ?></a>
			</span>
								</div>

							<?php endforeach; ?>

								<?php else: ?>
							<div class="alert alert-warning "><?php echo e(trans('messages.no_announcements')); ?></div>
						<?php endif; ?>


					</div>
				</div>

				<div class="panel-heading no-bg panel-settings">
					<!-- Tab business -->
					<?php /*<div class="tab-pane fade active in" role="tabpanel" id="business" aria-labelledby="businessTab">*/ ?>
					<?php /*<div class="container-fluid container-posts">*/ ?>

					<h3 class="panel-title">
						<?php echo e(trans('admin.student_announcements')); ?>


						<span class="pull-right">
				<a href="<?php echo e(url('admin/announcements/create_std')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</h3>
				</div>
				<div class="panel-body">
					<div class="announcement-container table-responsive">
						<table class="table announcements-table">
							<thead>
							<th><?php echo e(trans('admin.title')); ?></th>
							<th><?php echo e(trans('common.description')); ?></th>
							<th><?php echo e(trans('admin.start_date')); ?></th>
							<th><?php echo e(trans('admin.end_date')); ?></th>
							<th><?php echo e(trans('common.status')); ?></th>
							<th><?php echo e(trans('admin.action')); ?></th>
							</thead>
							<tbody>
							<?php foreach($std_announcements as $announcement): ?>
								<tr>
									<td><?php echo e($announcement->title); ?></td>
									<td>
			            	<span class="description">
			            		<?php echo e($announcement->description); ?>

								<?php /* <div class="time">			 */ ?>
								<?php /**/ $announce_date = date("F d Y, G:i A", strtotime($announcement->created_at)) /**/ ?>
								<?php /* <?php echo e($announce_date); ?>

                            </div> */ ?>
								<div class="time">
			            			<span class="help-text"><i class="fa fa-eye"></i> <?php echo e(count($announcement->users)); ?>

			            			</span>
			            		</div>
			            	</span>
									</td>
									<td>
										<?php echo e($announcement->start_date); ?>

									</td>
									<td>
										<?php echo e($announcement->end_date); ?>

									</td>
									<?php /**/ $status = $announcement->id == Setting::get('announcement') ? trans('admin.active') : trans('admin.inactive') /**/ ?>
									<td>
										<?php if($announcement->value == "1"): ?>
											<a href="<?php echo e(url('admin/inactivate/'.$announcement->id)); ?>" class="btn btn-success announcement-status" >Active</a>
										<?php else: ?>
											<a href="<?php echo e(url('admin/activate/'.$announcement->id)); ?>" class="btn btn-default announcement-status" >Inactivate</a>
										<?php endif; ?>
									</td>
									<td>
										<ul class="list-inline">
											<li><a href="<?php echo e(url('admin/announcements/'.$announcement->id.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
											<li><a href="#" data-announcement-id="<?php echo e($announcement->id); ?>" class="announce-delete"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
										</ul>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						<div class="pagination-holder">
							<?php echo e($announcements->render()); ?>

						</div>
					</div>
				</div>

			</div><!-- end Tab student center -->

			</div><!-- Close Tab Content-->

	</div><!--Close content posts-->


<?php echo Theme::asset()->container('footer')->usePath()->add('admin', 'js/admin.js'); ?>

