<style>

	/* change border radius for the tab , apply corners on top*/

	#exTab3 .nav-pills > li > a {
		border-radius: 4px 4px 0 0 ;
	}

	#exTab3 .tab-content {
		color : white;
		background-color: #428bca;
		padding : 5px 15px;
	}

</style>
<div class="panel panel-default">
	<div id="exTab3" class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			<?php echo e(trans('common.manage_users')); ?>


			<?php if(Auth::user()->language =='ar'): ?>
				<ul class="list-inline">

					<li class="pull-left">
					<span class="pull-right">
				<a href="<?php echo e(url('admin/users/add/create')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</li>
					<li class="pull-left">

			<span class="pull-right">
				<a href="<?php echo e(url('admin/users/add/create_admin')); ?>" class="btn btn-success"><?php echo e(trans('common.create_admin')); ?></a>
			</span>
						</span>
					</li>
				</ul>
			<?php else: ?>
				<ul class="list-inline">

					<li class="pull-right">
					<span class="pull-right">
				<a href="<?php echo e(url('admin/users/add/create')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
					</li>
					<li class="pull-right">

			<span class="pull-right">
				<a href="<?php echo e(url('admin/users/add/create_admin')); ?>" class="btn btn-success"><?php echo e(trans('common.create_admin')); ?></a>
			</span>
						</span>
					</li>
				</ul>
			<?php endif; ?>

		</h3>
		<br>
		<br>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#users"><?php echo e(trans('common.all_users')); ?></a></li>
		<li><a data-toggle="tab" href="#business"><?php echo e(trans('common.bus_users')); ?></a></li>
		<li><a data-toggle="tab" href="#art"><?php echo e(trans('common.art_users')); ?></a></li>
		<li><a data-toggle="tab" href="#it"><?php echo e(trans('common.it_users')); ?></a></li>
		<li><a data-toggle="tab" href="#med"><?php echo e(trans('common.mid_users')); ?></a></li>
		<li><a data-toggle="tab" href="#eng"><?php echo e(trans('common.eng_users')); ?></a></li>
		<li><a data-toggle="tab" href="#english"><?php echo e(trans('common.english_users')); ?></a></li>
		<?php /*<li><a data-toggle="tab" href="#std">Student Center</a></li>*/ ?>
	</ul>
	<br>
	<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="tab-content clearfix">
		<div id="users" class="tab-pane fade in active">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student"><?php echo e(trans('common.students')); ?></a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#inst"><?php echo e(trans('common.instructors')); ?></a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#dean"><?php echo e(trans('common.deans')); ?></a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#admin"><?php echo e(trans('common.admins')); ?></a></li>
					<li class="divider">&nbsp;</li>
					<li><a data-toggle="pill" href="#validator"><?php echo e(trans('common.validator')); ?></a></li>
				</ul>

				<?php if(count($users) > 0): ?>
					<div class="tab-content" dir="trl">
						<div id="student" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('auth.st_id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>

									<?php foreach($users as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->st_id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst" class="tab-pane fade">
							<?php if(count($users) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div>
						<?php /*end istroctors*/ ?>

						<div id="dean" class="tab-pane fade">
							<?php if(count($users) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div>
						<?php /*end Deans*/ ?>

						<div id="validator" class="tab-pane fade">
							<?php if(count($users) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='validator'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div>
						<?php /*end validator*/ ?>

						<div id="admin" class="tab-pane fade">
							<?php if(count($users) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='admin'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div>
						<?php /*end admins*/ ?>

					</div>


			</div>
		</div><?php /*end of all users*/ ?>


		<div id="business" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_bu"><?php echo e(trans('common.students')); ?></a></li>
					<li><a data-toggle="pill" href="#inst_bu"><?php echo e(trans('common.instructors')); ?></a></li>
					<li><a data-toggle="pill" href="#dean_bu"><?php echo e(trans('common.deans')); ?></a></li>

				</ul>
				<?php /*<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>

					<div class="tab-content">
						<div id="student_bu" class="tab-pane fade in active">
							<?php if(count($users_bu) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('auth.st_id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_bu as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->st_id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>


													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Active</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >NonActive</a>
														<?php endif; ?>
													</td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_bu" class="tab-pane fade">
							<?php if(count($users_bu) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_bu as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_bu" class="tab-pane fade">
							<?php if(count($users_bu) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_bu as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of business users*/ ?>

		<div id="art" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_art"><?php echo e(trans('common.students')); ?></a></li>
					<li><a data-toggle="pill" href="#inst_art"><?php echo e(trans('common.instructors')); ?></a></li>
					<li><a data-toggle="pill" href="#dean_art"><?php echo e(trans('common.deans')); ?></a></li>

				</ul>
				<?php /*<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>

					<div class="tab-content">
						<div id="student_art" class="tab-pane fade in active">
							<?php if(count($users_art) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('auth.st_id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_art as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->st_id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_art" class="tab-pane fade">
							<?php if(count($users_art) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_art as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_art" class="tab-pane fade">
							<?php if(count($users_art) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_art as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of art users*/ ?>

		<div id="it" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_it"><?php echo e(trans('common.students')); ?></a></li>
					<li><a data-toggle="pill" href="#inst_it"><?php echo e(trans('common.instructors')); ?></a></li>
					<li><a data-toggle="pill" href="#dean_it"><?php echo e(trans('common.deans')); ?></a></li>

				</ul>
				<?php /*<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>

					<div class="tab-content">
						<div id="student_it" class="tab-pane fade in active">
							<?php if(count($users_it) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('auth.st_id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_it as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->st_id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_it" class="tab-pane fade">
							<?php if(count($users_it) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_it as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

								<?php /*<?php else: ?>*/ ?>
								<?php /*<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>*/ ?>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_it" class="tab-pane fade">
							<?php if(count($users_it) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_it as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

								<?php /*<?php else: ?>*/ ?>
								<?php /*<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>*/ ?>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of IT users*/ ?>

		<div id="med" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_med"><?php echo e(trans('common.students')); ?></a></li>
					<li><a data-toggle="pill" href="#inst_med"><?php echo e(trans('common.instructors')); ?></a></li>
					<li><a data-toggle="pill" href="#dean_med"><?php echo e(trans('common.deans')); ?></a></li>

				</ul>
				<?php /*<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>

				<div class="tab-content">
					<div id="student_med" class="tab-pane fade in active">
						<?php if(count($users_med) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('auth.st_id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_med as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->st_id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

						<?php else: ?>
							<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
						<?php endif; ?>
					</div><?php /*end students*/ ?>

					<div id="inst_med" class="tab-pane fade">
						<?php if(count($users_med) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_med as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='instructor'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

						<?php else: ?>
							<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
						<?php endif; ?>
					</div><?php /*end istroctors*/ ?>

					<div id="dean_med" class="tab-pane fade">
						<?php if(count($users_med) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_med as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='dean'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

						<?php else: ?>
							<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
						<?php endif; ?>
					</div><?php /*end Deans*/ ?>

				</div>


			</div>
		</div><?php /*end of medical users*/ ?>

		<div id="eng" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_eng"><?php echo e(trans('common.students')); ?></a></li>
					<li><a data-toggle="pill" href="#inst_eng"><?php echo e(trans('common.instructors')); ?></a></li>
					<li><a data-toggle="pill" href="#dean_eng"><?php echo e(trans('common.deans')); ?></a></li>

				</ul>
				<?php /*<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>

					<div class="tab-content">
						<div id="student_eng" class="tab-pane fade in active">
							<?php if(count($users_eng) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('auth.st_id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_eng as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->st_id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_eng" class="tab-pane fade">
							<?php if(count($users_eng) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_eng as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_eng" class="tab-pane fade">
							<?php if(count($users_eng) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.username')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('common.status')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_eng as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
														<td><?php echo e($user->timeline->name); ?></td>
														<td><?php echo e($user->email); ?></td>
														<td>
															<?php if($user->active == "1"): ?>
																<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
															<?php else: ?>
																<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
															<?php endif; ?>
														</td>

														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of enginnring users*/ ?>

		<div id="english" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-pills heading-list">
					<li class="active"><a data-toggle="pill" href="#student_english"><?php echo e(trans('common.students')); ?></a></li>
					<li><a data-toggle="pill" href="#inst_english"><?php echo e(trans('common.instructors')); ?></a></li>
					<li><a data-toggle="pill" href="#dean_english"><?php echo e(trans('common.deans')); ?></a></li>

				</ul>
				<?php /*<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>*/ ?>

					<div class="tab-content">
						<div id="student_english" class="tab-pane fade in active">
							<?php if(count($users_english) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('auth.st_id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_english as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->st_id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php /*<?php else: ?>*/ ?>
							<?php /*<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>*/ ?>
							<?php /*<?php endif; ?>*/ ?>
						</div><?php /*end students*/ ?>


						<div id="inst_english" class="tab-pane fade">

							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_english as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='instructor'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php /*<?php else: ?>*/ ?>
							<?php /*<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>*/ ?>
							<?php /*<?php endif; ?>*/ ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_english" class="tab-pane fade">

							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.username')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('common.status')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_english as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='dean'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td style="padding-bottom: 10px;"><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images" style="width: 60px !important;
height: 50px !important;"></a><a href="<?php echo e(url($user->timeline->username)); ?>" style="   margin-bottom: 0px;
margin-left: auto;
margin-right: auto;
padding: 3px 4px 4px 6px;
border-radius: 5px;
background: #cec9bd4d;
position: relative;
display: inherit;
color: #033164;"> <?php echo e($user->timeline->username); ?></a></td>
													<td><?php echo e($user->timeline->name); ?></td>
													<td><?php echo e($user->email); ?></td>
													<td>
														<?php if($user->active == "1"): ?>
															<a href="<?php echo e(url('admin/deactivate-user/'.$user->id)); ?>" class="btn btn-success announcement-status" onclick="return confirm('<?php echo e(trans("messages.are_you_sure_deactivate")); ?>')">Activate</a>
														<?php else: ?>
															<a href="<?php echo e(url('admin/activate-user/'.$user->id)); ?>" class="btn btn-danger announcement-status" >Deactivate</a>
														<?php endif; ?>
													</td>

													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<?php /*<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>*/ ?>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php /*<?php else: ?>*/ ?>
							<?php /*<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>*/ ?>
							<?php /*<?php endif; ?>*/ ?>
						</div><?php /*end Deans*/ ?>

					</div>
				<?php else: ?>
					<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
				<?php endif; ?>
			</div>



		</div><?php /*end of english users*/ ?>

	</div>
	</div>
