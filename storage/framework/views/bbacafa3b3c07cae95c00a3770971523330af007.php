
<div class="panel panel-default">
	<div class="panel-heading no-bg panel-settings">
	<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<h3 class="panel-title">
			<?php echo e(trans('admin.create_announcement')); ?>

		</h3>
	</div>
	<div class="panel-body">		
	<?php if($mode=="create"): ?>
		<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

			<?php elseif($mode=="create_business"): ?>
				<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

					<?php elseif($mode=="create_art"): ?>
						<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

							<?php elseif($mode=="create_it"): ?>
								<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

									<?php elseif($mode=="create_medicine"): ?>
										<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

											<?php elseif($mode=="create_eng"): ?>
												<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

													<?php elseif($mode=="create_english"): ?>
														<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

															<?php elseif($mode=="create_std"): ?>
																<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements')); ?>">

																<?php else: ?>
																		<form method="POST" class="socialite-form" action="<?php echo e(url('admin/announcements/'.$announcement->id.'/update')); ?>">

	<?php endif; ?>		    
	
	<?php echo e(csrf_field()); ?>

		<div class="form-horizontal announcements">
			<div class="form-group required ">
				<?php /*<?php echo e(Form::label('title', trans('admin.user id'), ['class' => 'col-sm-2 control-label'])); ?>*/ ?>
				<div class="col-sm-10">
					<div class="col-sm-10">
						<?php if($mode == "create"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php elseif($mode=="create_business"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php elseif($mode=="create_art"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php elseif($mode=="create_it"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php elseif($mode=="create_medicine"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php elseif($mode=="create_eng"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php elseif($mode=="create_english"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php elseif($mode=="create_std"): ?>
							<?php echo e(Form::hidden('user_id', Auth::user()->id)); ?>


						<?php else: ?>
							<?php echo e(Form::hidden('user_id', $announcement->user_id)); ?>


						<?php endif; ?>
					</div>
				</div>
			</div>

			<div class="form-group required ">
				<?php /*<?php echo e(Form::label('School_id', trans('admin.school id'), ['class' => 'col-sm-2 control-label'])); ?>*/ ?>
				<div class="col-sm-10">
					<?php if($mode == "create"): ?>
						<?php echo e(Form::hidden('school_id', Auth::user()->school_id)); ?>


					<?php elseif($mode=="create_business"): ?>
						<?php echo e(Form::hidden('school_id', '7')); ?>

					<?php elseif($mode=="create_art"): ?>
						<?php echo e(Form::hidden('school_id', '2')); ?>

						<?php /*<?php echo e(Form::text('school_id', '2', ['class' => 'form-control', 'readonly' => 'true'])); ?>*/ ?>
					<?php elseif($mode=="create_it"): ?>
						<?php echo e(Form::hidden('school_id', '4')); ?>

					<?php elseif($mode=="create_medicine"): ?>
						<?php echo e(Form::hidden('school_id', '5')); ?>

					<?php elseif($mode=="create_eng"): ?>
						<?php echo e(Form::hidden('school_id', '3')); ?>

					<?php elseif($mode=="create_english"): ?>
						<?php echo e(Form::hidden('school_id', '6')); ?>

					<?php elseif($mode=="create_std"): ?>
						<?php echo e(Form::hidden('school_id', '8')); ?>


				</div>
				<?php else: ?>
					<?php echo e(Form::hidden('school_id', $announcement->school_id)); ?>

					<?php /*<?php echo e(Form::text('school_id', $announcement->school_id, ['class' => 'form-control', 'readonly' => 'true'])); ?>*/ ?>
				<?php endif; ?>
				<?php if($errors->has('title')): ?>
					<span class="help-block">
					<strong><?php echo e($errors->first('title')); ?></strong>
					</span>
				<?php endif; ?>
			</div>
		</div>

			<div class="form-group required <?php echo e($errors->has('title') ? ' has-error' : ''); ?>">
			    <?php echo e(Form::label('title', trans('admin.title'), ['class' => 'col-sm-2 control-label'])); ?>

			    <div class="col-sm-10">
			      <?php if($mode == "create"): ?>
			      	<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>


					<?php elseif($mode=="create_business"): ?>
						<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_art"): ?>
						<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_it"): ?>
						<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_medicine"): ?>
						<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_eng"): ?>
						<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_english"): ?>
						<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_std"): ?>
						<?php echo e(Form::text('title',null,['class' => 'form-control'])); ?>

			      <?php else: ?>
			      	<?php echo e(Form::text('title', $announcement->title, ['class' => 'form-control'])); ?>

			      <?php endif; ?>
			      
			      <?php if($errors->has('title')): ?>
			      <span class="help-block">
			      	<strong><?php echo e($errors->first('title')); ?></strong>
			      </span>
			      <?php endif; ?>
			    </div>
			</div>
			<br>
			<br>
			<div class="form-group required <?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
			    <?php echo e(Form::label('description', trans('common.description'), ['class' => 'col-sm-2 control-label'])); ?>

			    <div class="col-sm-10">
			     	<?php if($mode =="create"): ?>
			     		<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>


					<?php elseif($mode=="create_business"): ?>
						<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_art"): ?>
						<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_it"): ?>
						<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_medicine"): ?>
						<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_eng"): ?>
						<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_english"): ?>
						<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

					<?php elseif($mode=="create_std"): ?>
						<?php echo e(Form::textarea('description', null ,['class' => 'form-control'])); ?>

			     	<?php else: ?>
			     	<?php echo e(Form::textarea('description', $announcement->description, ['class' => 'form-control'])); ?>

			     	<?php endif; ?>

			     	<?php if($errors->has('description')): ?>
					<span class="help-block">
						<strong><?php echo e($errors->first('description')); ?></strong>
					</span>
					<?php endif; ?>		     	
			    </div>
			</div>
			
			<div class="form-group required <?php echo e($errors->has('start_date') || $errors->has('end_date') ? ' has-error' : ''); ?>">
				<div class="row">
					<div class="col-md-6">
					 	<?php echo e(Form::label('start_date', trans('admin.start_date'), ['class' => 'col-sm-4 control-label'])); ?>


					 	<div class="input-group date datepicker col-sm-8">
                            <span class="input-group-addon addon-left calendar-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                            <?php if($mode=="create"): ?>
                            	<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">

							<?php elseif($mode=="create_business"): ?>
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							<?php elseif($mode=="create_art"): ?>
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							<?php elseif($mode=="create_it"): ?>
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							<?php elseif($mode=="create_medicine"): ?>
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							<?php elseif($mode=="create_eng"): ?>
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							<?php elseif($mode=="create_english"): ?>
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
							<?php elseif($mode=="create_std"): ?>
								<input type="text" class="form-control" name="start_date" id="datepicker1" placeholder="01/01/1970">
                            <?php else: ?>
                            	<input type="text" class="form-control" name="start_date" id="datepicker1" value="<?php echo e($announcement->start_date); ?>">
                            <?php endif; ?>                            
                            <span class="input-group-addon addon-right angle-addon">
                                <span class="fa fa-angle-down"></span>
                            </span>
                        </div>
                        <?php if($errors->has('start_date')): ?>
                        <span class="help-block">
                        	<strong><?php echo e($errors->first('start_date')); ?></strong>
                        </span>
                        <?php endif; ?>
					</div>
					<div class="col-md-6">
					 	<?php echo e(Form::label('end_date', trans('admin.end_date'), ['class' => 'col-sm-4 control-label'])); ?>

					 	<div class="input-group date datepicker col-sm-8">
                            <span class="input-group-addon addon-left calendar-addon">
                                <span class="fa fa-calendar"></span>
                            </span>                           
                            <?php if($mode=="create"): ?>
                            	<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							<?php elseif($mode=="create_business"): ?>
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							<?php elseif($mode=="create_art"): ?>
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							<?php elseif($mode=="create_it"): ?>
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							<?php elseif($mode=="create_medicine"): ?>
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							<?php elseif($mode=="create_eng"): ?>
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							<?php elseif($mode=="create_english"): ?>
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
							<?php elseif($mode=="create_std"): ?>
								<input type="text" class="form-control" name="end_date" id="datepicker2" placeholder="01/01/1970">
                            <?php else: ?>
                            	<input type="text" class="form-control" name="end_date" id="datepicker2" value="<?php echo e($announcement->end_date); ?>">
                            <?php endif; ?> 
                             <span class="input-group-addon addon-right angle-addon">
                                <span class="fa fa-angle-down"></span>
                            </span>
                        </div>
                        <?php if($errors->has('end_date')): ?>
                        <span class="help-block">
                        	<strong><?php echo e($errors->first('end_date')); ?></strong>
                        </span>
                        <?php endif; ?>
                    </div>
				</div>
			</div>
			
			<div class="form-group">
			    <div class="text-center">
			      <?php if($mode=="create"): ?>
			      	<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
					<?php elseif($mode=="create_business"): ?>
						<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
					<?php elseif($mode=="create_art"): ?>
						<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
					<?php elseif($mode=="create_it"): ?>
						<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
					<?php elseif($mode=="create_medicine"): ?>
						<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
					<?php elseif($mode=="create_eng"): ?>
						<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
					<?php elseif($mode=="create_english"): ?>
						<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
					<?php elseif($mode=="create_std"): ?>
						<button type="submit" class="btn btn-success"><?php echo e(trans('common.create')); ?></button>
			      <?php else: ?>
			      	<button type="submit" class="btn btn-success"><?php echo e(trans('common.save_changes')); ?></button>
			      <?php endif; ?>
			    </div>
			</div>
		</div><!-- /announcements -->
		</form>
	</div>
</div>












