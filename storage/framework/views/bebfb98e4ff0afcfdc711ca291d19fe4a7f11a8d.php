<!-- Modal starts here-->
<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog modal-likes" role="document">
        <div class="modal-content">
        	<i class="fa fa-spinner fa-spin"></i>
        </div>
    </div>
</div>
<div class="" style="padding-left: 0 px !important;padding-right: 0 px !important;">
	<div class="footer-description">
		<?php if(Auth::guest()): ?>		<div class="liusocial-terms text-center">

		<?php elseif(Auth::user()->hasRole('admin')): ?>
			<div class="liusocial-terms text-center">
				<?php elseif(Auth::user()->hasRole('dean')): ?>
					<div class="liusocial-terms text-center" style="background: #02647B">

						<?php elseif(Auth::user()->hasRole('student')): ?>
							<div class="liusocial-terms text-center">

							<?php endif; ?>

			<?php if(Auth::check()): ?>
				<a href="<?php echo e(url('contact')); ?>"><?php echo e(trans('common.contact')); ?></a> - 
				<a href="<?php echo e(url(Auth::user()->username.'/create-page')); ?>"><?php echo e(trans('common.create_page')); ?></a> - 
				<a href="<?php echo e(url(Auth::user()->username.'/create-group')); ?>"><?php echo e(trans('common.create_group')); ?></a>
							<a class="" href="<?php echo e(url('/')); ?>">
								<img class="liusocial-logo" src="<?php echo url('setting/'.Setting::get('logo')); ?>" alt="<?php echo e(Setting::get('site_name')); ?>" title="<?php echo e(Setting::get('site_name')); ?>">
							</a>
			<?php else: ?>
				<a href="<?php echo e(url('login')); ?>"><?php echo e(trans('auth.login')); ?></a> - 
				<a href="<?php echo e(url('register')); ?>"><?php echo e(trans('auth.register')); ?></a><a class="" href="<?php echo e(url('/')); ?>">
								<img class="liusocial-logo" src="<?php echo url('setting/'.Setting::get('logo')); ?>" alt="<?php echo e(Setting::get('site_name')); ?>" title="<?php echo e(Setting::get('site_name')); ?>">
							</a>
			<?php endif; ?>
			<?php foreach(App\StaticPage::get() as $staticpage): ?>
				- <a href="<?php echo e(url('page/'.$staticpage->slug)); ?>"><?php echo e($staticpage->title); ?></a>		        
		    <?php endforeach; ?>	
		    <a href="<?php echo e(url('/contact')); ?>"> - <?php echo e(trans('common.contact')); ?></a>
		</div>
					<?php if(Auth::guest()): ?><div class="liusocial-terms text-center">
					<?php elseif(Auth::user()->hasRole('admin')): ?>
						<div class="liusocial-terms text-center">
							<?php elseif(Auth::user()->hasRole('dean')): ?>
								<div class="liusocial-terms text-center" style=" ">

									<?php elseif(Auth::user()->hasRole('student')): ?>
										<div class="liusocial-terms text-center">


									<?php endif; ?>
									Built By: <?php echo e(trans('common.developers')); ?>.---.&copy;<?php echo e(date('Y')); ?>.<?php echo e(trans('common.liu')); ?>

		</div>
	</div>
</div>

<?php echo Theme::asset()->container('footer')->usePath()->add('app', 'js/app.js'); ?>