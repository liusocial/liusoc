<div class="panel panel-default">
    <div class="panel-heading no-bg panel-settings">
        <h3 class="panel-title">
            <?php echo e(trans('admin.users_acc_req')); ?>

        </h3>
    </div>
    <div class="panel-body timeline">
        <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php if(count($users) > 0): ?>
            <div class="table-responsive manage-table">
                <table class="table existing-products-table liusocial">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th><?php echo e(trans('auth.st_id')); ?></th>
                        <th><?php echo e(trans('auth.name')); ?></th>
                        <th><?php echo e(trans('common.email')); ?></th>
                        <th><?php echo e(trans('admin.user_req_date')); ?></th>

                        <th><?php echo e(trans('admin.approve_req')); ?></th>
                        <th><?php echo e(trans('admin.reject_req')); ?></th>


                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($users as $user): ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td><?php echo e($user->st_id); ?></td>
                            <td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->name); ?></a></td>

                            <td><?php echo e($user->email); ?></td>
                            <td><?php echo e($user->created_at); ?></td>
                            <td>

                                <a href="<?php echo e(url('admin/users/requests/'.$user->id.'/approve')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class=" btn btn  btn-info btn-lg"><i class="glyphicon glyphicon-ok" aria-hidden="true">Accept</i></span></a>
                            </td>
                            <td>
                                   <a href="<?php echo e(url('admin/users/requests/'.$user->id.'/reject')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="btn btn btn-danger btn-lg"><i class="glyphicon glyphicon-remove" aria-hidden="true">Reject</i></span></a>



                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div class="pagination-holder">
                <?php echo e($users->render()); ?>

            </div>
        <?php else: ?>
            <div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
        <?php endif; ?>
    </div>
</div>
