<style>

	/* change border radius for the tab , apply corners on top*/

	#exTab3 .nav-pills > li > a {
		border-radius: 4px 4px 0 0 ;
	}

	#exTab3 .tab-content {
		color : white;
		background-color: #428bca;
		padding : 5px 15px;
	}

</style>
<div class="panel panel-default">
	<div id="exTab3" class="panel-heading no-bg panel-settings">
		<h3 class="panel-title">
			<?php echo e(trans('common.manage_users')); ?>

			<ul class="list-inline">
				<li class="pull-right">
					<span class="pull-right">
				<a href="<?php echo e(url('admin/users/add/create')); ?>" class="btn btn-success"><?php echo e(trans('common.create')); ?></a>
			</span>
				</li>
				<li class="pull-right">

			<span class="pull-right">
				<a href="<?php echo e(url('admin/users/add/create_admin')); ?>" class="btn btn-success"><?php echo e(trans('common.create_admin')); ?></a>
			</span>
			</span>
				</li>
			</ul>


		</h3>
		<br>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#users">All Users</a></li>
		<li><a data-toggle="tab" href="#business">Business College</a></li>
		<li><a data-toggle="tab" href="#art">Art College</a></li>
		<li><a data-toggle="tab" href="#it">IT College</a></li>
		<li><a data-toggle="tab" href="#med">Medicine College</a></li>
		<li><a data-toggle="tab" href="#eng">Engineering College</a></li>
		<li><a data-toggle="tab" href="#english">English College</a></li>
		<?php /*<li><a data-toggle="tab" href="#std">Student Center</a></li>*/ ?>
	</ul>
	<br>
	<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<div class="tab-content clearfix">
		<div id="users" class="tab-pane fade in active">

		<div class="panel-body timeline">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#student">Students</a></li>
				<li><a data-toggle="tab" href="#inst">Instructor</a></li>
				<li><a data-toggle="tab" href="#dean">Deans</a></li>
				<li><a data-toggle="tab" href="#admin">Admins</a></li>
			</ul>

		<?php if(count($users) > 0): ?>
				<div class="tab-content">
					<div id="student" class="tab-pane fade in active">
			<div class="table-responsive manage-table">
				<table class="table existing-products-table socialite">
					<thead>
						<tr>
							<th>&nbsp;</th>
							<th><?php echo e(trans('admin.id')); ?></th> 
							<th><?php echo e(trans('auth.name')); ?></th>
							<th><?php echo e(trans('common.email')); ?></th> 
							<th><?php echo e(trans('admin.options')); ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>

						<?php foreach($users as $user): ?>
							<?php foreach($user->roles as $role): ?>
							<?php if($role->name =='student'): ?>
						<tr>
							<td>&nbsp;</td>	
							<td><?php echo e($user->id); ?></td>
							<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

							<td><?php echo e($user->email); ?></td> 
							<td>
								<ul class="list-inline">
									<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
									<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
								</ul>

							</td>
							<td>&nbsp;</td> 
						</tr>
						<?php endif; ?>
								<?php endforeach; ?>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<div class="pagination-holder">
					<?php echo e($users->render()); ?>

				</div>

			<?php else: ?>
				<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
						<?php endif; ?>
					</div><?php /*end students*/ ?>


					<div id="inst" class="tab-pane fade">
						<?php if(count($users) > 0): ?>
						<div class="table-responsive manage-table">
							<table class="table existing-products-table socialite">
								<thead>
								<tr>
									<th>&nbsp;</th>
									<th><?php echo e(trans('admin.id')); ?></th>
									<th><?php echo e(trans('auth.name')); ?></th>
									<th><?php echo e(trans('common.email')); ?></th>
									<th><?php echo e(trans('admin.options')); ?></th>
									<th>&nbsp;</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($users as $user): ?>
									<?php foreach($user->roles as $role): ?>
										<?php if($role->name =='instructor'): ?>
											<tr>
												<td>&nbsp;</td>
												<td><?php echo e($user->id); ?></td>
												<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

												<td><?php echo e($user->email); ?></td>
												<td>
													<ul class="list-inline">
														<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
														<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
													</ul>

												</td>
												<td>&nbsp;</td>
											</tr>
										<?php endif; ?>
									<?php endforeach; ?>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="pagination-holder">
							<?php echo e($users->render()); ?>

						</div>

						<?php else: ?>
							<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
						<?php endif; ?>
					</div>
					<?php /*end istroctors*/ ?>

					<div id="dean" class="tab-pane fade">
						<?php if(count($users) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='dean'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

						<?php else: ?>
							<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
						<?php endif; ?>
					</div>
					<?php /*end Deans*/ ?>

					<div id="admin" class="tab-pane fade">
						<?php if(count($users) > 0): ?>
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='admin'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

						<?php else: ?>
							<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
						<?php endif; ?>
					</div>
					<?php /*end admins*/ ?>

					</div>


		</div>
	</div><?php /*end of all users*/ ?>


		<div id="business" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#student_bu">Students</a></li>
					<li><a data-toggle="tab" href="#inst_bu">Instructor</a></li>
					<li><a data-toggle="tab" href="#dean_bu">Deans</a></li>

				</ul>
				<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php if(count($users) > 0): ?>
					<div class="tab-content">
						<div id="student_bu" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_bu as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_bu" class="tab-pane fade">
							<?php if(count($users) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_bu as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_bu" class="tab-pane fade">
							<?php if(count($users) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_bu as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
	</div><?php /*end of business users*/ ?>

		<div id="art" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#student_art">Students</a></li>
					<li><a data-toggle="tab" href="#inst_art">Instructor</a></li>
					<li><a data-toggle="tab" href="#dean_art">Deans</a></li>

				</ul>
				<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php if(count($users_art) > 0): ?>
					<div class="tab-content">
						<div id="student_bu" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_art as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_art" class="tab-pane fade">
							<?php if(count($users_art) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_art as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_art" class="tab-pane fade">
							<?php if(count($users_art) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_art as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of art users*/ ?>

		<div id="it" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#student_it">Students</a></li>
					<li><a data-toggle="tab" href="#inst_it">Instructor</a></li>
					<li><a data-toggle="tab" href="#dean_it">Deans</a></li>

				</ul>
				<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php if(count($users_it) > 0): ?>
					<div class="tab-content">
						<div id="student_it" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_it as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_it" class="tab-pane fade">
							<?php if(count($users_it) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_it as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_it" class="tab-pane fade">
							<?php if(count($users_it) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_it as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of IT users*/ ?>

		<div id="med" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#student_med">Students</a></li>
					<li><a data-toggle="tab" href="#inst_med">Instructor</a></li>
					<li><a data-toggle="tab" href="#dean_med">Deans</a></li>

				</ul>
				<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php if(count($users_med) > 0): ?>
					<div class="tab-content">
						<div id="student_bu" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_med as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_med" class="tab-pane fade">
							<?php if(count($users_med) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_med as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_med" class="tab-pane fade">
							<?php if(count($users) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_med as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of medical users*/ ?>

		<div id="eng" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#student_eng">Students</a></li>
					<li><a data-toggle="tab" href="#inst_eng">Instructor</a></li>
					<li><a data-toggle="tab" href="#dean_eng">Deans</a></li>

				</ul>
				<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php if(count($users_eng) > 0): ?>
					<div class="tab-content">
						<div id="student_eng" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_eng as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_eng" class="tab-pane fade">
							<?php if(count($users_eng) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_eng as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_eng" class="tab-pane fade">
							<?php if(count($users_eng) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_eng as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of enginnring users*/ ?>

		<div id="english" class="tab-pane fade">

			<div class="panel-body timeline">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#student_english">Students</a></li>
					<li><a data-toggle="tab" href="#inst_english">Instructor</a></li>
					<li><a data-toggle="tab" href="#dean_english">Deans</a></li>

				</ul>
				<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php if(count($users_english) > 0): ?>
					<div class="tab-content">
						<div id="student_english" class="tab-pane fade in active">
							<div class="table-responsive manage-table">
								<table class="table existing-products-table socialite">
									<thead>
									<tr>
										<th>&nbsp;</th>
										<th><?php echo e(trans('admin.id')); ?></th>
										<th><?php echo e(trans('auth.name')); ?></th>
										<th><?php echo e(trans('common.email')); ?></th>
										<th><?php echo e(trans('admin.options')); ?></th>
										<th>&nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach($users_english as $user): ?>
										<?php foreach($user->roles as $role): ?>
											<?php if($role->name =='student'): ?>
												<tr>
													<td>&nbsp;</td>
													<td><?php echo e($user->id); ?></td>
													<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

													<td><?php echo e($user->email); ?></td>
													<td>
														<ul class="list-inline">
															<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
															<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
														</ul>

													</td>
													<td>&nbsp;</td>
												</tr>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="pagination-holder">
								<?php echo e($users->render()); ?>

							</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end students*/ ?>


						<div id="inst_english" class="tab-pane fade">
							<?php if(count($users_english) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_english as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='instructor'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end istroctors*/ ?>

						<div id="dean_english" class="tab-pane fade">
							<?php if(count($users_english) > 0): ?>
								<div class="table-responsive manage-table">
									<table class="table existing-products-table socialite">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th><?php echo e(trans('admin.id')); ?></th>
											<th><?php echo e(trans('auth.name')); ?></th>
											<th><?php echo e(trans('common.email')); ?></th>
											<th><?php echo e(trans('admin.options')); ?></th>
											<th>&nbsp;</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach($users_english as $user): ?>
											<?php foreach($user->roles as $role): ?>
												<?php if($role->name =='dean'): ?>
													<tr>
														<td>&nbsp;</td>
														<td><?php echo e($user->id); ?></td>
														<td><a href="#"><img src="<?php echo e($user->avatar); ?>" alt="images"></a><a href="<?php echo e(url($user->timeline->username)); ?>"> <?php echo e($user->timeline->username); ?></a></td>

														<td><?php echo e($user->email); ?></td>
														<td>
															<ul class="list-inline">
																<li><a href="<?php echo e(url('admin/users/'.$user->timeline->username.'/edit')); ?>"><span class="pencil-icon bg-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a></li>
																<li><a href="<?php echo e(url('admin/users/'.$user->id.'/delete')); ?>" onclick="return confirm('<?php echo e(trans("messages.are_you_sure")); ?>')"><span class="trash-icon bg-danger"><i class="fa fa-trash" aria-hidden="true"></i></span></a></li>
															</ul>

														</td>
														<td>&nbsp;</td>
													</tr>
												<?php endif; ?>
											<?php endforeach; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="pagination-holder">
									<?php echo e($users->render()); ?>

								</div>

							<?php else: ?>
								<div class="alert alert-warning"><?php echo e(trans('messages.no_users')); ?></div>
							<?php endif; ?>
						</div><?php /*end Deans*/ ?>

					</div>


			</div>
		</div><?php /*end of english users*/ ?>

</div>
</div>
